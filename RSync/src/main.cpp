/*
 * Copyright (c) 2011-2015 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "applicationui.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <bbndk.h>
#include <bb/cascades/Application>


//#ifdef BBNDK_VERSION_AT_LEAST
//#if BBNDK_VERSION_AT_LEAST(10,3,1)
//#include <bb/cascades/DevelopmentSupport>
//#endif
//#endif


using namespace bb::cascades;

void myMessageOutput(QtMsgType type, const char *msg)
 {
     //in this function, you can write the message to any stream!
    // In this implementation the log is saved to file
    // devicename:/accounts/1000/appdata/app.id/logs/log
     switch (type) {
     case QtDebugMsg:
         fprintf(stderr, "[DEBUG] %s\n", msg);
         break;
     case QtWarningMsg:
         fprintf(stderr, "[WARN] %s\n", msg);
         break;
     case QtCriticalMsg:
         fprintf(stderr, "[CRITICAL] %s\n", msg);
         break;
     case QtFatalMsg:
         fprintf(stderr, "[FATAL] %s\n", msg);
         abort();
     }
 }

Q_DECL_EXPORT int main(int argc, char **argv)
{
    //qInstallMsgHandler(myMessageOutput); //install : set the callback
    //qDebug() << "[main] installed 'myMessageOutput'. Log messages will have custom format and be saved to file: devicename/accounts/1000/appdata/com.williamrjribeiro.bb10.Memoir.testDev_bb10_Memoir71b91fa2/logs/log";

    Application app(argc, argv);
    // Create the Application UI object, this is where the main.qml file
    // is loaded and the application scene is set. Set the Application as it's Parent!
    ApplicationUI mainApp(&app);
    mainApp.init();

//#ifdef BBNDK_VERSION_AT_LEAST
//#if BBNDK_VERSION_AT_LEAST(10,3,1)
    // Enable development support.
    //qWarning() << "[main] DevelopmentSupport installed! This should not be available on Release Build.";
    //DevelopmentSupport::install();
//#endif
//#endif

    qDebug() << "[main] starting main loop...";
    // Enter the application main event loop.
    return Application::exec();
}
