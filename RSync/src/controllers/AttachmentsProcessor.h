/*
 * AttachmentsProcessor.h
 *
 *  Created on: Nov 26, 2015
 *      Author: Will
 */

#ifndef ATTACHMENTSPROCESSOR_H_
#define ATTACHMENTSPROCESSOR_H_

#include <QFutureWatcher>
#include <QtNetwork/QHttpPart>

class AttachmentsProcessor: public QObject
{
    Q_OBJECT

    Q_PROPERTY(QVariantMap rememberEntry READ rememberEntry CONSTANT FINAL)
    Q_PROPERTY(bool processing READ processing NOTIFY processingChanged)

public:
    AttachmentsProcessor(const QVariantMap rememberEntry, const QString key, const QString id, const QString action, QObject* parent = 0);
    virtual ~AttachmentsProcessor();

    void process();

    bool isAllAttachmentsProcessed() const;

    QList<QHttpPart> httpParts() const;
    QList<QVariantMap> metaHttpParts() const;

    QVariantMap rememberEntry() const;

    QString getAction() const;

    QString getUniqueKey() const;

    QString getOneNoteId() const;

Q_SIGNALS:
    void processingChanged(const bool isProcessing, AttachmentsProcessor* processor);

public Q_SLOTS:
    QVariantMap start();
    void onAttachmentsProcessingFinished();

private:

    bool processing() const;

    QVariantMap m_rememberEntry;
    QString m_key;
    QString m_oneNoteId;
    QString m_action;
    QList<QHttpPart> m_httpParts;
    QList<QVariantMap> m_metaHttpParts;
    bool m_processing;
    bool m_allProcessed;

    // The thread status watcher
    QFutureWatcher<QVariantMap> *m_watcher;

};

#endif /* ATTACHMENTSPROCESSOR_H_ */
