/*
 * AppLocalization.cpp
 *
 *  Created on: 13/07/2016
 *      Author: MSI-GX660
 */

#include <QDebug>
#include <QCoreApplication>

#include "AppLocalization.h"

using namespace bb::cascades;

AppLocalization::AppLocalization(QObject *parent)
    :QObject(parent)
    , m_translator(new QTranslator(this))
    , m_localeHandler(new LocaleHandler(this))
{
    qDebug() << "[AppLocalization::AppLocalization]";

    bool ok = QObject::connect(m_localeHandler, SIGNAL(systemLanguageChanged()), this,
            SLOT(onSystemLanguageChanged()));
    Q_UNUSED(ok);
    Q_ASSERT_X(ok,"[AppLocalization::AppLocalization]", "connect systemLanguageChanged failed");

    // Initiate, load and install the application translation files.
    onSystemLanguageChanged();
}

void AppLocalization::onSystemLanguageChanged()
{
    QLocale systemLocale;
    qDebug() << "[AppLocalization::onSystemLanguageChanged] name:" << systemLocale.name() << ", language:" << systemLocale.language();

    QCoreApplication *inst = QCoreApplication::instance();
    m_currentLocale = systemLocale.name();
    QString file_name = "RSync_" % m_currentLocale;
    if (m_translator->load(file_name, "app/native/qm")) {
        inst->removeTranslator(m_translator);
        inst->installTranslator(m_translator);
    }
    else {
        qWarning() << "[AppLocalization::onSystemLanguageChanged] couldn't load language file: " << file_name;
    }
}

QString AppLocalization::getCurrentLocale()
{
    return m_currentLocale;
}

