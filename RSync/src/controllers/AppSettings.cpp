/*
 * AppSettings.cpp
 *
 *  Created on: Feb 3, 2013
 *      Author: WilliamRafael
 */

#include "AppSettings.hpp"

#include <QDebug>
#include <QDateTime>
#include <QCoreApplication>
#include <QVariant>

#include "../SettingsNames.h"

AppSettings::AppSettings(QObject *parent)
	: QObject(parent)
	, m_lastClosed("")
	, m_accessToken("")
    , m_accessTokenDateTime(QDateTime())
    , m_refreshToken("")
    , m_masterNotebookId("")
    , m_masterNotebookName(SettingsNames::MASTER_NOTEBOOK_NAME)
    , m_showUploadAlert(true)
    , m_showUploadAllAlert(true)
    , m_showFixInvalidNamesAlert(false)
    , m_showOverwriteAlert(true)
    , m_showUploadCompletedTasksAlert(false)
    , m_uploadCompletedTasks(true)
{
	qDebug() << "[AppSettings::AppSettings]";
	init();
}

void AppSettings::init()
{
    qDebug() << "[AppSettings::init]";
    // Set the application organization and name, which is used by QSettings
    // when saving values to the persistent store.
    QCoreApplication::setOrganizationName("Will Thrill");
    QCoreApplication::setApplicationName(SettingsNames::MASTER_NOTEBOOK_NAME);
    QCoreApplication::setOrganizationDomain("williamrjribeiro.com");

    //this->purge();

    QString atdt = this->getValueFor( SettingsNames::APP_ACCESS_TOKEN_DT, QString(""));
    QDateTime now = QDateTime::currentDateTime();

    if( atdt.length() > 0) {
        m_accessTokenDateTime = QDateTime::fromString(atdt, Qt::ISODate);
        qint64 secsDiff = m_accessTokenDateTime.secsTo(now);

        qDebug() << "[AppSettings::AppSettings] secsDiff: " << secsDiff;

        // Only re-use the access token if it's been less than 1hour (3600s) since it was obtained
        if(secsDiff > 0 && secsDiff < 3600){
            m_accessToken = this->getValueFor( SettingsNames::APP_ACCESS_TOKEN, m_accessToken);
        }
        else{
            setAccessTokenDateTime(QDateTime());
        }
    }

    m_masterNotebookId = this->getValueFor( SettingsNames::APP_MASTER_NOTEBOOK_ID, m_masterNotebookId);

    // Get the date and time of last time user closed the application
    m_lastClosed = this->getValueFor( SettingsNames::APP_LAST_CLOSED, m_lastClosed);
    if( false == m_lastClosed.isEmpty()) {
        const QLatin1String trueS = QLatin1String("true");
    	m_showUploadAlert = this->getValueFor( SettingsNames::APP_SHOW_UPLOAD_ALERT, trueS) == trueS;

        m_showUploadAllAlert = this->getValueFor( SettingsNames::APP_SHOW_UPLOAD_ALL_ALERT, trueS) == trueS;

        m_showFixInvalidNamesAlert = false;

        m_showOverwriteAlert = this->getValueFor( SettingsNames::APP_SHOW_OVERWRITE_ALERT, trueS) == trueS;

        m_showUploadCompletedTasksAlert = false;

        m_uploadCompletedTasks = true;
    }

    QDateTime lastClosed = QDateTime::fromString(m_lastClosed, SettingsNames::APP_DATE_FORMAT);

    qDebug() << "[AppSettings::AppSettings] m_masterNotebookId:" << m_masterNotebookId
             << ", m_showUploadAlert:" << m_showUploadAlert
             << ", m_showUploadAllAlert:" << m_showUploadAllAlert
             << ", m_showFixInvalidNamesAlert:" << m_showFixInvalidNamesAlert
             << ", m_showOverwriteAlert:" << m_showOverwriteAlert
             << ", m_showUploadCompletedTasksAlert:" << m_showUploadCompletedTasksAlert
             << ", m_uploadCompletedTasks:" << m_uploadCompletedTasks
             << ", m_lastClosed:" << m_lastClosed
             << ", days last closed:" << lastClosed.daysTo(now);
}

AppSettings::~AppSettings()
{
}


QMap<QString, QVariant> AppSettings::getSyncInfo(const QString &rememberEntryId) const
{
    //qDebug() << "[AppSettings::getSyncInfo] id: " << id;
    QMap<QString, QVariant> uploaded = this->m_settings.value(SettingsNames::MODEL_SYNC_INFO).toMap();
    QMap<QString, QVariant> val = uploaded[rememberEntryId].toMap();
    if(false == val.isEmpty())
        return val;
    else {
        return uploaded[uploaded[rememberEntryId].toString()].toMap();
    }
}

void AppSettings::saveSyncInfo(const QString rememberEntryId, const QVariantMap syncInfo) const
{
    QMap<QString, QVariant> uploaded = this->m_settings.value(SettingsNames::MODEL_SYNC_INFO).toMap();
    const QString oneNotePageId = syncInfo["oneNotePageId"].toString();

    qDebug() << "[AppSettings::saveSyncInfo] rememberEntryId:" << rememberEntryId << ", oneNotePageId:" << oneNotePageId;

    if(uploaded.contains(rememberEntryId)){

    }
    uploaded.insert(rememberEntryId, syncInfo);
    uploaded.insert(oneNotePageId, rememberEntryId);

    this->m_settings.setValue(SettingsNames::MODEL_SYNC_INFO, uploaded);
}

QString AppSettings::getValueFor(const QString& settingName, const QString& defaultValue)
{
	//qDebug() << "[AppSettings::getValueFor] settingName: " << settingName << " defaultValue: " << defaultValue;

	// get the value of the key
	QVariant value = m_settings.value(settingName);

	// If no value has been saved, return the default value.
	if( value.isNull()){
		return defaultValue;
	}

	// Otherwise, return the value stored in the m_settings object.
	return value.toString();
}

QMap<QString, QVariant> AppSettings::getValueFor(const QString& settingName, const QMap<QString, QVariant> defaultValue)
{
    //qDebug() << "[AppSettings::getValueFor] settingName: " << settingName << " defaultValue: " << defaultValue;

    // get the value of the key
    QVariant value = m_settings.value(settingName);

    // If no value has been saved, return the default value.
    if( value.isNull()){
        return defaultValue;
    }

    // Otherwise, return the value stored in the m_settings object.
    return value.toMap();
}

void AppSettings::saveValueFor(const QString& settingName, const QString& inputValue)
{
	//qDebug() << "[AppSettings::saveValueFor] settingName: " << settingName << " inputValue: " << inputValue;
    m_settings.setValue(settingName,QVariant(inputValue));
}

void AppSettings::saveValueFor(const QString &settingName, const QMap<QString, QVariant> map)
{
    m_settings.setValue(settingName, map);
}

QString AppSettings::lastClosed() const
{
	//qDebug() << "[AppSettings::lastClosed] m_lastClosed: " << m_lastClosed;
	return m_lastClosed;
}

QString AppSettings::accessToken() const
{
	return m_accessToken;
}

QString AppSettings::refreshToken() const
{
    return m_refreshToken;
}

QDateTime AppSettings::accessTokenDateTime() const
{
    return m_accessTokenDateTime;
}

QString AppSettings::masterNotebookId() const
{
    return m_masterNotebookId;
}

QString AppSettings::masterNotebookName() const
{
    return m_masterNotebookName;
}

void AppSettings::setAccessToken(const QString value)
{
	//qDebug() << "[AppSettings::setAccessToken] value: " << value;
    if(m_accessToken == value)
        return;
	m_accessToken = value;
	// saves automatically to settings
	this->saveValueFor(SettingsNames::APP_ACCESS_TOKEN, value);
	emit accessTokenChanged();
}

void AppSettings::setAccessTokenDateTime(const QDateTime value)
{
    //qDebug() << "[AppSettings::setAcessTokenDateTime] value: " << value;
    if(m_accessTokenDateTime == value)
        return;
    m_accessTokenDateTime = value;
    // saves automatically to settings
    this->saveValueFor(SettingsNames::APP_ACCESS_TOKEN_DT, value.toString(Qt::ISODate));
    emit accessTokenDateTimeChanged();
}

void AppSettings::setRefreshToken(const QString value)
{
    //qDebug() << "[AppSettings::setRefreshToken] value: " << value;
    if(m_refreshToken == value)
        return;
    m_refreshToken = value;
    // saves automatically to settings
    this->saveValueFor(SettingsNames::APP_REFRESH_TOKEN, value);
    emit refreshTokenChanged();
}

void AppSettings::setMasterNotebookId(const QString value)
{
    //qDebug() << "[AppSettings::setMasterNotebookId] value: " << value;
    if(m_masterNotebookId == value)
        return;
    m_masterNotebookId = value;
    // saves automatically to settings
    this->saveValueFor(SettingsNames::APP_MASTER_NOTEBOOK_ID, value);
    emit masterNotebookIdChanged();
}

void AppSettings::purge()
{
    qDebug() << "[AppSettings::purge]";
    //setMasterNotebookId(QString(""));
    this->saveValueFor(SettingsNames::APP_MASTER_NOTEBOOK_ID, QString(""));
    //setAccessToken(QString(""));
    //setAccessTokenDateTime(QDateTime());
    //setRefreshToken(QString(""));
    QVariantMap qvm;
    saveValueFor(SettingsNames::MODEL_SYNC_INFO, qvm);
}

bool AppSettings::showUploadAlert() const
{
    return m_showUploadAlert;
}

bool AppSettings::showUploadAllAlert() const
{
    return m_showUploadAllAlert;
}

bool AppSettings::showFixInvalidNamesAlert() const
{
    return m_showFixInvalidNamesAlert;
}

bool AppSettings::showOverwriteAlert() const
{
    return m_showOverwriteAlert;
}

bool AppSettings::showUploadCompletedTasksAlert() const
{
    return m_showUploadCompletedTasksAlert;
}

bool AppSettings::uploadCompletedTasks() const
{
    return m_uploadCompletedTasks;
}

void AppSettings::setShowUploadAlert(bool value)
{
    if(m_showUploadAlert != value){
        m_showUploadAlert = value;
        this->m_settings.setValue( SettingsNames::APP_SHOW_UPLOAD_ALERT, value );
        emit showUploadAlertChanged(value);
    }
}

void AppSettings::setShowUploadAllAlert(bool value)
{
    if(m_showUploadAllAlert != value){
        m_showUploadAllAlert = value;
        this->m_settings.setValue( SettingsNames::APP_SHOW_UPLOAD_ALL_ALERT, value );
        emit showUploadAllAlertChanged(value);
    }
}

void AppSettings::setShowFixInvalidNamesAlert(bool value)
{
    if(m_showFixInvalidNamesAlert != value){
        m_showFixInvalidNamesAlert = value;
        this->m_settings.setValue( SettingsNames::APP_SHOW_FIX_INVALID_NAMES_ALERT, value );
        emit showFixInvalidNamesAlertChanged(value);
    }
}

void AppSettings::setShowOverwriteAlert(bool value)
{
    if(m_showOverwriteAlert != value){
        m_showOverwriteAlert = value;
        this->m_settings.setValue( SettingsNames::APP_SHOW_OVERWRITE_ALERT, value );
        emit showOverwriteAlertChanged(value);
    }
}

void AppSettings::setShowUploadCompletedTasksAlert(bool value)
{
    if(m_showUploadCompletedTasksAlert != value){
        m_showUploadCompletedTasksAlert = value;
        this->m_settings.setValue( SettingsNames::APP_SHOW_UPLOAD_COMPLETED_TASKS_ALERT, value );
        emit showUploadCompletedTasksAlertChanged(value);
    }
}

void AppSettings::setUploadCompletedTasks(bool value)
{
    if(m_uploadCompletedTasks != value){
        m_uploadCompletedTasks = value;
        this->m_settings.setValue( SettingsNames::APP_UPLOAD_COMPLETED_TASKS, value );
        emit uploadCompletedTasksChanged(value);
    }
}
