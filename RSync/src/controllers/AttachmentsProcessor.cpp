/*
 * AttachmentsProcessor.cpp
 *
 *  Created on: Nov 26, 2015
 *      Author: Will
 */

#include <QDebug>
#include <QtCore>
#include <QByteArray>
#include <QBuffer>
#include <QIODevice>
#include <QDir>
#include <QtGui/QImage>
#include <QtGui/QImageReader>
#include <QtNetwork/QNetworkRequest>

#include "AttachmentsProcessor.h"

AttachmentsProcessor::AttachmentsProcessor(const QVariantMap rememberEntry, const QString key, const QString id, const QString action, QObject* parent)
    : QObject(parent)
    , m_rememberEntry(rememberEntry)
    , m_key(key)
    , m_oneNoteId(id)
    , m_action(action)
    , m_processing(false)
    , m_allProcessed(false)
    , m_watcher(NULL)
{
    qDebug() << "[AttachmentsProcessor::AttachmentsProcessor] m_rememberEntry.id: " << m_rememberEntry["id"].toString();
}

AttachmentsProcessor::~AttachmentsProcessor()
{
    if(m_watcher != NULL){
        if(m_watcher->isRunning()){
            m_watcher->cancel();
            m_watcher->waitForFinished();
        }
        m_watcher->disconnect();
        m_watcher->deleteLater();
        m_watcher = NULL;
    }
}

void AttachmentsProcessor::process()
{
    qDebug() << "[AttachmentsProcessor::process]";

    QFuture<QVariantMap> future = QtConcurrent::run(this, &AttachmentsProcessor::start);

    // create a new Future Watcher
    m_watcher = new QFutureWatcher<QVariantMap>(this);

    // Invoke our onProcessingFinished slot after the processing has finished.
    // http://qt-project.org/doc/qt-4.8/qt.html#ConnectionType-enum
    bool ok = connect(m_watcher, SIGNAL(finished()), this, SLOT(onAttachmentsProcessingFinished()), Qt::QueuedConnection);

    // This affects only Debug builds.
    Q_ASSERT_X(ok,"[AttachmentsProcessor::process]","connect finished to onAttachmentsProcessingFinished failed");
    Q_UNUSED(ok);

    // start watching the given future
    m_watcher->setFuture(future);

    m_processing = true;
    emit processingChanged(m_processing, this);
}

QVariantMap AttachmentsProcessor::start()
{
    qDebug() << "[AttachmentsProcessor::start] m_rememberEntry.id:" << m_rememberEntry["id"].toString();

    QString desc = m_rememberEntry["builtHtml"].toString();
    QVariantList attachments = m_rememberEntry["attachments"].toList();
    const QLatin1String replace = QLatin1String("</div></body>");
    int partCount = 1;

    //qDebug() << "[AttachmentsProcessor::start] desc:" << desc;

    foreach (const QVariant &att, attachments) {
        QHttpPart part;
        QVariantMap metaHttpPart;
        QVariantMap qvm = att.toMap();
        QString mymeType = qvm["mimeType"].toString();
        QString partName;
        QString tag;
        // QImage/QImageReader APIs don't work like cascades::ImageView APIs. No need for file:/// protocol!
        QString path = qvm["dataId"].toString().replace("file:///accounts/1000", QDir::currentPath());
        QFile* file = new QFile(path);
        bool openResult = false;

        //file->setParent(m_watcher); // deleted when QNetworkReply is deleted
        openResult = file->open(QIODevice::ReadOnly);

        if(false == openResult){
            qWarning() << "[AttachmentsProcessor::start] could not load file! errorString: " << file->errorString();
            continue;
        }

        // TODO: Must validate the file size! MAX 25MB!!!

        if(mymeType.indexOf(QString("image/")) >= 0){
            //qDebug() << "[AttachmentsProcessor::start] processing IMAGE file!  mymeType: " << mymeType <<", path: " << path;
            QImage img(path);

            if( img.isNull() == false){
                // e.g.: <img src="name:imageDataPart1" width="50px" height="50px"/>
                partName = "imageDataPart" % QString::number(partCount);
                tag = "<br /><img src=\"name:" % partName % "\" width=\"" % QString::number(img.size().width()) % "px\" height=\"" % QString::number(img.size().height()) % "px\" alt=\"" % file->fileName().section("/",-1,-1) % "\"/></div>";
            }
            else{
                qWarning() << "[AttachmentsProcessor::start] couldn't load image file!!!" ;
                continue;
            }
        }
        else{
            //qDebug() << "[AttachmentsProcessor::start] processing file! mymeType: " << mymeType <<", path: " << path;
            // e.g.: <object data-attachment="audio.ogg" data="name:fileDataPart2" type="audio/ogg" />
            partName = "fileDataPart" % QString::number(partCount);
            //tag = QString("<br /><object data=\"name:%1\" data-attachment=\"%2\" type=\"%3\" /></div>").arg(partName, file->fileName().section("/",-1,-1), mymeType);
            tag = "<br /><object data=\"name:\"" % partName% "\" data-attachment=\"" % file->fileName().section("/",-1,-1) % "\" type=\"" % mymeType %"\" /></div>";
        }

        //qDebug() << "[AttachmentsProcessor::start] tag:" << tag;
        // The attachment tag must be inserted on the <div id="memoir-content"> so it can be easily replaced!
        desc.replace(replace, tag);

        if( m_action == QLatin1String("PATCH") ){
            QVariant v = qVariantFromValue((void *) file);
            metaHttpPart["contentType"] = mymeType;
            metaHttpPart["partName"] = partName;
            metaHttpPart["file"] = v;
            m_metaHttpParts.append(metaHttpPart);
        }
        else{
            // Create the POST MIME Part
            part.setHeader(QNetworkRequest::ContentTypeHeader, QVariant(mymeType));
            part.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\""+partName+"\""));
            part.setBodyDevice(file);
            m_httpParts.append(part);
        }

        partCount++;
    }

    m_rememberEntry["builtHtml"] = desc;
    //qDebug() << "[AttachmentsProcessor::start] builtHtml:" << desc;
    m_allProcessed = true;
    return m_rememberEntry;
}

void AttachmentsProcessor::onAttachmentsProcessingFinished()
{
    qDebug() << "[AttachmentsProcessor::onAttachmentsProcessingFinished] m_rememberEntry.id: " << m_rememberEntry["id"].toString();
    // If the Future Watcher exists (other thread), create the Cascades Image from the Cascades ImageData from it
    if( m_watcher != NULL ){
        QVariantMap qvm = QVariantMap(m_watcher->future().result());
        //qDebug() << "[AttachmentsProcessor::onAttachmentsProcessingFinished] qvm: " << qvm;
        m_watcher->disconnect();
        m_watcher->deleteLater();
        m_watcher = NULL;
    }

    m_processing = false;
    emit processingChanged(m_processing, this);
}

QVariantMap AttachmentsProcessor::rememberEntry() const
{
    return m_rememberEntry;
}

bool AttachmentsProcessor::processing() const
{
    return m_processing;
}

bool AttachmentsProcessor::isAllAttachmentsProcessed() const
{
    return m_allProcessed;
}

QList<QHttpPart> AttachmentsProcessor::httpParts() const
{
    return m_httpParts;
}

QList<QVariantMap> AttachmentsProcessor::metaHttpParts() const
{
    return m_metaHttpParts;
}

QString AttachmentsProcessor::getUniqueKey() const
{
    // key: 18:12:0-51D5BFBE52D00908!37543
    return m_key;
}

QString AttachmentsProcessor::getOneNoteId() const
{
    return m_oneNoteId;
}

QString AttachmentsProcessor::getAction() const
{
    return m_action;
}
