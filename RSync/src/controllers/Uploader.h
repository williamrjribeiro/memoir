/*
 * Uploader.h
 *
 *  Created on: Dec 9, 2015
 *      Author: Will
 */

#ifndef UPLOADER_H_
#define UPLOADER_H_

class Uploader : public QObject
{
    Q_OBJECT
public:
    Uploader(QObject *parent = 0);
    virtual ~Uploader(){};
    Q_INVOKABLE virtual void upload(const QVariantMap rememberEntry, const QString oneNoteSectionId) = 0;
    Q_INVOKABLE virtual void patchPage(const QVariantMap rememberEntry, const QString oneNotePageId) = 0;
    Q_INVOKABLE virtual void setAccessToken(const QString) = 0;
    Q_INVOKABLE virtual QString getAccessToken() = 0;
};

#endif /* UPLOADER_H_ */
