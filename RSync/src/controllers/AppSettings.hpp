/*
 * AppSettings.h
 *
 *  Created on: Feb 3, 2013
 *      Author: williamrjribeiro
 */

#ifndef APPSETTINGS_HPP_
#define APPSETTINGS_HPP_

#include <QObject>
#include <QSettings>
#include <QDateTime>

class AppSettings: public QObject {

	Q_OBJECT

    // The Date & Time the application was closed last time. Used for showing the Long Press Tutorial image on MFE.
    // It is saved as the app Setting APP_LAST_CLOSED.
    Q_PROPERTY(QString lastClosed READ lastClosed CONSTANT FINAL)

	// OneNote Access Token
	Q_PROPERTY(QString accessToken READ accessToken WRITE setAccessToken NOTIFY accessTokenChanged)

	// The DateTime that OneNote Access Token was retrieved. Access Tokens are valid for only 1 hour.
	Q_PROPERTY(QDateTime accessTokenDateTime READ accessTokenDateTime WRITE setAccessTokenDateTime NOTIFY accessTokenDateTimeChanged)

	// OneNote Refresh Token
	Q_PROPERTY(QString refreshToken READ refreshToken WRITE setRefreshToken NOTIFY refreshTokenChanged)

	// OneNote Master Notebook Id
	Q_PROPERTY(QString masterNotebookId READ masterNotebookId WRITE setMasterNotebookId NOTIFY masterNotebookIdChanged)

	// OneNote Master Notebook Name
	Q_PROPERTY(QString masterNotebookName READ masterNotebookName CONSTANT FINAL)

	Q_PROPERTY (bool showUploadAlert READ showUploadAlert WRITE setShowUploadAlert NOTIFY showUploadAlertChanged)

	Q_PROPERTY (bool showUploadAllAlert READ showUploadAllAlert WRITE setShowUploadAllAlert NOTIFY showUploadAllAlertChanged)

	Q_PROPERTY (bool showFixInvalidNamesAlert READ showFixInvalidNamesAlert WRITE setShowFixInvalidNamesAlert NOTIFY showFixInvalidNamesAlertChanged)

	Q_PROPERTY (bool showOverwriteAlert READ showOverwriteAlert WRITE setShowOverwriteAlert NOTIFY showOverwriteAlertChanged)

	Q_PROPERTY (bool showUploadCompletedTasksAlert READ showUploadCompletedTasksAlert WRITE setShowUploadCompletedTasksAlert NOTIFY showUploadCompletedTasksAlertChanged)

	Q_PROPERTY (bool uploadCompletedTasks READ uploadCompletedTasks WRITE setUploadCompletedTasks NOTIFY uploadCompletedTasksChanged)

public:
	AppSettings(QObject *parent = 0);
	virtual ~AppSettings();

	QString lastClosed() const;
	QString	accessToken() const;
	QDateTime accessTokenDateTime() const;
	QString refreshToken() const;
	QString masterNotebookId() const;
	QString masterNotebookName() const;
	bool showUploadAlert() const;
	bool showUploadAllAlert() const;
	bool showFixInvalidNamesAlert() const;
	bool showOverwriteAlert() const;
	bool showUploadCompletedTasksAlert() const;
	bool uploadCompletedTasks() const;

	void setAccessToken(const QString);
	void setAccessTokenDateTime(const QDateTime);
	void setRefreshToken(const QString);
	void setMasterNotebookId(const QString);
	void setShowUploadAlert(bool value);
    void setShowUploadAllAlert(bool value);
    void setShowFixInvalidNamesAlert(bool value);
    void setShowOverwriteAlert(bool value);
    void setShowUploadCompletedTasksAlert(bool value);
    void setUploadCompletedTasks(bool value);

	/* Invokable functions that we can call from QML*/

	/**
	 * This Invokable function gets a value from the QSettings,
	 * if that value does not exist in the QSettings database, the default value is returned.
	 *
	 * @param settingName Index path to the item
	 * @param defaultValue Used to create the data in the database when adding
	 * @return If the objectName exists, the value of the QSettings object is returned.
	 *         If the objectName doesn't exist, the default value is returned.
	 */
	Q_INVOKABLE
	QString getValueFor(const QString &settingName, const QString &defaultValue);

	/**
     * This Invokable function gets a value from the QSettings,
     * if that value does not exist in the QSettings database, the default value is returned.
     *
     * @param settingName Index path to the item
     * @param defaultValue Used to create the data in the database when adding
     * @return If the objectName exists, the value of the QSettings object is returned.
     *         If the objectName doesn't exist, the default value is returned.
     */
    Q_INVOKABLE
    QMap<QString, QVariant> getValueFor(const QString &settingName, const QMap<QString, QVariant> defaultValue);

	/**
	 * This function sets a value in the QSettings database. This function should to be called
	 * when a data value has been updated from QML
	 *
	 * @param settingName Index path to the item
	 * @param inputValue new value to the QSettings database
	 */
	Q_INVOKABLE
	void saveValueFor(const QString &settingName, const QString &inputValue);

	/**
     * This function sets a value in the QSettings database. This function should to be called
     * when a data value has been updated from QML
     *
     * @param settingName Index path to the item
     * @param map Basically a JSON Object
     */
    Q_INVOKABLE
    void saveValueFor(const QString &settingName, const QMap<QString, QVariant> map);


    /**
     * Retrieves the SyncInfo Object/QVariantMap defined by the NotebookEntryId stored on QSettings.
     * Entries are added after a Remember Notebook is uploaded to OneNote.
     * Every Remember Entry is a new OneNote Page.
     * See: SyncRememberNotebookCmd.prototype.createEntryPageCb
     * See: RememberModel.saveSyncInfo()
     * @param id: Remember Notebook Entry Id or OneNote Page ID
     */
    Q_INVOKABLE QMap<QString, QVariant> getSyncInfo(const QString &rememberEntryId) const;

    /**
     * Saves the SyncInfo Object/QVariantMap into QSettings. The key is the 'rememberEntryId' value or 'pageId'.
     * All objects are stored on AppSettings::MODEL_SYNC_INFO.
     * Every Remember Entry is a new OneNote Page.
     */
    Q_INVOKABLE void saveSyncInfo(const QString rememberEntryId, const QVariantMap syncInfo) const;

    /**
     * Clear all Settings values saved.
     */
    Q_INVOKABLE
    void purge();

signals:
    void accessTokenChanged();
    void accessTokenDateTimeChanged();
    void refreshTokenChanged();
    void masterNotebookIdChanged();
    void showUploadAlertChanged(bool);
    void showUploadAllAlertChanged(bool);
    void showFixInvalidNamesAlertChanged(bool);
    void showOverwriteAlertChanged(bool);
    void showUploadCompletedTasksAlertChanged(bool);
    void uploadCompletedTasksChanged(bool);

private:
    void init();
    mutable QSettings m_settings;
	QString m_lastClosed;
	QString	m_accessToken;
	QDateTime m_accessTokenDateTime;
	QString m_refreshToken;
	QString m_masterNotebookId;
	QString m_masterNotebookName;
	bool m_showUploadAlert;
	bool m_showUploadAllAlert;
	bool m_showFixInvalidNamesAlert;
	bool m_showOverwriteAlert;
	bool m_showUploadCompletedTasksAlert;
	bool m_uploadCompletedTasks;
};
#endif /* APPSETTINGS_HPP_ */
