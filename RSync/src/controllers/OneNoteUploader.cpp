/*
 * OneNoteUploader.cpp
 *
 *  Created on: Dec 9, 2015
 *      Author: Will
 */

#include "OneNoteUploader.h"

#include <bb/data/JsonDataAccess>
#include <QtNetwork/QHttpPart>
#include <QFile>
#include <QDir>
#include <QBuffer>

using namespace bb::data;

QString OneNoteUploader::SERVICE_POST_PAGE_URL = "https://www.onenote.com/api/v1.0/me/notes/sections/%1/pages";
QString OneNoteUploader::SERVICE_PATCH_PAGE_URL = "https://www.onenote.com/api/v1.0/me/notes/pages/%1/content";

OneNoteUploader::OneNoteUploader(QObject *parent)
: Uploader(parent)
, m_networkAcessManager(new QNetworkAccessManager(this))
, m_accessToken(QString(""))
, m_entries(QMap<QString,QVariant>())
{
    qDebug() << "[OneNoteUploader::OneNoteUploader]";
    bool ok = connect( m_networkAcessManager, SIGNAL( finished(QNetworkReply*) ),
                       this, SLOT( requestFinished(QNetworkReply*) )
                     );
    Q_ASSERT_X(ok,"[OneNoteUploader::OneNoteUploader]", "connect requestFinished failed");
    Q_UNUSED(ok);
}

OneNoteUploader::~OneNoteUploader()
{
    // TODO: destroy/stop QNetworkAccessManager?
    disconnect( m_networkAcessManager, SIGNAL( finished(QNetworkReply*) ),
                this, SLOT( requestFinished(QNetworkReply*) )
              );
    m_networkAcessManager->disconnect();
    m_networkAcessManager->deleteLater();
    m_networkAcessManager = NULL;
    m_entries.clear();
}

void OneNoteUploader::upload(const QVariantMap rememberEntry, const QString oneNoteSectionId)
{
    qDebug() << "[OneNoteUploader::upload] oneNoteSectionId: " << oneNoteSectionId;

    QString key = createUniqueKey(rememberEntry, oneNoteSectionId);

    // DON´T make this a parent of the AttachmentsProcessor because this must have only 1 instance for the whole app
    // AttachmentsProcessor's parent is the NetworkReply so that both can be deleted later together.
    AttachmentsProcessor* processor = new AttachmentsProcessor(rememberEntry, key, oneNoteSectionId, QString("POST"));

    bool ok = connect(processor, SIGNAL(processingChanged(bool, AttachmentsProcessor*)),
                      qobject_cast<OneNoteUploader*>(this), SLOT(onProcessingChanged(bool, AttachmentsProcessor*)));

    Q_ASSERT_X(ok,"[OneNoteUploader::upload]", "connect processingChanged failed");
    Q_UNUSED(ok);

    m_entries[key] = rememberEntry;
    processor->process();
}

void OneNoteUploader::patchPage(const QVariantMap rememberEntry, const QString oneNotePageId)
{
    bool hasAttachments = rememberEntry["attachments"].toList().size() > 0;
    QString key = createUniqueKey(rememberEntry, oneNotePageId);

    qDebug() << "[OneNoteUploader::patchPage] rememberEntry.id:" << rememberEntry["id"].toString() << ", oneNotePageId:" << oneNotePageId << ", hasAttachments:" << hasAttachments;

    if(hasAttachments){
        // DON´T make this a parent of the AttachmentsProcessor because there's only 1 instance of OneNoteUploader.
        // AttachmentsProcessor parent is the NetworkReply so that both can be deleted later together.
        AttachmentsProcessor* processor = new AttachmentsProcessor(rememberEntry, key, oneNotePageId, QString("PATCH"));

        bool ok = connect(processor, SIGNAL(processingChanged(bool, AttachmentsProcessor*)),
                          qobject_cast<OneNoteUploader*>(this), SLOT(onProcessingChanged(bool, AttachmentsProcessor*)));

        Q_ASSERT_X(ok,"[OneNoteUploader::patchPage]", "connect processingChanged failed");
        Q_UNUSED(ok);

        processor->process();
    }
    else{
        sendPatchRequest(rememberEntry, oneNotePageId);
    }

    m_entries[key] = rememberEntry;
}

void OneNoteUploader::onProcessingChanged(const bool isProcessing, AttachmentsProcessor* processor)
{
    qDebug() << "[OneNoteUploader::onProcessingChanged] isProcessing: " << isProcessing << ", isAllAttachmentsProcessed: " << processor->isAllAttachmentsProcessed();
    if(false == isProcessing && processor->isAllAttachmentsProcessed()){
        // Create POST Request and send it over
        if(processor->getAction() == "POST")
            sendRequest(processor);
        else
            sendPatchRequest(processor);
    }
}

void OneNoteUploader::sendRequest(AttachmentsProcessor* processor)
{
    qDebug() << "[OneNoteUploader::sendRequest] processor.getUniqueKey:" << processor->getUniqueKey();

    QByteArray body;
    QVariantMap entry = processor->rememberEntry();
    QString desc = entry["builtHtml"].toString();
    //qDebug() << "[OneNoteUploader::sendRequest] description: " << desc;
    body.append(desc);

    // 1st: add a QHttpPart for the HTML code
    QHttpPart textPart;
    textPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"Presentation\""));
    textPart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("text/html"));
    textPart.setBody(body);

    // 2nd: Create the MultipartRequest and add all the parts
    QHttpMultiPart* multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);
    multiPart->setBoundary("MemoirBoundary");
    multiPart->append(textPart);

    foreach (const QHttpPart &part, processor->httpParts()) {
        multiPart->append(part);
    }

    // 3rd: Use the QNetworkAccessManager to send the request over the Internet
    QUrl url(buildPostPageURL(processor->getOneNoteId()));
    //qDebug() << "[OneNoteUploader::sendRequest] url: " << url.toString();
    QNetworkRequest request(url);

    QByteArray bearer;
    bearer.append("bearer ");
    bearer.append(m_accessToken);

    request.setRawHeader("Authorization", bearer);

    QNetworkReply *reply = m_networkAcessManager->post(request, multiPart);
    // Reply > MultiPart > AttachmentsProcessor > QFile : deleted on requestFinished()
    multiPart->setParent(reply); // when Reply is deleted, multiPart and AttachmentsProcessor will be too
    processor->setParent(multiPart);

    reply->setObjectName(processor->getUniqueKey());
}

void OneNoteUploader::sendPatchRequest(AttachmentsProcessor* processor)
{
    qDebug() << "[OneNoteUploader::sendPatchRequest 1] processor.key:" << processor->getUniqueKey();

    QVariantMap entry = processor->rememberEntry();

    QVariantMap command;
    command["target"] = entry["generatedId"].toString();
    command["action"] = "replace";
    command["content"] = entry["builtHtml"].toString();

    QVariantList body;
    body.append(command);

    JsonDataAccess jda;
    QString sbody;

    jda.saveToBuffer(body, &sbody);

    if( jda.hasError() ){
        qWarning() << "[OneNoteUploader::sendPatchRequest 1] jda.error.errorMessage:" << jda.error().errorMessage();
        return;
    }
    else{
        qDebug() << "[OneNoteUploader::sendPatchRequest 1] sbody:" << sbody;
    }

    QUrl url(buildPatchPageURL(processor->getOneNoteId()));
    QNetworkRequest request(url);

    QString boundary("MemoirBoundary");
    request.setRawHeader("Content-Type", QByteArray("multipart/form-data; boundary=").append(boundary));

    QByteArray bearer;
    bearer.append("Bearer ");
    bearer.append(m_accessToken);
    request.setRawHeader("Authorization", bearer);

    // 1 - Create the Commands part with the JSON object and HTML content
    QByteArray data;
    data.append("--" + boundary + "\r\n");
    data.append("Content-Disposition: form-data; name=\"Commands\"\r\n");
    data.append("Content-Type: application/json\r\n");
    data.append("\r\n");
    data.append(sbody.toUtf8());
    data.append("\r\n");

    QFile *file;

    // 2 - loop through all the Processors QHttpParts to create all the other boundaries.
    foreach (const QVariantMap &metaPart, processor->metaHttpParts()) {
        data.append("--" + boundary + "\r\n");
        data.append("Content-Disposition: form-data; name=\""+metaPart["partName"].toString()+"\"\r\n");
        data.append("Content-Type: "+metaPart["contentType"].toString()+"\r\n");
        data.append("\r\n");
        file = (QFile *) metaPart["file"].value<void *>();
        data.append(file->readAll());
        data.append("\r\n");
    }
    data.append("--" + boundary + "--\r\n");

    QBuffer *buffer = new QBuffer();
    buffer->open((QBuffer::ReadWrite));
    buffer->write(data);
    buffer->seek(0);

    //qDebug() << "[OneNoteUploader::sendPatchRequest 1] ABOUT TO SEND ATTACHMENTS!";
    //qDebug() << "[OneNoteUploader::sendPatchRequest 1]" << QString(data);
    QNetworkReply *reply = m_networkAcessManager->sendCustomRequest(request, "PATCH", buffer);

    // Reply > MultiPart > AttachmentsProcessor > QFile : deleted on requestFinished()
    reply->setObjectName(processor->getUniqueKey());
    buffer->setParent(reply);
    file->setParent(reply);
}

void OneNoteUploader::sendPatchRequest(const QVariantMap rememberEntry, const QString oneNotePageId)
{
    qDebug() << "[OneNoteUploader::sendPatchRequest] rememberEntry.id:" << rememberEntry["id"].toString() << ", oneNotePageId:" << oneNotePageId;

    //QString sbody = QString("[{'target':'#memoir-content','action':'replace','content':'%1'}]").arg(rememberEntry["builtHtml"].toString());
    QString target = rememberEntry["generatedId"].toString();
    QVariantMap command;
    command["target"] = target;
    command["action"] = target == QLatin1String("body") ? QLatin1String("append") : QLatin1String("replace");
    command["content"] = rememberEntry["builtHtml"].toString();

    QVariantList body;
    body.append(command);

    JsonDataAccess jda;
    QString sbody;

    jda.saveToBuffer(body, &sbody);

    if( jda.hasError() ){
        qWarning() << "[OneNoteUploader::sendPatchRequest] jda.error.errorMessage:" << jda.error().errorMessage();
        return;
    }
    else{
        qDebug() << "[OneNoteUploader::sendPatchRequest] sbody:" << sbody;
    }

    QBuffer *buffer = new QBuffer();
    buffer->open((QBuffer::ReadWrite));
    buffer->write(sbody.toUtf8());
    buffer->seek(0);

    QByteArray bearer;
    bearer.append("Bearer ");
    bearer.append(m_accessToken);

    QNetworkRequest req( QUrl(buildPatchPageURL(oneNotePageId)) );
    req.setRawHeader("Authorization", bearer);
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    //qDebug() << "[OneNoteUploader::sendPatchRequest] ContentLengthHeader:" << QString(dataSize).toUtf8();

    //qDebug() << "[OneNoteUploader::sendPatchRequest] ABOUT TO SEND!";
    QNetworkReply *reply = m_networkAcessManager->sendCustomRequest(req, "PATCH", buffer);

    // Reply > QBuffer : deleted on requestFinished()
    reply->setObjectName(createUniqueKey(rememberEntry, oneNotePageId));
    buffer->setParent(reply);
}

void OneNoteUploader::requestFinished(QNetworkReply *reply)
{
    bool m_error = false;
    int m_errorType = reply->error();
    QString m_errorMessage;
    QVariant m_result;
    QVariantMap entry = m_entries[reply->objectName()].toMap();

    // Operation::PostOperation == 4, Operation::CustomOperation == 6
    const QNetworkAccessManager::Operation operation = reply->operation();

    qDebug() << "[OneNoteUploader::requestFinished] id:" << entry["id"].toString() << ", title:" << entry["title"].toString() << ", reply.objectName:" << reply->objectName() << ", operation:" << operation;

    // Check the network reply for errors
    if (m_errorType == QNetworkReply::NoError) {

        // Read all the bytes to a ByteArray...
        int available = reply->bytesAvailable();
        QLatin1String response = QLatin1String("");

        if (available > 0) {
            int bufSize = sizeof(QLatin1Char) * available + sizeof(QLatin1Char);
            QByteArray buffer(bufSize, 0);
            reply->read(buffer.data(), available);

            // ... and make a QString of the ByteArray so it can be converted to JSON object
            response = QLatin1String(buffer);
        }
        else if(reply->size() > 0){
            qWarning() << "[OneNoteUploader::requestFinished] bytes NOT available. isRunning: " << reply->isRunning()<<", isFinished: " << reply->isFinished() << ", size: " << reply->size();
            response = QLatin1String(reply->readAll());
        }

        // Response is usually a HUGE string
        //qDebug() << "[OneNoteUploader::requestFinished] response:" << response;

        // Operation::PostOperation == 4, Operation::CustomOperation == 6
        if(operation == QNetworkAccessManager::PostOperation){
            // Use the JsonDataAccess class to convert from JSON String -> QVariantMap
            JsonDataAccess jda;
            m_result = jda.loadFromBuffer( response ).toMap();

            if (jda.hasError()) {
                const DataAccessError err = jda.error();
                m_error = true;
                m_errorMessage = QString("Error converting JSON data. errorMessage: %1, errorString: %2, status: %3").arg(err.errorMessage(),reply->errorString(),reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toString());
                qWarning() << "[OneNoteUploader::requestFinished] m_errorMessage:" << m_errorMessage;
            }
        }
        else {
            QVariant statusCode = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute );
            int status = statusCode.toInt();

            if(status > 204 && status <= 226){
                m_error = true;
                m_errorMessage = reply->attribute( QNetworkRequest::HttpReasonPhraseAttribute ).toString();
                qWarning() << "[OneNoteUploader::requestFinished] m_errorMessage:" << m_errorMessage <<", status:" << status;
            }
            m_result = status;
        }
    }
    else {
        const QList<QNetworkReply::RawHeaderPair> headers = reply->rawHeaderPairs();
        foreach (const QNetworkReply::RawHeaderPair &pair, headers) {
            qWarning() << "[OneNoteUploader::requestFinished] RawHeaderPair:" << pair;
        }
        m_error = true;
        m_errorMessage = reply->errorString();
        qWarning() << "[OneNoteUploader::requestFinished] m_errorType: " << m_errorType << ", m_errorMessage: " << m_errorMessage;
    }

    QVariantMap response;
    response["operation"] = operation;
    response["result"] = QVariant::fromValue(m_result);
    response["entry"] = entry;
    response["hasError"] = QVariant::fromValue(m_error);

    if(true == m_error){
        response["errorMessage"] = m_errorMessage;
        response["errorType"] = m_errorType;
    }

    reply->deleteLater();

    //qDebug() << "[OneNoteUploader::requestFinished] hasError:" << m_error << ", m_result:" << m_result;

    emit finished(response);
}

QString OneNoteUploader::createUniqueKey(const QVariantMap entry, const QString oneNoteId) const
{
    // key: 18:12:0-51D5BFBE52D00908!37543
    return entry["id"].toString()+":"+oneNoteId;
}

QString OneNoteUploader::buildPostPageURL(const QString val) const
{
    return OneNoteUploader::SERVICE_POST_PAGE_URL.arg(val);
}

QString OneNoteUploader::buildPatchPageURL(const QString val) const
{
    return OneNoteUploader::SERVICE_PATCH_PAGE_URL.arg(val);
}

void OneNoteUploader::setAccessToken(const QString val)
{
    m_accessToken = val;
}

QString OneNoteUploader::getAccessToken()
{
    return m_accessToken;
}
