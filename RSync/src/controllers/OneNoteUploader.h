/*
 * OneNoteUploader.h
 *
 *  Created on: Dec 9, 2015
 *      Author: Will
 */

#ifndef ONENOTEUPLOADER_H_
#define ONENOTEUPLOADER_H_

#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>

#include "AttachmentsProcessor.h"
#include "Uploader.h"

class OneNoteUploader : public Uploader
{
    Q_OBJECT
public:
    OneNoteUploader(QObject *parent = 0);
    virtual ~OneNoteUploader();
    void upload(const QVariantMap rememberEntry, const QString oneNoteSectionId);
    void patchPage(const QVariantMap rememberEntry, const QString oneNotePageId);
    void setAccessToken(const QString);
    QString getAccessToken();

Q_SIGNALS:
    void finished(const QVariantMap result);
    void patchFinished(const QVariantMap result);

private:
    void sendRequest(AttachmentsProcessor* processor);
    void sendPatchRequest(AttachmentsProcessor* processor);
    void sendPatchRequest(const QVariantMap rememberEntry, const QString oneNotePageId);
    QString createUniqueKey(const QVariantMap entry, const QString oneNoteId) const;
    QString buildPostPageURL(const QString val) const;
    QString buildPatchPageURL(const QString val) const;

    QNetworkAccessManager *m_networkAcessManager;
    QString m_accessToken;
    QMap<QString,QVariant> m_entries;

    static QString SERVICE_POST_PAGE_URL;
    static QString SERVICE_PATCH_PAGE_URL;

private Q_SLOTS:
    void onProcessingChanged(const bool isProcessing, AttachmentsProcessor* processor);
    void requestFinished(QNetworkReply*);
};

#endif /* ONENOTEUPLOADER_H_ */
