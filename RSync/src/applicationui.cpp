/*
 * Copyright (c) 2011-2015 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "applicationui.hpp"

#include <QDateTime>
#include <QTimer>

#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/LocaleHandler>
#include <bb/pim/notebook/NotebookEntry>
#include <bb/pim/notebook/NotebookEntryId>
#include <bb/pim/notebook/NotebookEntryDescription>
#include <bb/pim/notebook/NotebookId>

#include <bb/pim/notebook/NotebookEntryAttachment>

#include "SettingsNames.h"
#include "controllers/OneNoteUploader.h"

using namespace bb::cascades;
using namespace bb::pim::notebook;

ApplicationUI::ApplicationUI(Application *app)
    : QObject(app)
    , m_App(app)
    , appSettings(new AppSettings(this))
    , m_appLocalization(new AppLocalization(this))
    , m_rememberModel(new RememberModel(this))
    , m_uploader(new OneNoteUploader(this))
{
    qDebug() << "[ApplicationUI::ApplicationUI]";
    bool res = connect( app, SIGNAL(aboutToQuit()), this, SLOT(onAboutToQuit()));
    Q_ASSERT_X(res,"[ApplicationUI::ApplicationUI]", "connect onAboutToQuit failed");
    Q_UNUSED(res);
}

void ApplicationUI::init()
{
    qDebug() << "[ApplicationUI::init]";

    this->m_rememberModel->setAppSettings(this->appSettings);
    this->m_rememberModel->refresh(QVariantMap());

    qmlRegisterType<QTimer>("registered.types", 1, 0, "QTimer");

    // Create scene document from main.qml asset, the parent is set
    // to ensure the document gets destroyed properly at shut down.
    QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this);

    // Make the AppSettings instance available to QML as _appSettings
    qml->setContextProperty("_appSettings", this->appSettings);

    // Make the RememberModel instance available to QML as _rememberModel
    qml->setContextProperty("_rememberModel", this->m_rememberModel);

    // Make the Uploader interface available to QML as _uploader. The type of instance is set on this CONSTRUCTOR!
    qml->setContextProperty("_uploader", this->m_uploader);

    // Create root object for the UI
    AbstractPane *root = qml->createRootObject<AbstractPane>();

    // Set created root object as the application scene
    m_App->setScene(root);
}


// triggered when the user closes the Application
void ApplicationUI::onAboutToQuit()
{
    // Save the date and time before closing so that we can figure out when to show the Long Press Tutoria image
    QString quitDate = QDateTime::currentDateTime().toString(SettingsNames::APP_DATE_FORMAT);
    qDebug() << "[ApplicationUI::onAboutToQuit] Exiting application. quitDate" << quitDate;
    appSettings->saveValueFor( SettingsNames::APP_LAST_CLOSED, quitDate);

    if(m_rememberModel != NULL){
        m_rememberModel->disconnect();
        m_rememberModel->deleteLater();
    }
}

AppSettings* ApplicationUI::getAppSettings() const
{
    return this->appSettings;
}

AppLocalization* ApplicationUI::getAppLocalization() const
{
    return this->m_appLocalization;
}

RememberModel* ApplicationUI::getRememberModel() const
{
    return this->m_rememberModel;
}

Uploader* ApplicationUI::getUploader() const
{
    return this->m_uploader;
}
