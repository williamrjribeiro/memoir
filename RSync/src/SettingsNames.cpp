/*
 * SettingsNames.cpp
 *
 *  Created on: Nov 25, 2015
 *      Author: Will
 */

#include "SettingsNames.h"

const int SettingsNames::APP_MAX_DAYS = 7;
const QString SettingsNames::APP_DATE_FORMAT = "dd/MM/yyyy hh:mm:ss";
const QString SettingsNames::APP_LANG = "APP_LANG";
const QString SettingsNames::APP_LAST_CLOSED = "APP_LAST_CLOSED";
const QString SettingsNames::APP_ACCESS_TOKEN = "APP_ACCESS_TOKEN";
const QString SettingsNames::APP_ACCESS_TOKEN_DT = "APP_ACCESS_TOKEN_DT";
const QString SettingsNames::APP_REFRESH_TOKEN = "APP_REFRESH_TOKEN";
const QString SettingsNames::APP_MASTER_NOTEBOOK_ID = "APP_MASTER_NOTEBOOK_ID";
const QString SettingsNames::APP_MASTER_NOTEBOOK_NAME = "APP_MASTER_NOTEBOOK_NAME";
const QString SettingsNames::APP_SHOW_UPLOAD_ALERT = "APP_SHOW_UPLOAD_ALERT";
const QString SettingsNames::APP_SHOW_UPLOAD_ALL_ALERT = "APP_SHOW_UPLOAD_ALL_ALERT";
const QString SettingsNames::APP_SHOW_FIX_INVALID_NAMES_ALERT = "APP_SHOW_FIX_INVALID_NAMES_ALERT";
const QString SettingsNames::APP_SHOW_OVERWRITE_ALERT = "APP_SHOW_OVERWRITE_ALERT";
const QString SettingsNames::APP_SHOW_UPLOAD_COMPLETED_TASKS_ALERT = "APP_SHOW_UPLOAD_COMPLETED_TASKS_ALERT";
const QString SettingsNames::APP_UPLOAD_COMPLETED_TASKS = "APP_UPLOAD_COMPLETED_TASKS";
const QString SettingsNames::MASTER_NOTEBOOK_NAME = "BB10 Notes by Memoir";
const QString SettingsNames::MODEL_SYNC_INFO = "MODEL_SYNC_INFO";


SettingsNames::SettingsNames()
{
}

SettingsNames::~SettingsNames()
{
}
