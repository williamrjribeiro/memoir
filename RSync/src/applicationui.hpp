/*
 * Copyright (c) 2011-2015 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ApplicationUI_HPP_
#define ApplicationUI_HPP_

#include <QObject>
#include <bb/cascades/Application>

#include "controllers/AppSettings.hpp"
#include "controllers/AppLocalization.h"
#include "controllers/Uploader.h"
#include "models/RememberModel.hpp"

/*!
 * @brief Application UI object
 *
 * Use this object to create and init app UI, to create context objects, to register the new meta types etc.
 */
class ApplicationUI : public QObject
{
    Q_OBJECT
public:
    ApplicationUI(bb::cascades::Application *app);
    virtual ~ApplicationUI() {};
    void init();
    AppSettings* getAppSettings() const;
    AppLocalization* getAppLocalization() const;
    RememberModel* getRememberModel() const;
    Uploader* getUploader() const;

private slots:
    void onAboutToQuit();

private:
    bb::cascades::Application* m_App;
    AppSettings* appSettings;
    AppLocalization* m_appLocalization;
    RememberModel* m_rememberModel;
    Uploader* m_uploader;
};

#endif /* ApplicationUI_HPP_ */
