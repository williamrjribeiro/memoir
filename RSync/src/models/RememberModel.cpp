/*
 * RememberModel.cpp
 *
 *  Created on: Jun 3, 2015
 *      Author: Will
 */

#include "RememberModel.hpp"

#include <bb/pim/notebook/NotebookEntry>
#include <bb/pim/notebook/NotebookEntryId>
#include <bb/pim/notebook/NotebookEntryDescription>
#include <bb/pim/notebook/NotebookEntryStatus>
#include <bb/system/LocaleHandler>
#include <bb/system/LocaleType>
#include <bb/utility/i18n/DateFormat>
#include <bb/utility/i18n/Formatting>
#include "bb/cascades/databinding/arraydatamodel.h"

#include <Qt>
#include <QSettings>
#include <QDebug>
#include <QLocale>

#include "../SettingsNames.h"

using namespace bb::cascades;
using namespace bb::system;
using namespace bb::pim::notebook;
using namespace bb::utility::i18n;

const QLatin1String RememberModel::NOTE_STATUS_OFFLINE = QLatin1String("offline");      // Offline: the data is not on OneNote, only on the device
const QLatin1String RememberModel::NOTE_STATUS_OUTDATED = QLatin1String("outdated");    // Outdated: the local data is newer than the remote data and needs to be uploaded
const QLatin1String RememberModel::NOTE_STATUS_UPLOADING = QLatin1String("uploading");  // Uploading: the local data is being uploaded to OneNote's servers
const QLatin1String RememberModel::NOTE_STATUS_UPDATED = QLatin1String("updated");      // Updated: the local data matches the remote data
const QLatin1String RememberModel::NOTE_STATUS_INVALID = QLatin1String("invalid");      // Invalid: the local data is older than the remote data. Since there's only 1-way syncing, the local data will overwrite the remote changes.

RememberModel::RememberModel(QObject *parent)
    : QObject(parent)
    , m_notebookService(new NotebookService(this))
    , m_notebooks(new ArrayDataModel(this))
    , m_appSettings(0)
    , m_countSyncedItems(0)
{
    qDebug() << "[RememberModel::RememberModel]";


    bool ok = connect(m_notebookService, SIGNAL(notebookEntriesAdded(QList<bb::pim::notebook::NotebookEntryId>)), SLOT(onNotebookServiceChange()));
    Q_ASSERT_X(ok,"[RememberModel::RememberModel]", "connect SIGNAL notebookEntriesAdded failed");

    ok = connect(m_notebookService, SIGNAL(notebookEntriesUpdated(QList<bb::pim::notebook::NotebookEntryId>)), SLOT(onNotebookServiceChange()));
    Q_ASSERT_X(ok,"[RememberModel::RememberModel]", "connect SIGNAL notebookEntriesUpdated failed");

    ok = connect(m_notebookService, SIGNAL(notebookEntriesDeleted(QList<bb::pim::notebook::NotebookEntryId>)), SLOT(onNotebookServiceChange()));
    Q_ASSERT_X(ok,"[RememberModel::RememberModel]", "connect SIGNAL notebookEntriesDeleted failed");

    ok = connect(m_notebookService, SIGNAL(notebooksAdded (const QList< bb::pim::notebook::NotebookId > &)), SLOT(onNotebookServiceChange()));
    Q_ASSERT_X(ok,"[RememberModel::RememberModel]", "connect SIGNAL notebooksAdded failed");

    ok = connect(m_notebookService, SIGNAL(notebooksDeleted (const QList< bb::pim::notebook::NotebookId > &)), SLOT(onNotebookServiceChange()));
    Q_ASSERT_X(ok,"[RememberModel::RememberModel]", "connect SIGNAL notebooksDeleted failed");

    ok = connect(m_notebookService, SIGNAL(notebooksUpdated (const QList< bb::pim::notebook::NotebookId > &)), SLOT(onNotebookServiceChange()));
    Q_ASSERT_X(ok,"[RememberModel::RememberModel]", "connect SIGNAL notebooksUpdated failed");

    Q_UNUSED(ok);
}

RememberModel::~RememberModel()
{
    this->disconnect();

    m_notebookService->disconnect();
    m_notebookService->deleteLater();
    m_notebookService = NULL;

    m_notebooks->disconnect();
    m_notebooks->deleteLater();
    m_notebooks = NULL;
}

void RememberModel::refresh(QVariantMap oneNoteFreshPages)
{
    const QList<Notebook> nbs = m_notebookService->notebooks();
    const LocaleHandler region( LocaleType::Region );
    const QLocale locale = region.locale();
    const bool uploadCompletedTasks = this->m_appSettings->uploadCompletedTasks();

    qWarning() << "[RememberModel::refresh] oneNoteFreshPages.size:" << oneNoteFreshPages.size() << ", uploadCompletedTasks: " << uploadCompletedTasks;

    const char * notesMessage = "%1 Note(s)";
    const char * tasksMessage = "%1 Task(s)";

    // Clear the old note information from the notebooks. EMITS itemsChanged() signal!!!
    m_notebooks->clear();
    m_countSyncedItems = 0;

    // Iterate over the list of notebooks
    foreach (const Notebook &nb, nbs) {
        // Only care for Generic (local) notebooks because...
        if(nb.type() ==  Notebook::Generic){

            NotebookEntryFilter filter;
            filter.setParentNotebookId(nb.id());

            if( false == uploadCompletedTasks )
                filter.setStatus(NotebookEntryStatus::NotActionable | NotebookEntryStatus::NotCompleted);

            const QList<NotebookEntry> notebookEntries = m_notebookService->notebookEntries(filter);
            int n = notebookEntries.size();
            QVariantList notebooks;

            qDebug() << "[RememberModel::refresh] nb.name: " << nb.name() << ", nb.id: " << nb.id().toString() << ", notebookEntries.size: " << n;
            // No need to show empty Notes folders
            if(n > 0){
                // Copy the data into a notebooks entry
                QVariantMap notebook;
                notebook["id"] = QVariant::fromValue(nb.id().toString());
                notebook["name"] = QVariant::fromValue(nb.name());

                // Tasks and Notes are never on the same Notebook! We only need to check the first element to figure out if we're dealing with
                // a tasklist or a notebook
                if( notebookEntries.at(0).status() == NotebookEntryStatus::NotActionable )
                    notebook["notesCount"] = QVariant::fromValue( tr(notesMessage, 0, n).arg( locale.toString(n) ) );
                else
                    notebook["notesCount"] = QVariant::fromValue( tr(tasksMessage, 0, n).arg( locale.toString(n) ) );

                QVariantList entries;
                QDateTime latestModified = QDateTime::fromTime_t(0);

                const QMap<QString, QVariant> uploadedPages = this->findNotebookUploadedPages(oneNoteFreshPages, nb.name());

                int countOffline = 0;
                int countOutdated = 0;
                int countUpdated = 0;
                int countInvalid = 0;
                int countTasks = 0;
                int countPlainTasks = 0;
                int countCompletedTasks = 0;

                bool hasPlainTaskListPage = false;

                // Iterate over the list of Entries of the current Notebook
                foreach (const NotebookEntry &entry, notebookEntries) {
                    QVariantMap e;
                    const NotebookEntryId nei = entry.id();
                    const NotebookEntryDescription description = entry.description();
                    const QDateTime elmdt = entry.lastModifiedDateTime();

                    // ATTENTION: Must call this BEFORE description.isEmpty() or else it bugs!
                    // https://supportforums.blackberry.com/t5/Native-Development/Bad-bug-in-NotebookEntryDescription-return-empty-when-it-is-not/m-p/2483159/highlight/true#M27464
                    //qDebug() << "[RememberModel::refresh] entry.description:" << description.text();
                    description.text();
                    const bool hasNoDescription = description.plainText().isEmpty();

                    e["id"] = QVariant::fromValue(nei.toString());
                    e["title"] = QVariant::fromValue(entry.title());
                    e["lastModified"] = QVariant::fromValue(elmdt.toString(Qt::ISODate));

                    // Every Notebook Entry is matched with an OneNote Page.
                    // Since Pages and Entries don't have unique Titles, i.e.: Pages can have the same title we map their id's.
                    // All the Sync Information is stored on 'syncInfo' property Object/Qmap
                    // Every NotebookEntry has a reference to a syncInfo which is stored on QSettings.
                    QMap<QString, QVariant> syncInfo = this->m_appSettings->getSyncInfo(nei.toString());
                    e["syncInfo"] = syncInfo;

                    const QLatin1String status = this->determineStatus2(entry, uploadedPages, syncInfo);
                    qDebug() << "[RememberModel::refresh] entry status:" << status;

                    if( status == RememberModel::NOTE_STATUS_INVALID){
                        countInvalid++;
                        e["status"] = RememberModel::NOTE_STATUS_INVALID;
                    }
                    else if( status == RememberModel::NOTE_STATUS_UPDATED){
                        countUpdated++;
                        e["status"] = RememberModel::NOTE_STATUS_UPDATED;
                    }
                    else if( status == RememberModel::NOTE_STATUS_OUTDATED){
                        countOutdated++;
                        e["status"] = RememberModel::NOTE_STATUS_OUTDATED;
                    }
                    else if( status == RememberModel::NOTE_STATUS_OFFLINE){
                        countOffline++;
                        e["status"] = RememberModel::NOTE_STATUS_OFFLINE;
                    }
                    else {
                        qWarning() << "[RememberModel::refresh] @#$ FUCK $#@ Unkown entry status: " << status;
                    }

                    if(elmdt > latestModified){
                        latestModified = elmdt;
                    }

                    QVariantList attachments;
                    QList<NotebookEntryAttachment> entryAttachments = entry.attachments();
                    qDebug() << "[RememberModel::refresh] entryAttachments.size: " << entryAttachments.size();

                    if(entryAttachments.size() > 0){
                        foreach (const NotebookEntryAttachment &attachment, entryAttachments) {
                            QVariantMap a;
                            a["dataId"] = QVariant::fromValue(attachment.dataId().toString());
                            a["mimeType"] = QVariant::fromValue(attachment.mimeType());
                            attachments.append(a);
                        }
                    }

                    e["attachments"] = attachments;

                    //const NotebookEntryDescription::Type dt = description.type();
                    //e["type"] = QVariant::fromValue(dt == NotebookEntryDescription::HTML ? QString("html") : QString("plain"));

                    const NotebookEntryStatus::Type st = entry.status();
                    e["taskStatus"] = st;

                    // taskStatus = 1: NotActionable, 2: NotCompleted, 4: Completed
                    if(st == NotebookEntryStatus::NotActionable ) {
                        e["type"] = QString("html");
                        e["dueDate"] = QString();
                        e["reminder"] = QString();
                    }
                    else{
                        countTasks++;

                        if(st == NotebookEntryStatus::Completed)
                            countCompletedTasks++;

                        const QDateTime dueDt = entry.dueDateTime();
                        const QDateTime reminderDt = entry.reminderTime();

                        // If a Task has no Description and no Due Date or Reminder, force it to be a PLAIN Task so its on the Task List Page
                        if( hasNoDescription && dueDt.isNull() && reminderDt.isNull()){
                            e["type"] = QString("plain");
                            e["dueDate"] = QString();
                            e["reminder"] = QString();

                            countPlainTasks++;

                            if( status != RememberModel::NOTE_STATUS_OFFLINE){
                                // Plain Tasks are grouped in Task List Pages so they don't count
                                hasPlainTaskListPage = true;
                            }
                        }
                        else{
                            e["type"] = QString("html");
                            e["dueDate"] = QVariant::fromValue( locale.toString( dueDt, dateFormat(locale, DateFormat::Medium)));
                            e["reminder"] = QVariant::fromValue( locale.toString( reminderDt, dateTimeFormat(locale, DateFormat::Medium)));

                            qDebug() << "[RememberModel::refresh] entry.reminder: " << reminderDt.toString() << ", entry.dueDate: " << dueDt.toString();
                        }
                    }

                    qDebug() << "[RememberModel::refresh] entry.title:" << entry.title() << ", entry.type:" << e["type"].toString() << ", hasNoDescription:" << hasNoDescription << ", entry.status:" << st;

                    e["description"] = QVariant::fromValue(description.text());

                    entries.append(e);
                }

                notebook["entries"] = entries;

                // Add all items of this Notebook to the Global Items count. Must remove Plain Tasks because they're on Task List Page (count that one!)
                m_countSyncedItems += (countOutdated + countUpdated + countInvalid - countPlainTasks + ( hasPlainTaskListPage ? 1 : 0 ) );

                qDebug() << "[RememberModel::refresh] countOffline:" << countOffline << ", countOutdated:" << countOutdated << ", countUpdated:" << countUpdated << ", countInvalid:" << countInvalid << ", countTasks:" << countTasks << ", countPlainTasks:" << countPlainTasks << ", notebook.name:" << nb.name();

                if(countInvalid > 0){
                    notebook["statusLabel"] = RememberModel::NOTE_STATUS_INVALID;
                }
                else if (countOutdated > 0 || (countOffline > 0 && countUpdated > 0)){
                    notebook["statusLabel"] = RememberModel::NOTE_STATUS_OUTDATED;
                }
                else if(countOffline > 0 && countOutdated <= 0 && countInvalid <= 0 && countUpdated <= 0){
                    // Show offline ONLY IF all Entries are offline.
                    notebook["statusLabel"] = RememberModel::NOTE_STATUS_OFFLINE;
                }
                else{
                    notebook["statusLabel"] = RememberModel::NOTE_STATUS_UPDATED;
                }

                notebook["countCompletedTasks"] = QVariant::fromValue( countCompletedTasks );

                // Add the entry to the notebooks
                notebooks.append(notebook);
            }

            if(notebooks.size() > 0){
                // Do a batch insert into the model so that the signal 'itemsChanged' is dispatched
                m_notebooks->append(notebooks);
            }
        }
    }
    qDebug() << "[RememberModel::refresh] notebooks.size:" << m_notebooks->size() << ", m_countSyncedItems:" << m_countSyncedItems;
    emit countSyncedItemsChanged( m_countSyncedItems );
}

QVariantMap RememberModel::updateNotebookByIndex(QVariantMap props, const QVariantList indexPath)
{
    qDebug() << "[RememberModel::updateNotebookByIndex] indexPath: " << indexPath << ", props: " << props;
    QVariantMap qvm = m_notebooks->data(indexPath).toMap();
    for(QVariantMap::const_iterator iter = props.begin(); iter != props.end(); ++iter) {
      //qDebug() << iter.key() << iter.value();
      qvm[iter.key()] = iter.value().toString();
    }
    // replace the item on the model list
    m_notebooks->replace(indexPath.at(0).toInt(), qvm);
    return qvm;
}

const QLatin1String RememberModel::determineStatus(QDateTime const &fromDT, QDateTime const &toDT) const
{
    const QString fromDTString = fromDT.toString(Qt::ISODate);
    const QString toDTString = toDT.toString(Qt::ISODate);
    const qint64 msDiff = fromDT.msecsTo(toDT);
    const qint64 MIN_MS_DIFF = 7000;

    qDebug() << "[RememberModel::determineStatus] fromDTString: " << fromDTString << ", toDTString: " << toDTString << ", msDiff: " << msDiff;

    if( fromDTString.compare( toDTString ) == 0 )
        return RememberModel::NOTE_STATUS_UPDATED;
    else {
        // Must ignore MS value from Remember DateTime value because OneNote does too. Give 7s for variation.
        if( msDiff < 0 && msDiff < - MIN_MS_DIFF ){
            qWarning() << "[RememberModel::determineStatus] INVALID ENTRIES!";
            return RememberModel::NOTE_STATUS_INVALID;
        }
        else if( qAbs(msDiff) < MIN_MS_DIFF){
            return RememberModel::NOTE_STATUS_UPDATED;
        }
        else if( msDiff >= MIN_MS_DIFF){
            return RememberModel::NOTE_STATUS_OUTDATED;
        }
    }
    return RememberModel::NOTE_STATUS_OFFLINE;
}

const QLatin1String RememberModel::determineStatus2(NotebookEntry const & entry, QMap<QString, QVariant> const & uploadedPages, QMap<QString, QVariant>& syncInfo) const
{
    qDebug() << "[RememberModel::determineStatus2] entry.title:" << entry.title() << ", uploadedPages.size:" << uploadedPages.size() << ", syncInfo.isEmpty:" << syncInfo.isEmpty();

    if(syncInfo.isEmpty() && uploadedPages.isEmpty() ) {
        return RememberModel::NOTE_STATUS_OFFLINE;
    }

    const QDateTime elmdt = entry.lastModifiedDateTime();

    // Se a Entry já foi syncronizada (uploaded)
    if( ! syncInfo.isEmpty() ) {

        const QString onpid = syncInfo["oneNotePageId"].toString();
        const QDateTime syncInfoEntryLMT = QDateTime::fromString(syncInfo["rememberLastModified"].toString(), Qt::ISODate);

        qDebug() << "[RememberModel::determineStatus2] syncInfo.rememberLastModified:" << syncInfoEntryLMT.toString(Qt::ISODate) << ", syncInfo.oneNotePageId:" << onpid;

        // Se há uploadedPages, usa o oneNotePageId do SyncInfo para obter a Page da lista de uploadedPages
        if( ! uploadedPages.isEmpty() ){
            QVariantMap page = uploadedPages.value(onpid).toMap();
            // Se não achou a página pelo oneNotePageId, procura pelo Título da Entry
            if( page.isEmpty() ){
                page = this->findUploadedPage(uploadedPages, QString(), entry.title());
                // Se ainda assim não achar, OFFLINE. MUITO ESTRANHO pois se há syncInfo e uploadedPages, tinha de haver uma Page. Provavelmente deletou a Page?
                if( page.isEmpty()) {
                    qWarning() << "[RememberModel::determineStatus2] @#$ FUCK $#@ What happened to this Entry's OneNote Page!?";
                    return RememberModel::NOTE_STATUS_OFFLINE;
                }
            }
            const QDateTime pageLMT = QDateTime::fromString(page["lastModifiedTime"].toString(), Qt::ISODate);
            const QDateTime syncInfoPageLMT = QDateTime::fromString(syncInfo["lastModifiedTime"].toString(), Qt::ISODate);

            // SE Page.LastModifiedTime == Entry.SyncInfo.PageLastModifiedTime, Page está UPDATED! se < CONFLICT, se > FUCK!
            // Se PAGE está UPDATED E Se Entry.SyncInfo.RememberLastModifiedTime == Entry.LastModifiedTime, Entry UPDATED, se < OUTDATED, se > FUCK!
            const QLatin1String pageStatus = this->determineStatus(pageLMT, syncInfoPageLMT);

            qDebug() << "[RememberModel::determineStatus2] pageStatus:" << pageStatus << ", page.title:" << page["title"].toString();

            if(pageStatus == RememberModel::NOTE_STATUS_UPDATED) {
                return this->determineStatus(syncInfoEntryLMT, elmdt);
            }
            else {
                return pageStatus;
            }
        }
        // Quando não há uploadedPages, é quando RememberModel.refresh() é chamado ANTES de ter configurado o OneNote (ou accessToken já expirou)
        //  portanto, basta comparar syncInfo.DUM com Entry.DUM (já que a Entry pode ser mais nova)
        else {
            return this->determineStatus(syncInfoEntryLMT, elmdt);
        }

    }

    // Se não há syncInfo MAS HÁ uploadedPages, provavelmente o App foi reinstalado
    //  então temos que reconstruir o syncInfo das Entries MAS SOMENTE SE houver Page correspondente
    else { //if( ! uploadedPages.isEmpty()) {
        QVariantMap page = this->findUploadedPage(uploadedPages, QString(), entry.title());
        // Se não encontrar Page com o Entry.title, provavelmente é uma Entry nova. OFFLINE
        if( page.isEmpty()) {
            qWarning() << "[RememberModel::determineStatus2] @#$ FUCK $#@ Is this a NEW Entry or WHAT !? entry.title: " << entry.title();
            return RememberModel::NOTE_STATUS_OFFLINE;
        }
        else {
            // ACHOU uma Page com o mesmo Entry.title. No OneNote é possível ter mais de uma Page com o mesmo Título na Section do Notebook, é um risco MAS
            //   assumimos que ela é única (equesafodaaa!!!).
            const QString elmdtString = elmdt.toString(Qt::ISODate);
            syncInfo["oneNotePageId"] = page["id"].toString();
            syncInfo["lastModifiedTime"] = elmdtString;
            syncInfo["rememberLastModified"] = elmdtString;
            syncInfo["createdTime"] = page["createdTime"].toString();
            syncInfo["oneNoteSectionId"] = page["parentSection"].toMap()["id"].toString();

            // Recontruímos e salvamos o syncInfo para a Entry.
            m_appSettings->saveSyncInfo(entry.id().toString(), syncInfo);

            // Como não é possível determinar o estado da Entry em relação a Page (ou vice-versa), é mais seguro alerta-lo de que pode haver perda de dados
            return RememberModel::NOTE_STATUS_INVALID;
        }
    }

    //qDebug() << "[RememberModel::determineStatus2] syncInfo.lastModifiedTime :" << syncInfoPageLMT.toString(Qt::ISODate) << ", page.lastModifiedTime:" << pageLMT.toString(Qt::ISODate) << ", page.id:" << page["id"].toString();
}

QMap<QString, QVariant> RememberModel::findUploadedPage(QVariantMap const & oneNoteFreshPages, QString const & notebookName, QString const & entryName) const
{
    qDebug() << "[RememberModel::findUploadedPage] oneNoteFreshPages.size: " << oneNoteFreshPages.size() << ", notebookName: " << notebookName << ", entryName: " << entryName;
    QMap<QString, QVariant> page;
    QMap<QString, QVariant> parentSection;
    QVariantMap::const_iterator iter = oneNoteFreshPages.begin();
    QVariantMap::const_iterator end = oneNoteFreshPages.end();

    for(; iter != end; ++iter){
        page = iter.value().toMap();
        //qDebug() << "[RememberModel::findUploadedPage] "<< iter.key() << page;
        parentSection = page["parentSection"].toMap();
        if(notebookName.isEmpty() && page["title"].toString() == entryName){
            return page;
        }
        else if( !parentSection.isEmpty() && parentSection["name"].toString() == notebookName && page["title"].toString() == entryName){
            //qDebug() << "[RememberModel::findUploadedPage] found it!";
            return page;
        }
    }

    return QMap<QString, QVariant>();
}

/**
 * Returns a Map<OneNotePage.id, OneNotePage> where OneNotePage.parentSection.name == notebookName. It can return an empty map!
 */
QMap<QString, QVariant> RememberModel::findNotebookUploadedPages(QVariantMap const & oneNoteFreshPages, QString const & notebookName) const
{
    qDebug() << "[RememberModel::findNotebookUploadedPages] oneNoteFreshPages.size: " << oneNoteFreshPages.size() << ", notebookName: " << notebookName;

    if( oneNoteFreshPages.isEmpty() )
        return QMap<QString, QVariant>();

    QMap<QString, QVariant> found;
    QMap<QString, QVariant> page;
    QMap<QString, QVariant> parentSection;
    QVariantMap::const_iterator iter = oneNoteFreshPages.begin();
    QVariantMap::const_iterator end = oneNoteFreshPages.end();

    for(; iter != end; ++iter){
        page = iter.value().toMap();
        const QString pageId = page["id"].toString();
        const QString pageTitle = page["title"].toString();

        parentSection = page["parentSection"].toMap();

        if( ! parentSection.isEmpty()){
            const QString parentSectionName = parentSection["name"].toString();
            if(notebookName.compare( parentSectionName ) == 0 ) {
                found.insert(pageId, page);
            }
            else {
                //qDebug() << "[RememberModel::findNotebookUploadedPages] NOT my page. pageTitle:" << pageTitle << ", parentSectionName:" << parentSectionName << ", pageId:" << pageId;
            }
        }
        else {
            qWarning() << "[RememberModel::findNotebookUploadedPages] Page without Parent Section!? pageTitle:" << pageTitle << ", pageId:" << pageId;
        }
    }
    //qDebug() << "[RememberModel::findNotebookUploadedPages] found.size: " << found.size();
    return found;
}

void RememberModel::onNotebookServiceChange()
{
    //qDebug() << "[RememberModel::onNotebookServiceChange]";
    this->refresh();
}

ArrayDataModel* RememberModel::notebooks() const
{
    return m_notebooks;
}

void RememberModel::setAppSettings(const AppSettings* appSettings)
{
    this->m_appSettings = appSettings;
}
const AppSettings* RememberModel::getAppSettings()
{
    return this->m_appSettings;
}

int RememberModel::countSyncedItems() const
{
    return this->m_countSyncedItems;
}
