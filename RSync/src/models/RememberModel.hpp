/*
 * RememberModel.hpp
 *
 *  Created on: Jun 3, 2015
 *      Author: Will
 */

#ifndef REMEMBERMODEL_HPP_
#define REMEMBERMODEL_HPP_

#include <bb/cascades/ArrayDataModel>
#include <bb/pim/notebook/NotebookId>
#include <bb/pim/notebook/NotebookEntry>
#include <bb/pim/notebook/NotebookService>
#include <bb/pim/notebook/NotebookEntryAttachment>

#include <QtCore/QObject>

#include "../controllers/AppSettings.hpp"

class RememberModel: public QObject
{
    Q_OBJECT

    // The model that provides the filtered list of notes
    Q_PROPERTY(bb::cascades::ArrayDataModel* notebooks READ notebooks CONSTANT FINAL);

    Q_PROPERTY(int countSyncedItems READ countSyncedItems NOTIFY countSyncedItemsChanged);

public:

    static const QLatin1String NOTE_STATUS_OFFLINE;       // Offline: the data is not on OneNote, only on the device
    static const QLatin1String NOTE_STATUS_OUTDATED;      // Outdated: the local data is newer than the remote data and needs to be uploaded
    static const QLatin1String NOTE_STATUS_UPLOADING;     // Uploading: the local data is being uploaded to OneNote's servers
    static const QLatin1String NOTE_STATUS_UPDATED;       // Updated: the local data matches the remote data
    static const QLatin1String NOTE_STATUS_INVALID;       // Invalid: the local data is older than the remote data. Since there's only 1-way syncing, the local data will overwrite the remote changes.

    RememberModel(QObject *parent = 0);
    virtual ~RememberModel();

    void setAppSettings(const AppSettings* appSettings);
    const AppSettings* getAppSettings();
    int  countSyncedItems() const;

    Q_INVOKABLE void refresh(QVariantMap oneNoteFreshPages = QVariantMap());

    Q_INVOKABLE QVariantMap updateNotebookByIndex(QVariantMap, const QVariantList);

signals:
    void countSyncedItemsChanged(int);

private:
    // The accessor methods of the Notebooks data
    bb::cascades::ArrayDataModel* notebooks() const;

    // variable to be passed without copying but stop it from then being altered. An immutable Reference!
    const QLatin1String determineStatus(QDateTime const &, QDateTime const  &) const;
    const QLatin1String determineStatus2(bb::pim::notebook::NotebookEntry const &, QMap<QString, QVariant> const &, QMap<QString, QVariant>&) const;

    QMap<QString, QVariant> findUploadedPage(QVariantMap const &, QString const &,QString const &) const;
    QMap<QString, QVariant> findNotebookUploadedPages(QVariantMap const &, QString const &) const;

    // The central object to access the notebook service
    bb::pim::notebook::NotebookService* m_notebookService;

    // The real Notebook data
    bb::cascades::ArrayDataModel* m_notebooks;

    const AppSettings* m_appSettings;

    // A Global counter of all Synced Items in all Notes
    int m_countSyncedItems;

private slots:
    void onNotebookServiceChange();
};

#endif /* REMEMBERMODEL_HPP_ */
