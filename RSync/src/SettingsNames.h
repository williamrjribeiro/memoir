/*
 * SettingsNames.h
 *
 *  Created on: Nov 25, 2015
 *      Author: Will
 */

#ifndef SETTINGSNAMES_H_
#define SETTINGSNAMES_H_

class SettingsNames
{
public:
    // The max days between the last time the app was used and now to show the tutorial: 7
    static const int APP_MAX_DAYS;

    // The String format used to save the date the app was last used: "dd/MM/yyyy hh:mm:ss"
    static const QString APP_DATE_FORMAT;

    // The Application language setting name.
    static const QString APP_LANG;

    // The Application lastClosed setting name
    static const QString APP_LAST_CLOSED;

    // The Application OneNote Access Token setting name
    static const QString APP_ACCESS_TOKEN;

    // The Application OneNote Access Token Date Time setting name
    static const QString APP_ACCESS_TOKEN_DT;

    // The Application OneNote Refresh Token setting name
    static const QString APP_REFRESH_TOKEN;

    // The Application Master Notebook ID setting name
    static const QString APP_MASTER_NOTEBOOK_ID;

    // The Application Master Notebook Name setting name
    static const QString APP_MASTER_NOTEBOOK_NAME;

    static const QString APP_SHOW_UPLOAD_ALERT;
    static const QString APP_SHOW_UPLOAD_ALL_ALERT;
    static const QString APP_SHOW_FIX_INVALID_NAMES_ALERT;
    static const QString APP_SHOW_OVERWRITE_ALERT;
    static const QString APP_SHOW_UPLOAD_COMPLETED_TASKS_ALERT;
    static const QString APP_UPLOAD_COMPLETED_TASKS;

    // The Master Notebook Name
    static const QString MASTER_NOTEBOOK_NAME;

    // The QSettings name of the object containing all uploaded entries
    static const QString MODEL_SYNC_INFO;

    SettingsNames();
    virtual ~SettingsNames();
};

#endif /* SETTINGSNAMES_H_ */
