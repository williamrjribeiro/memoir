import bb.cascades 1.3
import bb.system 1.2
import "js/onenoteapi.js" as ONC

Page {
    id: page
    content: ScrollView {
        scrollViewProperties.scrollMode: ScrollMode.Vertical
        scrollViewProperties.pinchToZoomEnabled: false
        Container {
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Button {
                    id: btnSetupOneNote
                    text: "Setup OneNote"
                    onClicked: {
                        _appSettings.accessToken = "";
                        _appSettings.refreshToken = "";
                        ONC.facade.resetTokens();
                        var sheet = oneNoteSetupCD.createObject(page);
                        sheet.open();
                    }
                }
                Button {
                    id: btnRefreshOneNote
                    text: "Refresh OneNote"
                    onClicked: {
                        ONC.facade.refreshAuthToken(function refreshAuthTokenCallback(req){
                            myQmlToast.body = req.status;
                            myQmlToast.show();
                        });
                    }
                }
            }
            Label {
                text: "Test RequestAssetsCmd:"
            }
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Button {
                    id: btnTestNotebooks
                    text: "Notebooks"
                    onClicked: {
                        enabled = false;
                        var c = new ONC.facade.controller.RequestAssetsCmd({
                                endpoint: "notebooks",
                                successCb: function(){
                                    btnTestNotebooks.text = "Notebooks: " + ONC.facade.model.notebooks.length;
                                    drdNotebookIds.removeAll();
                                    
                                    for(var i = 0, l = ONC.facade.model.notebooks.length; i < l; i++){
                                        var item = ONC.facade.model.notebooks[i],
                                            opt = cdOption.createObject();
                                            
                                        opt.text = item.name + " :: " + item.id
                                        opt.value = item;
                                        drdNotebookIds.add(opt);
                                    }
                                    btnTestNotebooks.enabled = true;
                                    txaOutput.text = c.responseText;
                                },
                                failCb: function(){
                                    console.log("fucked notebooks...");
                                    btnTestNotebooks.enabled = true;
                                    txaOutput.text = c.responseText;
                                }
                        });
                        c.execute();
                    }
                }
                Button {
                    id: btnTestSections
                    text: "Sections"
                    onClicked: {
                        enabled = false;
                        var c = new ONC.facade.controller.RequestAssetsCmd({
                                endpoint: "sections",
                                notebookId: drdNotebookIds.selectedOption ? drdNotebookIds.selectedOption.value.id : null,
                                successCb: function(){
                                    btnTestSections.text = "Sections: " + ONC.facade.model.sections.length;
                                    drdSectionIds.removeAll();
                                    var model = ONC.facade.model.sections;
                                    if(drdNotebookIds.selectedOption){
                                        model = ONC.facade.getFromModelBy("notebooks","id", drdNotebookIds.selectedOption.value.id, true).sections;
                                    }
                                    for(var i = 0, l = model.length; i < l; i++){
                                        var item = ONC.facade.model.sections[i],
                                            opt = cdOption.createObject();
                                            
                                        opt.text = item.name + " :: " + item.id;
                                        opt.value = item;
                                        drdSectionIds.add(opt);
                                    }
                                    btnTestSections.enabled = true;
                                    txaOutput.text = c.responseText;
                                },
                                failCb: function(){
                                    console.log("fucked sections...");
                                    btnTestSections.enabled = true;
                                    txaOutput.text = c.responseText;
                                }
                        });
                        c.execute();
                    }
                }
                Button {
                    id: btnTestPages
                    text: "Pages"
                    onClicked: {
                        enabled = false;
                        var c = new ONC.facade.controller.RequestAssetsCmd({
                                endpoint: "pages",
                                sectionId: drdSectionIds.selectedOption ? drdSectionIds.selectedOption.value.id : null,
                                args: "?orderby=createdTime&select=id,title,lastModifiedTime&count=true" + (drdNotebookIds.selectedOption ? "&filter=parentNotebook/id eq '"+drdNotebookIds.selectedOption.value.id+"'&expand=parentNotebook" : ""),
                                successCb: function(){
                                    btnTestPages.text = "Pages: " + ONC.facade.model.pages.length;
                                    drdPageIds.removeAll();
                                    
                                    for(var i = 0, l = ONC.facade.model.pages.length; i < l; i++){
                                        var item = ONC.facade.model.pages[i],
                                        opt = cdOption.createObject();
                                        
                                        opt.text = item.title + " :: " + item.id;
                                        opt.value = item;
                                        drdPageIds.add(opt);
                                    }
                                    btnTestPages.enabled = true;
                                    txaOutput.text = c.responseText;
                                },
                                failCb: function(){
                                    console.log("fucked pages...");
                                    btnTestPages.enabled = true;
                                    txaOutput.text = c.responseText;
                                }
                        });
                        c.execute();
                    }
                }
                Button {
                    id: btnTestContent
                    text: "Content"
                    onClicked: {
                        enabled = false;
                        var c = new ONC.facade.controller.RequestAssetsCmd({
                                endpoint: "content",
                                id: drdPageIds.selectedOption ? drdPageIds.selectedOption.value.id : null,
                                successCb: function(){
                                    btnTestContent.enabled = true;
                                    txaOutput.text = c.responseText;
                                },
                                failCb: function(){
                                    console.log("fucked content...");
                                    btnTestContent.enabled = true;
                                    txaOutput.text = c.responseText;
                                }
                        });
                        c.execute();
                    }
                }
            }
            DropDown {
                id: drdNotebookIds
                title: "Notebook IDs"
                enabled: true
                onSelectedOptionChanged: {
                    // "2014-06-25T12:07:56.897Z"
                    myQmlToast.body = "" + selectedOption.value.lastModifiedTime;
                    myQmlToast.show();
                }
            }
            DropDown {
                id: drdSectionIds
                title: "Sections IDs"
                enabled: true
                onSelectedOptionChanged: {
                    // "2014-06-25T12:07:56.897Z"
                    myQmlToast.body = "" + selectedOption.value.lastModifiedTime;
                    myQmlToast.show();
                }
            }
            DropDown {
                id: drdPageIds
                title: "Page IDs"
                enabled: true
                onSelectedOptionChanged: {
                    // "2014-06-25T12:07:56.897Z"
                    myQmlToast.body = "" + selectedOption.value.lastModifiedTime;
                    myQmlToast.show();
                }
            }
            Label {
                text: "Test CreateAssetsCmd:"
            }
            TextField {
                id: txfAssetName
                hintText: "Asset name"
                horizontalAlignment: HorizontalAlignment.Fill
                inputMode: TextFieldInputMode.Text
                textFormat: TextFormat.Plain
            }
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Button {
                    id: btnCreateNotebook
                    text: "Notebooks"
                    onClicked: {
                        enabled = false;
                        var c = new ONC.facade.controller.CreateAssetsCmd({
                                endpoint: "notebooks",
                                body: JSON.stringify({"name": txfAssetName.text}),
                                successCb: function(){
                                    console.log(c.data.createdTime + " - " + c.data.id);
                                    btnCreateNotebook.enabled = true;
                                },
                                failCb: function(){
                                    console.log("fucked notebooks...");
                                    btnCreateNotebook.enabled = true;
                                }
                        });
                        c.execute();
                    }
                }
                Button {
                    id: btnCreateSection
                    text: "Sections"
                    onClicked: {
                        enabled = false;
                        var c = new ONC.facade.controller.CreateAssetsCmd({
                                endpoint: "sections",
                                notebookId: drdNotebookIds.selectedOption.value,
                                body: JSON.stringify({"name": txfAssetName.text}),
                                successCb: function(){
                                    console.log(c.data.createdTime + " - " + c.data.id);
                                    btnCreateSection.enabled = true;
                                },
                                failCb: function(){
                                    console.log("fucked sections...");
                                    btnCreateSection.enabled = true;
                                }
                        });
                        c.execute();
                    }
                }
                Button {
                    id: btnCreatePage
                    text: "Pages"
                    onClicked: {
                        enabled = false;
                        var strVar="";
                        strVar += "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
                        strVar += "<html xmlns=\"http:\/\/www.w3.org\/1999\/xhtml\" lang=\"en-us\">";
                        strVar += "    <head>";
                        strVar += "        <title>"+txfAssetName.text+"<\/title>";
                        strVar += "        <meta name=\"created\" content=\""+Date.now()+"\" \/> ";
                        strVar += "    <\/head>";
                        strVar += "    <body>";
                        strVar += "        <h1>"+txfAssetName.text+"<\/h1>";
                        strVar += "        <h2>sectionId: "+drdSectionIds.selectedOption.value+"<\/h2>";
                        strVar += "    <\/body>";
                        strVar += "<\/html>";
                        
                        var c = new ONC.facade.controller.CreateAssetsCmd({
                                endpoint: "pages",
                                sectionId: drdSectionIds.selectedOption.value,
                                body: strVar,
                                successCb: function(){
                                    console.log(c.data.createdTime + " - " + c.data.id);
                                    btnCreatePage.enabled = true;
                                },
                                failCb: function(){
                                    console.log("fucked pages...");
                                    btnCreatePage.enabled = true;
                                }
                        });
                        c.execute();
                    }
                }
                Button {
                    id: btnPatchPage
                    text: "Patch"
                    onClicked: {
                        enabled = false;
                        var c = new ONC.facade.controller.PatchPageCmd({
                                id: drdPageIds.selectedOption ? drdPageIds.selectedOption.value.id : null,
                                body: JSON.stringify([{
                                            target: '#memoir-content',
                                            action: 'replace',
                                            content: '<p>'+txfAssetName.text+'</p>'
                                }]),
                                successCb: function(){
                                    console.log("Page Contents PATCHED!!!");
                                    txaOutput.text = c.responseText;
                                    btnPatchPage.enabled = true;
                                },
                                failCb: function(){
                                    console.log("fucked PATCHING Page Contents...");
                                    btnPatchPage.enabled = true;
                                }
                        });
                        c.execute();
                    }
                }
            }
            TextArea {
                id: txaOutput
                hintText:" This is used for output!" 
                editable: false
                textFormat: TextFormat.Plain
                textStyle.fontSize: FontSize.XSmall
                verticalAlignment: VerticalAlignment.Fill
                horizontalAlignment: HorizontalAlignment.Fill
            }       
            attachedObjects: [
                ComponentDefinition {
                    id: oneNoteSetupCD
                    source: "OneNoteSetup.qml"
                },
                ComponentDefinition {
                    id: cdOption
                    Option {}
                },
                SystemToast {
                    id: myQmlToast
                }
            ]
        }
    }
}
