import bb.cascades 1.4

import "js/onenoteapi.js" as ONC

Sheet {
    id: setUpOneNoteSheet
    onCreationCompleted: {
        oneNoteWebview.url = ONC.facade.getServiceTokenRequestUrl()
        setUpOneNoteSheet.open();
    }
    // Destroy onClosed because WebView uses too much memory
    onClosed: setUpOneNoteSheet.destroy()
    content: Page {
        titleBar: TitleBar {
            title: qsTr("Configuring OneNote")
            dismissAction: ActionItem {
                title: qsTr("Close")
                ActionBar.placement: ActionBarPlacement.OnBar
                onTriggered: setUpOneNoteSheet.close()
            }
        }
        Container {
            // layout definition
            layout: DockLayout {}
            ScrollView {
                scrollRole: ScrollRole.Main
                scrollViewProperties.scrollMode: ScrollMode.Vertical
                scrollViewProperties.initialScalingMethod: ScalingMethod.None
                verticalAlignment: VerticalAlignment.Fill
                horizontalAlignment: HorizontalAlignment.Fill
                
                WebView {
                    id: oneNoteWebview
                    settings.background: Color.Transparent
                    settings.credentialAutoFillEnabled: true
                    settings.formAutoFillEnabled: true
                    onNavigationRequested: {
                        //console.log("[oneNoteWebview.onNavigationRequested] url: " + request.url + " -- navigationType: " + request.navigationType)
                        
                        acinWebPageLoading.running = true;
                        acinWebPageLoading.visible = true;
                        
                         var u = request.url.toString();
                         ONC.facade.setAccessTokenByURL(u, true);
                        
                        if (ONC.facade.getAccessToken().length == 0) {
                            request.action = WebNavigationRequestAction.Accept;
                        } else {
                            // Don't let the Webview load this URL!!!
                            request.action = WebNavigationRequestAction.Ignore;
                            
                            //
                            // MasterNotebook is initialized from LocalNotes.notebooksContainer.initMB()
                            // once this Sheet is closed!
                            //
                            
                            setUpOneNoteSheet.close();
                        }
                    }
                    onLoadingChanged: {
                        //console.log("[oneNoteWebview.onLoadingChanged] loadRequest.status: " + loadRequest.status);
                        if (loadRequest.status == WebLoadStatus.Succeeded) {
                            acinWebPageLoading.running = false;
                            acinWebPageLoading.visible = false;
                        }
                    }
                } // Webview
            } // ScrollView
            ActivityIndicator {
                id: acinWebPageLoading
                visible: true
                running: true
                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Center
                scaleX: 2
                scaleY: 2
            }
        } // Page
    }
}
