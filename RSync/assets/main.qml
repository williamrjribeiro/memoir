import bb 1.3
import bb.cascades 1.4
import bb.system 1.2
import registered.types 1.0

import "js/onenoteapi.js" as ONC

TabbedPane {
    id: rootTabbedPane
    showTabsOnActionBar: false
    property variant helpComp: null
    property variant oneNoteSetupComp: null
    
    function openHelp(){
        if(! helpComp)
            helpComp = compDefHelp.createObject(rootTabbedPane)
        else
            helpComp.open()
    }
    
    function onOneNoteSetupClosed(){
        //console.log("[rootTabbedPane.onOneNoteSetupClosed]");
        if(ONC.facade.getAccessToken().length > 0){
            initMasterNotebook();
        }
        else 
            console.log("[rootTabbedPane.onOneNoteSetupClosed] MUST CONFIGURE ONENOTE FIRST!");
    }
    
    function openOneNoteSetup(){
        if(! oneNoteSetupComp){
            oneNoteSetupComp = compDefOneNoteSetup.createObject(rootTabbedPane);
            oneNoteSetupComp.closed.connect(onOneNoteSetupClosed);
        }
        else
            oneNoteSetupComp.open();
    }
    
    function showWaitMessage(){
        syToInfo.body = qsTr("Setting up OneNote. Please wait...");
        syToInfo.cancel();
        syToInfo.show();
    }
    
    function showReadyMessage(){
        updateRememberModel();
        
        syToInfo.body = qsTr("OneNote setup finished. Memoir's ready to use!");
        syToInfo.cancel();
        syToInfo.show();
        notebookList.actionItemUploadAllEnabled = !_rememberModel.notebooks.isEmpty();
    }
    
    function updateRememberModel(){
        var m = ONC.facade.model;
        
        console.log("[main.updateRememberModel pages.length: " + m.pages.length );
        
        if(m.pages.length > 0){
            var pageIds = {},
                p;
                
            for(var i = 0, l = m.pages.length - 1; i <= l; i++){
                p = m.pages[i];
                pageIds[p.id] = p;
            }
            
            _rememberModel.refresh(pageIds);
        }
        else
            _rememberModel.refresh();
    }
    
    function initMasterNotebook(){
        showWaitMessage();
        ONC.facade.initMasterNotebook(showReadyMessage);
    }
    
    Tab {
        id: notebooksListTab
        title: qsTr("Notebooks List")
        content: NotebooksList {
            id: notebookList
        }
    }
    /*
    Tab {
        id: onctest
        title: qsTr("DEBUG")
        content: ONCTest {
        }
    }
    */
    onCreationCompleted: {
        OrientationSupport.supportedDisplayOrientation = SupportedDisplayOrientation.DisplayPortrait;

        var taskLabels = {
            listTitle: qsTr("Task List"),
            done: qsTr("Done"),
            dueDate: qsTr("Due Date"),
            reminder: qsTr("Reminder")
        };
        
        if( ONC.facade.init(_appSettings, _rememberModel, _uploader, waitTimer, taskLabels) ){
            notebookList.emitTokenNeeded.connect(openOneNoteSetup);
            notebookList.emitMasterNotebookIdNeeded.connect(showWaitMessage);
            
            if(! ONC.facade.isAccessTokenValid())
                openOneNoteSetup();
            else {
                var url = "#access_token="+_appSettings.accessToken+"&token_type=";
                ONC.facade.setAccessTokenByURL(url, false);
                initMasterNotebook();
            }
        }
        else{
            syToInfo.body = qsTr("Could Not Initialize ONC. Please contact support.");
            syToInfo.cancel();
            syToInfo.show();
        }
    }

    Menu.definition: MenuDefinition {
        settingsAction: SettingsActionItem {
            onTriggered: settingsComp.open()
        }
        helpAction: HelpActionItem {
            onTriggered: openHelp()
        }
    }
    
    attachedObjects: [
        SystemToast {
            id: syToInfo
            button{
                enabled: true
                label: qsTr("OK")
            }
        },
        ComponentDefinition {
            id: compDefOneNoteSetup
            source: "OneNoteSetup.qml"
        },
        Settings {
            id: settingsComp
        },
        ComponentDefinition {
            id: compDefHelp
            source: "Help.qml"
        },
        MemoryInfo {
            id: memoryInfo
            onLowMemory: {
                console.log("[rootTabbedPane.memoryInfo.onLowMemory] level:", level, ", activeTab.title:", activeTab.title);
                if (level == LowMemoryWarningLevel.LowPriority) {
                    if(settingsComp && ! settingsComp.opened){
                        settingsComp.destroy();
                    }
                    if(oneNoteSetupComp && ! oneNoteSetupComp.opened){
                        oneNoteSetupComp.destroy();
                        oneNoteSetupComp = null;
                    }
                    if(helpComp && ! helpComp.opened){
                        helpComp.destroy();
                        helpComp = null;
                    }
                }
            }
        },
        /*
         * QML doesn´t support JavaScript setTimeout() APIs so we must use QTimer.
         * QTimer is not a native QML type. It´s registered as a custom registered type in ApplicationUI().
         * This instance is passed to OneNoteJSAPI so it will be used there too. 
         * @see ONC.facade.init(...)
         */
        QTimer{
            id: waitTimer
            singleShot: true
            interval: 15000
        }
    ]
}