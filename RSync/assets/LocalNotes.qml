import bb.cascades 1.4

import "js/onenoteapi.js" as ONC

Page {
    id: page
     Container {
         id: notebooksContainer
         function initMB(){
             console.log("[notebooksContainer.initMB]");
             ONC.facade.initMasterNotebook(_appSettings);
         }
         layout: StackLayout {}
         onCreationCompleted: {
             console.log("[notebooksContainer.onCreationCompleted]");
             
             //ONC.facade.resetTokens();
             var sheet = oneNoteSetupCompDef.createObject(page);
             sheet.closed.connect(initMB);
             sheet.open();
         }
         Label {
             text: qsTr("Local Notes")
             horizontalAlignment: HorizontalAlignment.Center
         }
         ListView {
             id: localNotebooksList
             horizontalAlignment: HorizontalAlignment.Fill
             verticalAlignment: VerticalAlignment.Fill
             dataModel: _rememberModel.notebooks
             function getModel() {
                 return _rememberModel;
             }
             function getSettings() {
                 return _appSettings;
             }
             function getONCFacade() {
                 return ONC.facade;
             }
             function syncRememberNotebook(rememberNotebook){
                 console.log("[LocalNotes.syncRememberNotebook] rememberNotebook.name: "+rememberNotebook.name);
                 var c = new ONC.facade.controller.SyncRememberNotebookCmd({rememberNotebook: rememberNotebook});
                 c.execute(_appSettings);
             }
             listItemComponents: [
                 ListItemComponent {
                     Container {
                         id: listItem
                         layout: StackLayout {
                         }
                         Label {
                             text: ListItemData.name
                             textStyle.fontWeight: FontWeight.Bold
                         }
                         Label {
                             text: qsTr("Notes found: ") + ListItemData.entries.length
                             textStyle.fontStyle: FontStyle.Italic
                             textStyle.fontSize: FontSize.Small
                         }
                         Button {
                             id: btnSync
                             text: qsTr("Upload")
                             enabled: readyToSync()
                             onClicked: {
                                 var model = listItem.ListItem.view.getModel(),
                                     rememberNotebook = model.notebooks.data(listItem.ListItem.indexPath);
                                  
                                  console.log("indexPath: "+listItem.ListItem.indexPath+", rememberNotebook.id: "+rememberNotebook.id);
                                  listItem.ListItem.view.syncRememberNotebook(rememberNotebook);
                             }
                         }
                         function readyToSync(){
                             var settings = listItem.ListItem.view.getSettings();
                             return (settings.accessToken.length > 0 && settings.masterNotebookId.length > 0)
                         }
                     }
                 }
             ]
         }
     }
    attachedObjects: [
        ComponentDefinition {
            id: oneNoteSetupCompDef
            source: "OneNoteSetup.qml"
        }
    ]
    actions: [
        ActionItem {
            ActionBar.placement: ActionBarPlacement.InOverflow
            title: qsTr("Refresh")
            onTriggered: {
                _rememberModel.refresh();
            }
        }
    ]
}
