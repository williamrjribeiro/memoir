import bb.cascades 1.4

Sheet {
    id: settingsSheet
    property real paddings: ui.sdu(2.0)
    content: Page {
        
        titleBar: TitleBar {
            title: qsTr("Settings")
            dismissAction: ActionItem {
                title: qsTr("Close")
                ActionBar.placement: ActionBarPlacement.OnBar
                onTriggered: settingsSheet.close()
            }
        }
        
        ScrollView {
            scrollRole: ScrollRole.Main
            scrollViewProperties {
                scrollMode: ScrollMode.Vertical
                pinchToZoomEnabled: false
                overScrollEffectMode: OverScrollEffectMode.OnScroll   
            }
            Container {
                Header {
                    title: qsTr("Confirmations")
                    bottomMargin: paddings
                    navigation.focusPolicy: NavigationFocusPolicy.NotFocusable
                }
                TextArea {
                    editable: false
                    text: qsTr("Enable or disable confirmation alert dialogs. If an option is enabled, an alert dialog will appear when needed.")
                    textFormat: TextFormat.Plain
                    textStyle.fontSize: FontSize.XSmall
                    textStyle.color: Color.Gray
                    bottomPadding: 0.0
                    topPadding: 0.0
                    leftPadding: 20.0

                    focusHighlightEnabled: false
                    navigation.focusPolicy: NavigationFocusPolicy.NotFocusable
                }
                ColorfulDivider {
                    color: ui.palette.primary
                }
                Container {
                    leftPadding: paddings
                    rightPadding: paddings
                    Container {
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        horizontalAlignment: HorizontalAlignment.Fill

                        Label {
                            id: lblUpload
                            text: qsTr("Notes & Tasks Folder Upload")
                            textStyle.fontWeight: FontWeight.Default
                            textStyle.fontSize: FontSize.Medium
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Fill
                            multiline: false
                            navigation.focusPolicy: NavigationFocusPolicy.NotFocusable
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 1
                            }
                        }
                        ToggleButton {
                            checked: _appSettings.showUploadAlert
                            onCheckedChanged: _appSettings.showUploadAlert = checked
                            verticalAlignment: VerticalAlignment.Center
                            accessibility.labelledBy: lblUpload
                            navigation.focusPolicy: NavigationFocusPolicy.Focusable
                        }
                    }
                    TextArea {
                        editable: false
                        text: qsTr("A Notebook named 'BB10 Notes by Memoir' is automatically added to your OneNote account after you set it up to work with Memoir. Notes & Tasks Folders are created as Sections of this Notebook. Check the Help page for more information.")
                        textFormat: TextFormat.Plain
                        textStyle.fontSize: FontSize.XSmall
                        textStyle.color: Color.Gray
                        bottomPadding: 0.0
                        topPadding: 0.0
                        leftPadding: 0.0
                        rightPadding: 0.0
                        focusHighlightEnabled: false
                        navigation.focusPolicy: NavigationFocusPolicy.NotFocusable
                    }
                }
                ColorfulDivider {
                    color: ui.palette.primary
                }
                Container {
                    leftPadding: paddings
                    rightPadding: paddings
                    Container {
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        horizontalAlignment: HorizontalAlignment.Fill
                        Label {
                            text: qsTr("Upload All")
                            textStyle.fontWeight: FontWeight.Default
                            textStyle.fontSize: FontSize.Medium
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Fill
                            multiline: false
                            navigation.focusPolicy: NavigationFocusPolicy.NotFocusable
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 1
                            }
                        }
                        ToggleButton {
                            checked: _appSettings.showUploadAllAlert
                            onCheckedChanged: _appSettings.showUploadAllAlert = checked
                            horizontalAlignment: HorizontalAlignment.Right
                            verticalAlignment: VerticalAlignment.Center
                            navigation.focusPolicy: NavigationFocusPolicy.Focusable
                        }
                    }
                    TextArea {
                        editable: false
                        text: qsTr("All Notes & Tasks are uploaded automatically including Outdated or Invalid ones. Invalid names are fixed automatically. ATTENTION: Important data might be lost when uploading folders in Conflict.")
                        textFormat: TextFormat.Plain
                        textStyle.fontSize: FontSize.XSmall
                        textStyle.color: Color.Gray
                        bottomPadding: 0.0
                        topPadding: 0.0
                        leftPadding: 0.0
                        rightPadding: 0.0
                        focusHighlightEnabled: false
                        navigation.focusPolicy: NavigationFocusPolicy.NotFocusable
                    }
                }
                ColorfulDivider {
                    color: ui.palette.primary
                }
                Container {
                    leftPadding: paddings
                    rightPadding: paddings
                    Container {
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        horizontalAlignment: HorizontalAlignment.Fill
                        Label {
                            text: qsTr("Overwrite Notes & Tasks")
                            textStyle.fontWeight: FontWeight.Default
                            textStyle.fontSize: FontSize.Medium
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Fill
                            multiline: false
                            navigation.focusPolicy: NavigationFocusPolicy.NotFocusable
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 1
                            }
                        }
                        ToggleButton {
                            id: tgbOverwrite
                            checked: _appSettings.showOverwriteAlert
                            onCheckedChanged: _appSettings.showOverwriteAlert = checked
                            horizontalAlignment: HorizontalAlignment.Right
                            verticalAlignment: VerticalAlignment.Center
                            navigation.focusPolicy: NavigationFocusPolicy.Focusable
                        }
                    }
                    TextArea {
                        editable: false
                        text: qsTr("A Folder, Notes or Tasks, is in Conflict when one of its items has already been sent to OneNote and the online version is newer than the one on the device. When the older Folder from the device is uploaded it OVERWRITES the newer items on OneNote. ATTENTION: Important data might be lost when uploading folders in Conflict.")
                        textFormat: TextFormat.Plain
                        textStyle.fontSize: FontSize.XSmall
                        bottomPadding: 0.0
                        topPadding: 0.0
                        leftPadding: 0.0
                        rightPadding: 0.0
                        focusHighlightEnabled: false
                        navigation.focusPolicy: NavigationFocusPolicy.NotFocusable
                    }
                }
            }
        }
    }
}