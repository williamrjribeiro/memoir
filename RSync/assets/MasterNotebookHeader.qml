import bb.cascades 1.4

Container {
    Label { 
        text: qsTr("Remember items are uploaded to OneNote's Notebook:")
        textFormat: TextFormat.Plain
        textStyle.fontSize: FontSize.XSmall
        bottomMargin: 0
    }
    Label {
        id: lblMBName
        text: "BB10 Notes by Memoir"
        textStyle.fontStyle: FontStyle.Default
        textStyle.fontWeight: FontWeight.Bold
        textStyle.fontSize: FontSize.Small
        textFormat: TextFormat.Plain
        topMargin: 0
    }
}
