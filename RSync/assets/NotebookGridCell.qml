import bb.cascades 1.4

CustomListItem {
    id: rootContainer
    dividerVisible: false
    preferredWidth: prefWidth
    preferredHeight: prefHeight
    bottomMargin: ui.sdu(.5)
    
    property variant offLineColor: Color.create("#4A0F5B");
    property variant outdatedColor: Color.create("#442B79");
    property variant uploadingColor: Color.create("#BD94C8");
    property variant updatedColor: Color.create("#7A3C8B");
    property variant invalidColor: Color.create("#B24977");
    //property variant fontColor: Color.White;
    
    property real prefWidth: 0.0
    property real prefHeight: ui.sdu(17.0) //163.0
    property real halfWidth: prefWidth / 2.0
    property real halfHeight: prefHeight / 2.0
    property real tscale: .98
    property real trotate: 1.5
    
    function onDataChanged(newData){
        //console.log("[NotebookGridCell.onDataChanged] id: "+ListItemData.id+", new.statusLabel: "+newData.statusLabel+", old.statusLabel: "+ListItemData.statusLabel);
        lblStatus.text = translateStatusLabel(newData.statusLabel);
        animatedColor.background = statusColor(newData.statusLabel);
        labels.scaleX = 1.0;
        labels.scaleY = 1.0;
        slideInAnimation.play();
    }
    
    function translateStatusLabel(text) {
        switch (text) {
            case "offline":
                return qsTr("Tap to Upload");
            case "outdated":
                return qsTr("Outdated");
            case "uploading":
                return qsTr("Uploading");
            case "updated":
                return qsTr("Updated");
            case "invalid":
                return qsTr("Conflict!");
        }
    }
    
    function statusColor(statusLabel) {
        switch (statusLabel) {
            case "offline":
                return offLineColor;
            case "outdated":
                return outdatedColor;
            case "uploading":
                return uploadingColor;
            case "updated":
                return updatedColor;
            case "invalid":
                return invalidColor;
        }
    }
    
    Container {
        id: staticColor
        bottomMargin: ui.sdu(2.0)
        
        preferredWidth: prefWidth
        preferredHeight: prefHeight    
        
        layout: AbsoluteLayout {}
        
        navigation{
            defaultHighlightEnabled: true
            focusPolicy: NavigationFocusPolicy.Focusable
            onWantsHighlightChanged: {
                console.log("[NotebookGridCell.navigation.onWantsHighlightChanged] name: " + ListItemData.name + ", wantsHighlight: " + wantsHighlight);
                if(wantsHighlight){
                    labels.scaleX = 1.03;
                    labels.scaleY = 1.03;
                }
                else {
                    labels.scaleX = 1.0;
                    labels.scaleY = 1.0;
                }
            }
        }
        
        Container {
            id: animatedColor
            preferredWidth: prefWidth
            preferredHeight: prefHeight
            scaleX: 0.0
            implicitLayoutAnimationsEnabled: true;
            layoutProperties: AbsoluteLayoutProperties {
                positionX: 0.0
                positionY: 0.0
            }
            animations: [
                ScaleTransition {
                    id: slideInAnimation
                    fromX: 0.0
                    toX: 1.0
                    duration: 650.0
                    easingCurve: StockCurve.BounceOut
                    onEnded: {
                        //console.log("[slideInAnimation.onEnded] statusLabel: "+ListItemData.statusLabel);
                        staticColor.background = statusColor(ListItemData.statusLabel);
                    }
                }
            ]
        }
        
        Container {
            id: labels
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Fill
            implicitLayoutAnimationsEnabled: false;
            
            topPadding: ui.sdu(1.0)
            leftPadding: ui.sdu(1.0)
            rightPadding: ui.sdu(1.0)
            bottomPadding: ui.sdu(2.0)
            
            ScrollView {
                bottomMargin: 0.0
                scrollViewProperties.scrollMode: ScrollMode.Horizontal
                scrollViewProperties.pinchToZoomEnabled: false
                minHeight: ui.sdu(5.0)
                Label {
                    id: lblNotebookName
                    text: ListItemData.name
                    textStyle.color: Color.White;
                    textStyle.fontWeight: FontWeight.Bold
                    textStyle.fontSize: FontSize.Medium
                    touchPropagationMode: TouchPropagationMode.None
                }
            }
            Label {
                id: lblNoteCount
                text: ListItemData.notesCount
                textStyle.fontStyle: FontStyle.Italic
                textStyle.fontSize: FontSize.XSmall
                textStyle.color: Color.White;
                topMargin: 0.0
            }
            Container {
                topMargin: ui.sdu(2.0)
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                horizontalAlignment: HorizontalAlignment.Fill
                Label {
                    id: lblStatus
                    textStyle.fontSize: FontSize.Small
                    textStyle.color: Color.White;
                    textStyle.fontWeight: FontWeight.Normal
                    textStyle.textAlign: TextAlign.Center
                }
                ActivityIndicator {
                    running: ListItemData.statusLabel == "uploading"
                    visible: ListItemData.statusLabel == "uploading"
                    scaleX: 1.0
                    scaleY: 1.0
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                }
            }
        }
    }
    onCreationCompleted: {
        //console.log("[NotebookGridCell.onCreationCompleted] id: "+ListItemData.id);
        staticColor.background = statusColor(ListItemData.statusLabel);
        animatedColor.background = staticColor.background;
        lblStatus.text = translateStatusLabel(ListItemData.statusLabel);
        ListItem.dataChanged.connect(onDataChanged);
    }
    onTouch: {
        //console.log("localX: "+event.localX+", localY: "+event.localY+", propagationPhase: "+event.propagationPhase)
        // Interesting way for not adding multiple checks on the IF statement...
        if (["uploading","updated"].indexOf(ListItemData.statusLabel) >= 0 ) {
            return;
        }
        
        if (event.isDown()) {
            rootContainer.scaleX = tscale;
            rootContainer.scaleY = tscale;
            
            // top right of square > spin counter clock-wise
            if (event.localX >= rootContainer.halfWidth && event.localY <= rootContainer.halfHeight) {
                rootContainer.rotationZ = - trotate;
            }
            // bottom left of square > spin clock-wise
            else if (event.localX < rootContainer.halfWidth && event.localY > rootContainer.halfHeight) {
                rootContainer.rotationZ = - trotate;
            }
            // top left of square > spin clock-wise
            else if (event.localX < rootContainer.halfWidth && event.localY <= rootContainer.halfHeight) {
                rootContainer.rotationZ = trotate;
            }
            // bottom right of square > spin clock-wise
            else if (event.localX > rootContainer.halfWidth && event.localY > rootContainer.halfHeight) {
                rootContainer.rotationZ = trotate;
            }
        } else if (event.isUp() || event.isCancel()) {
            rootContainer.scaleX = 1.0;
            rootContainer.scaleY = 1.0;
            rootContainer.rotationZ = 0.0;
        }
    }
}
