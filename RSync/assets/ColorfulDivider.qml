import bb.cascades 1.4

Container {
    property variant color: Color.Gray
    property real margins: ui.sdu(2.0)
    background: color
    preferredHeight: 2.0
    preferredWidth: Number.MAX_VALUE
    topMargin: margins
    bottomMargin: margins
    navigation.focusPolicy: NavigationFocusPolicy.NotFocusable
}
