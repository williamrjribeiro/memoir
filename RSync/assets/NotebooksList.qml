import bb.cascades 1.4
import bb.device 1.3
import bb.system 1.2

import "js/onenoteapi.js" as ONC

Page {
    id: rootPage
    signal emitTokenNeeded
    signal emitMasterNotebookIdNeeded;
    property variant selectedNotebook: null
    property variant selectedIndexPath: null
    property bool isUploadAll: false
    
    property alias actionItemUploadAllEnabled: acitUploadAll.enabled;
    
    function canBeUploaded(rememberNotebook){
        return rememberNotebook && ["uploading","updated"].indexOf(rememberNotebook.statusLabel) < 0;
    }
    
    function syncNotebook(indexPath){
        var rememberNotebook = _rememberModel.notebooks.data(indexPath);
        
        if(! canBeUploaded(rememberNotebook))
            return;
        
        console.log("[NotebooksList.syncNotebook] id: "+rememberNotebook.id+", name: "+rememberNotebook.name+", indexPath: "+indexPath);
        
        if(!isOverwriteNeeded(rememberNotebook, indexPath)){
            if(validateNotebookName(rememberNotebook, indexPath)){
                handleUploadNotebook(rememberNotebook, indexPath);
            }
        }
    }
    
    function isOverwriteNeeded(rememberNotebook, indexPath){
        console.log("[NotebooksList.isOverwriteNeeded] name: " + rememberNotebook.name + ", showOverwriteAlert: " + _appSettings.showOverwriteAlert + ", rememberNotebook.statusLabel: " + rememberNotebook.statusLabel);
        //var needed = ["outdated","invalid"].indexOf(rememberNotebook.statusLabel) >= 0;
        var needed = "invalid" == rememberNotebook.statusLabel;
        
        if(!needed){
            return false;
        }
        if(_appSettings.showOverwriteAlert){
            selectedNotebook = rememberNotebook;
            selectedIndexPath = indexPath;
            
            sysdConfirmOverwrite.show();
            
            return true;
        }
        return false;
    }
    
    function validateNotebookName(rememberNotebook, indexPath){
        console.log("[NotebooksList.validateNotebookName] name: "+rememberNotebook.name+", showFixInvalidNamesAlert: "+_appSettings.showFixInvalidNamesAlert);
        var isValidName = ONC.facade.isValidName(rememberNotebook.name);
        
        if(isValidName){
            return true;
        }
        
        if(_appSettings.showFixInvalidNamesAlert){
            selectedNotebook = rememberNotebook;
            selectedIndexPath = indexPath;
            
            sysdInvalidName.body = qsTr("The Notebook name '") + (rememberNotebook.name) + qsTr("' is invalid. Fix name and proceed?");
            sysdInvalidName.show();
            
            return false;
        }
        fixNotebookName(rememberNotebook);
        return true;
    }
    
    function fixNotebookName(rememberNotebook){
        var fixed = ONC.facade.fixName(rememberNotebook.name);
        rememberNotebook.name = fixed;
        
        if(selectedNotebook)
            selectedNotebook = rememberNotebook;
        
        console.log("[NotebooksList.fixNotebookName] rememberNotebook.name: "+rememberNotebook.name);
        return rememberNotebook;
    }
    
    function handleUploadNotebook(rememberNotebook, indexPath){
        console.log("[NotebooksList.handleUploadNotebook] name: "+ rememberNotebook.name +", showUploadAlert: "+ _appSettings.showUploadAlert +", indexPath: "+ indexPath +", countCompletedTasks: "+rememberNotebook.countCompletedTasks);
        if(_appSettings.showUploadAlert){
            selectedNotebook = rememberNotebook;
            selectedIndexPath = indexPath;
            
            sysdConfirmUpload.body = qsTr("The Remember Folder '") + (rememberNotebook.name) + qsTr("' will be added as a Section of the Notebook 'BB10 Notes by Memoir' on OneNote. Proceed?");
            sysdConfirmUpload.show();
        }
        else if (rememberNotebook.countCompletedTasks > 0)
            handleUploadCompletedTasks(rememberNotebook, indexPath);
        else
            uploadNotebook(rememberNotebook, indexPath);
    }
    
    function handleUploadCompletedTasks(rememberNotebook, indexPath){
        console.log("[NotebooksList.handleUploadCompletedTasks] name: "+rememberNotebook.name+", showUploadCompletedTasksAlert: "+ _appSettings.showUploadCompletedTasksAlert +", countCompletedTasks: "+rememberNotebook.countCompletedTasks);
        if( rememberNotebook.countCompletedTasks > 0 && _appSettings.showUploadCompletedTasksAlert){
            selectedNotebook = rememberNotebook;
            selectedIndexPath = indexPath;
            
            sysdConfirmUploadCompletedTasks.body = qsTr("There are ") + (rememberNotebook.countCompletedTasks) + qsTr(" completed Tasks on the selected folder.");
            sysdConfirmUploadCompletedTasks.show();
        }
        else
            uploadNotebook(rememberNotebook, indexPath);
    }
    
    function uploadNotebook(rememberNotebook, indexPath){
        console.log("[NotebooksList.uploadNotebook] name: "+rememberNotebook.name+", indexPath: "+indexPath+", id: "+rememberNotebook.id);
        
        selectedNotebook = null;
        selectedIndexPath = null;

        if(typeof ONC.facade.uploading[rememberNotebook.id] == "object"){
            console.log("[NotebooksList.uploadNotebook] NOTEBOOK IS CURRENTLY BEING UPLOADED! "+rememberNotebook.name);
        }
        else{
            _rememberModel.updateNotebookByIndex({statusLabel: "uploading"}, indexPath);
            
            var c = new ONC.facade.controller.SyncRememberNotebookCmd({
                    rememberNotebook: rememberNotebook,
                    successCb: uploadDone,
                    failCb: uploadFail
            });
        
            ONC.facade.uploading[rememberNotebook.id] = {
                command: c,
                indexPath: indexPath,
                oldStatus: rememberNotebook.statusLabel
            };
            c.execute(_appSettings);
        }
    }
    
    function uploadDone(data){
        console.log("[NotebooksList.uploadDone] data.id: "+data.id+", data.name: "+data.name);
        var obj = ONC.facade.uploading[data.id];
        _rememberModel.updateNotebookByIndex({statusLabel: "updated"}, obj.indexPath);
        delete ONC.facade.uploading[data.id];
    }
    
    function uploadFail(data){
        var obj = ONC.facade.uploading[data.id];
        
        console.log("[NotebooksList.uploadFail] data.id: "+data.id+", isDoneCreatingPages:"+obj.command.isDoneCreatingPages() + ", name: " + data.name);
        
        if(obj.command.isDoneCreatingPages()){
            _rememberModel.updateNotebookByIndex({statusLabel: obj.oldStatus}, obj.indexPath);
            delete ONC.facade.uploading[data.id];
            systMessages.body = qsTr("Failed to upload Notebook '")+data.name+"'. Please try again later or contact support.";
            systMessages.show();
        }
    }
    
    function uploadAll(){
        var l = _rememberModel.notebooks.size();
        console.log("[NotebooksList.uploadAll] _rememberModel.notebooks.size: "+l);
        
        if( _appSettings.showUploadCompletedTasksAlert && false === isUploadAll){
            isUploadAll = true;
            sysdConfirmUploadCompletedTasks.body = "";
            sysdConfirmUploadCompletedTasks.show();
        }
        else {
            isUploadAll = false;
            for(var i = 0; i < l; i++){
                var indexPath = [i],
                rememberNotebook = _rememberModel.notebooks.data(indexPath);
                if(! canBeUploaded(rememberNotebook))
                    continue;
                if(! ONC.facade.isValidName(rememberNotebook.name))
                    rememberNotebook = fixNotebookName(rememberNotebook);
                uploadNotebook(rememberNotebook, indexPath);
            }
        }
    }
    
    content: Container {
        topPadding: ui.sdu(1)
        leftPadding: ui.sdu(1)
        rightPadding: ui.sdu(1)
        verticalAlignment: VerticalAlignment.Fill
        horizontalAlignment: HorizontalAlignment.Fill
        layout: DockLayout {
        }
        ControlDelegate {
            id: cdl_emptyGrid
            delegateActive: false
            sourceComponent: ComponentDefinition {
                Container {
                    verticalAlignment: VerticalAlignment.Fill
                    horizontalAlignment: HorizontalAlignment.Fill
                    layout: DockLayout {
                    }
                    background: Color.Transparent
                    Container {
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Fill
                        background: Color.Transparent
                        ImageView {
                            imageSource: "asset:///imgs/remember_app_icon.png"
                            horizontalAlignment: HorizontalAlignment.Center
                            verticalAlignment: VerticalAlignment.Center
                        }
                        Label {
                            text: qsTr("No Notes found on Remember app\nNothing to upload")
                            multiline: true
                            textFormat: TextFormat.Plain
                            textStyle.textAlign: TextAlign.Center
                            horizontalAlignment: HorizontalAlignment.Fill
                            textStyle.fontSize: FontSize.Medium
                        }
                        Label {
                            text: qsTr("Tasks are not supported yet.")
                            textStyle.fontSize: FontSize.Small
                            textStyle.textAlign: TextAlign.Center
                            horizontalAlignment: HorizontalAlignment.Fill
                            textStyle.fontStyle: FontStyle.Italic
                        }
                    }
                }
            }
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill
        }
        ControlDelegate {
            id: cdl_grid
            delegateActive: false
            sourceComponent: ComponentDefinition {
                ListView {
                    id: livNotebooks
                    // ListItemComponent is on a scope of its own (it can't see anything else from this QML).
                    // It can only access its parent ListView (livNotebooks) and all its properties
                    // Therefore, to expose anything we want to ListItemComponent, we must create alias/property on ListView.
                    property alias displayWidth: displayInfo.pixelSize.width
                    
                    scrollRole: ScrollRole.Main
                    
                    dataModel: _rememberModel.notebooks
                    /*dataModel: ArrayDataModel {
                     id: fake
                     }
                     onCreationCompleted: {
                     for(var i = 0; i < 10; i++){
                     fake.append({
                     id: i,
                     name: "Notebook " + i,
                     notesCount: i,
                     lastModified: "",
                     entries: [],
                     statusLabel: "offline"
                     });
                     }
                     }*/
                    
                    horizontalAlignment: HorizontalAlignment.Fill
                    verticalAlignment: VerticalAlignment.Fill
                    
                    leadingVisualSnapThreshold: 1.0
                    leadingVisual: MasterNotebookHeader {}
                    
                    listItemComponents: [
                        ListItemComponent {
                            NotebookGridCell {
                                id: listItem
                                // DisplayInfo Object is 'too fat' to be on a ListItemComponent so we inject the value we want in it for faster lists
                                prefWidth: listItem.ListItem.view.displayWidth
                            }
                        }
                    ]
                    
                    onTriggered: {
                        if(! ONC.facade.isAccessTokenValid()){
                            emitTokenNeeded();
                            return;
                        }
                        else if(_appSettings.masterNotebookId.length <= 0){
                            emitMasterNotebookIdNeeded();
                            return;
                        }
                        
                        syncNotebook(indexPath);
                    }
                    
                    attachedObjects: [
                        DisplayInfo {
                            id: displayInfo
                        }
                    ]
                }
            }
        }
        onCreationCompleted: {
            _rememberModel.notebooks.itemsChanged.connect(onItemsChanged);
            var empty = _rememberModel.notebooks.isEmpty();
            
            console.log("[NotebooksList.onCreationCompleted] showUploadAlert: "+_appSettings.showUploadAlert+", showFixInvalidNamesAlert: "+_appSettings.showFixInvalidNamesAlert+", empty: "+empty);
            
            cdl_emptyGrid.delegateActive = empty;
            cdl_grid.delegateActive = ! empty;
        }
        
        function onItemsChanged(eChangeType, indexMapper){
            if(typeof(_rememberModel) == "undefined"){
                console.log("[NotebooksList.onItemsChanged] _rememberModel NOT FOUND!");
                return;
            }
            var empty = _rememberModel.notebooks.isEmpty();
            
            console.log("[NotebooksList.onItemsChanged] empty: "+empty);
            
            cdl_emptyGrid.delegateActive = empty;
            cdl_grid.delegateActive = ! empty;
        }
        
        attachedObjects: [
            SystemDialog {
                id: sysdConfirmUpload
                title: qsTr("Proceed with upload?")
                rememberMeChecked: false
                rememberMeText: qsTr("Don't ask me again. Always upload.")
                includeRememberMe: true
                onFinished: {
                    console.log("[sysdConfirmUpload.onFinished] rememberMeSelection: "+sysdConfirmUpload.rememberMeSelection());
                    
                    if(sysdConfirmUpload.rememberMeSelection()){
                        _appSettings.showUploadAlert = false;
                    }
                    
                    if (sysdConfirmUpload.result == SystemUiResult.ConfirmButtonSelection){
                        handleUploadCompletedTasks(selectedNotebook, selectedIndexPath);
                    }
                }
            },
            SystemDialog {
                id: sysdInvalidName
                title: qsTr("Fix invalid names?")
                rememberMeChecked: false
                rememberMeText: qsTr("Don't ask me again. Always fix names.")
                includeRememberMe: true
                onFinished: {
                    console.log("[sysdInvalidName.onFinished] rememberMeSelection: "+sysdInvalidName.rememberMeSelection());
                    
                    if(sysdInvalidName.rememberMeSelection()){
                        _appSettings.showFixInvalidNamesAlert = false;
                    }
                    
                    if (sysdInvalidName.result == SystemUiResult.ConfirmButtonSelection){
                        fixNotebookName(selectedNotebook);
                        handleUploadNotebook(selectedNotebook, selectedIndexPath);
                    }
                }
            },
            SystemDialog {
                id: sysdUploadAll
                title: qsTr("Proceed with upload of all Folders?")
                body: qsTr("Invalid names will be fixed automatically.")
                rememberMeChecked: false
                rememberMeText: qsTr("Don't ask me again. Always upload all.")
                includeRememberMe: true
                onFinished: {
                    console.log("[sysdUploadAll.onFinished] rememberMeSelection: "+sysdUploadAll.rememberMeSelection());
                    
                    if(sysdUploadAll.rememberMeSelection()){
                        _appSettings.showUploadAllAlert = false;
                    }
                    
                    if (sysdUploadAll.result == SystemUiResult.ConfirmButtonSelection){
                        uploadAll();
                    }
                }
            },
            SystemDialog {
                id: sysdConfirmOverwrite
                title: qsTr("Proceed with OVERWRITE?")
                body: qsTr("Newer versions of some items of this Folder are available on OneNote. If you choose to continue, the older items will overwrite the newer Notes on OneNote. ATTENTION: Important data might be lost. Continue?");
                rememberMeChecked: false
                rememberMeText: qsTr("Don't ask me again. Always overwrite.")
                includeRememberMe: true
                onFinished: {
                    console.log("[sysdConfirmOverwrite.onFinished] rememberMeSelection: "+sysdConfirmOverwrite.rememberMeSelection());

                    if(sysdConfirmOverwrite.rememberMeSelection()){
                        _appSettings.showOverwriteAlert = false;
                    }

                    if (sysdConfirmOverwrite.result == SystemUiResult.ConfirmButtonSelection){
                        if(validateNotebookName(selectedNotebook, selectedIndexPath)){
                            handleUploadNotebook(selectedNotebook, selectedIndexPath);
                        }
                    }
                }
            },
            SystemDialog {
                id: sysdConfirmUploadCompletedTasks
                title: qsTr("Upload Completed Tasks?")
                rememberMeChecked: false
                rememberMeText: qsTr("Don't ask me again. Remember my decision.")
                includeRememberMe: true
                onFinished: {
                    if(sysdConfirmUploadCompletedTasks.rememberMeSelection()){
                        _appSettings.showUploadCompletedTasksAlert = false;
                    }
                    
                    _appSettings.uploadCompletedTasks = (sysdConfirmUploadCompletedTasks.result == SystemUiResult.ConfirmButtonSelection);
                    
                    console.log("[sysdConfirmUploadCompletedTasks.onFinished] rememberMeSelection: "+sysdConfirmUploadCompletedTasks.rememberMeSelection() + ", uploadCompletedTasks: " + _appSettings.uploadCompletedTasks);
                    
                    if( true === isUploadAll ) {
                        uploadAll();
                    }
                    else
                        uploadNotebook(selectedNotebook, selectedIndexPath);
                }
                confirmButton.label: qsTr("Yes")
                cancelButton.label: qsTr("No")
            },
            SystemToast {
                id: systMessages
                button{
                    enabled: true
                    label: qsTr("OK")
                }
            }
        ]
    }
    actions: [
        ActionItem {
            id: acitUploadAll
            enabled: false
            title: qsTr("Upload All")
            ActionBar.placement: ActionBarPlacement.Signature
            imageSource: "asset:///imgs/icon_upload_all.png"
            onTriggered: {
                console.log("[uploadAll.onTriggered]");
                if(! ONC.facade.isAccessTokenValid()){
                    emitTokenNeeded();
                }
                else{
                    console.log("[uploadAll.onTriggered] showUploadAllAlert: " + _appSettings.showUploadAllAlert);
                    if(_appSettings.showUploadAllAlert){
                        sysdUploadAll.show();
                    }
                    else{
                        uploadAll();
                    }
                }
            }
        },
        ActionItem {
            ActionBar.placement: ActionBarPlacement.InOverflow
            id: acitRefresh
            title: qsTr("Check Notes & Tasks")
            imageSource: "asset:///imgs/icon_refresh.png"
            onTriggered: {
                if(! ONC.facade.isAccessTokenValid()){
                    _rememberModel.refresh();
                    emitTokenNeeded();
                }
                else{
                    //console.log("[checkItems.onTriggered] _rememberModel.countSyncedItems: " + _rememberModel.countSyncedItems);
                    
                    var tries = 1;
                    
                    function checkOneNote(){
                        
                        systMessages.body = qsTr("Checking... " + tries);
                        systMessages.show();
                        
                        var c = new ONC.facade.controller.RequestAssetsCmd({ 
                                endpoint: "pages",
                                args: "?filter=parentNotebook/id eq '" + _appSettings.masterNotebookId + "'&orderby=lastModifiedTime&select=id,title,lastModifiedTime,createdTime&count=true&expand=parentNotebook(select=id,name),parentSection(select=id,name)",
                                successCb: function(pages){
                                    var l = pages.length;

                                    // Sometimes all Pages are not retrieves from a Notebook. If not, try 3 times to get all pages!
                                    if(l < _rememberModel.countSyncedItems){
                                        console.log("[checkItems.onTriggered.checkOneNote] @#$ FUCK $#@ Didn't find all Pages of Masternotebook!? Retrying... _rememberModel.countSyncedItems: " + _rememberModel.countSyncedItems + ", pages.length: " + l + ", tries: " + tries);
                                        if(tries < 3){
                                            ++tries;
                                            checkOneNote();
                                            return;
                                        }
                                        else 
                                            this.failCb();
                                    }
                                    
                                    var pageIds = {};
                                    for(var i = 0; i < l; i++){
                                        var p = pages[i];
                                        pageIds[p.id] = p;
                                        //console.log("[refreshOneNote.onTriggered.successCb] title: "+p.title+", createdTime: "+p.createdTime+", lastModifiedTime: "+p.lastModifiedTime+", id: "+p.id);
                                    }
                                    
                                    systMessages.body = qsTr("Check completed");
                                    systMessages.show();
                                    
                                    _rememberModel.refresh(pageIds);
                                },
                                failCb: function(){
                                    console.log("[refreshOneNote.onTriggered.getMasterNotebookPages.failCb] @#$ FUCK $#@ couldn't get MasterNotebook Pages...");
                                    
                                    if(tries < 3){
                                        ++tries;
                                        checkOneNote();
                                        return;
                                    }
                                    
                                    systMessages.body = qsTr("Something went wrong. Try again later or contact support.");
                                    systMessages.show();
                                }
                        });
                        c.execute();
                    }
                    
                    checkOneNote();
                }
            }
        }
    ]
}
