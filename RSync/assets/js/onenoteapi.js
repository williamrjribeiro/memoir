//*************************************************** //
//OneNote Command API - Interfaces the REST API via JavaScript
//*************************************************** //

.pragma library // uncomment this ONLY for using with Qt/Cascades. Beaks Browsers.

var facade = function() {
    Function.prototype.bind = function(oThis) {
        if (typeof this !== 'function') {
            // closest thing possible to the ECMAScript 5
            // internal IsCallable function
            throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
        }

        var aArgs = Array.prototype.slice.call(arguments, 1),
            fToBind = this,
            fNOP = function() {},
            fBound = function() {
                return fToBind.apply(this instanceof fNOP ?
                    this :
                    oThis,
                    aArgs.concat(Array.prototype.slice.call(arguments)));
            };

        if (this.prototype) {
            // Function.prototype don't have a prototype property
            fNOP.prototype = this.prototype;
        }
        fBound.prototype = new fNOP();

        return fBound;
    };

    // http://phrogz.net/JS/classes/OOPinJS2.html
    Function.prototype.inheritsFrom = function(parentClassOrObject) {
        if (parentClassOrObject.constructor == Function) {
            //Normal Inheritance
            this.prototype = new parentClassOrObject();
            this.prototype.constructor = this;
            this.prototype.parent = parentClassOrObject.prototype;
        } else {
            //Pure Virtual Inheritance
            this.prototype = parentClassOrObject;
            this.prototype.constructor = this;
            this.prototype.parent = parentClassOrObject;
        }
        return this;
    };

    var _accessToken = "",
        _serviceClientId = "0000000040156DA8",
        _appSettings = null,
        _rememberModel = null,
        _uploader = null,
        _timer = null,
        _taskLabels = null,
        _serviceAPIUrl = "https://www.onenote.com/api/v1.0/me/notes",
        _serviceTokenRequestUrl = "https://login.live.com/oauth20_authorize.srf?client_id=0000000040156DA8&display=touch&scope=office.onenote_update&response_type=token&redirect_uri=https://login.live.com/oauth20_desktop.srf",
        _fixUnit = function(n) {
            switch (n) {
                // Pixel values where converted by OneNote itself!
                case "1":
                    return "7.5px";
                case "2":
                    return "10px";
                case "3":
                    return "12px";
                case "4":
                    return "13.5px";
                case "5":
                    return "18px";
                case "6":
                    return "24px";
                case "7":
                    return "36px";
            }
            return "11px"; // default return value
        },
        _TASKLIST_PAGE_ID = "TaskListPageID",
        _HTML_ENTRY = "html",
        _PLAIN_ENTRY = "plain",
        _OFFLINE_STATUS = "offline",
        _OUTDATED_STATUS = "outdated",
        _UPLOADING_STATUS = "uploading",
        _UPDATED_STATUS = "updated",
        _INVALID_STATUS = "invalid",
        _VALID_REMEMBER_ENTRY_TYPES = [_HTML_ENTRY, _PLAIN_ENTRY],
        _POST_OPERATION = 4,
        _CUSTOM_OPERATION = 6,
        _NOT_ACTIONABLE = 1, // taskStatus: NotActionable
        _NOT_COMPLETED = 2, // taskStatus: NotCompleted
        _COMPLETED = 4, // taskStatus: Completed
        _DECODE_CHARS = {
            quot: 34,
            amp: 38,
            lt: 60,
            gt: 62,
            nbsp: 160,
            copy: 169,
            reg: 174,
            deg: 176,
            frasl: 47,
            trade: 8482,
            euro: 8364,
            Agrave: 192,
            Aacute: 193,
            Acirc: 194,
            Atilde: 195,
            Auml: 196,
            Aring: 197,
            AElig: 198,
            Ccedil: 199,
            Egrave: 200,
            Eacute: 201,
            Ecirc: 202,
            Euml: 203,
            Igrave: 204,
            Iacute: 205,
            Icirc: 206,
            Iuml: 207,
            ETH: 208,
            Ntilde: 209,
            Ograve: 210,
            Oacute: 211,
            Ocirc: 212,
            Otilde: 213,
            Ouml: 214,
            times: 215,
            Oslash: 216,
            Ugrave: 217,
            Uacute: 218,
            Ucirc: 219,
            Uuml: 220,
            Yacute: 221,
            THORN: 222,
            szlig: 223,
            agrave: 224,
            aacute: 225,
            acirc: 226,
            atilde: 227,
            auml: 228,
            aring: 229,
            aelig: 230,
            ccedil: 231,
            egrave: 232,
            eacute: 233,
            ecirc: 234,
            euml: 235,
            igrave: 236,
            iacute: 237,
            icirc: 238,
            iuml: 239,
            eth: 240,
            ntilde: 241,
            ograve: 242,
            oacute: 243,
            ocirc: 244,
            otilde: 245,
            ouml: 246,
            divide: 247,
            oslash: 248,
            ugrave: 249,
            uacute: 250,
            ucirc: 251,
            uuml: 252,
            yacute: 253,
            thorn: 254,
            yuml: 255,
            lsquo: 8216,
            rsquo: 8217,
            sbquo: 8218,
            ldquo: 8220,
            rdquo: 8221,
            bdquo: 8222,
            dagger: 8224,
            Dagger: 8225,
            permil: 8240,
            lsaquo: 8249,
            rsaquo: 8250,
            spades: 9824,
            clubs: 9827,
            hearts: 9829,
            diams: 9830,
            oline: 8254,
            larr: 8592,
            uarr: 8593,
            rarr: 8594,
            darr: 8595,
            hellip: 133,
            ndash: 150,
            mdash: 151,
            iexcl: 161,
            cent: 162,
            pound: 163,
            curren: 164,
            yen: 165,
            brvbar: 166,
            brkbar: 166,
            sect: 167,
            uml: 168,
            die: 168,
            ordf: 170,
            laquo: 171,
            not: 172,
            shy: 173,
            macr: 175,
            hibar: 175,
            plusmn: 177,
            sup2: 178,
            sup3: 179,
            acute: 180,
            micro: 181,
            para: 182,
            middot: 183,
            cedil: 184,
            sup1: 185,
            ordm: 186,
            raquo: 187,
            frac14: 188,
            frac12: 189,
            frac34: 190,
            iquest: 191,
            Alpha: 913,
            alpha: 945,
            Beta: 914,
            beta: 946,
            Gamma: 915,
            gamma: 947,
            Delta: 916,
            delta: 948,
            Epsilon: 917,
            epsilon: 949,
            Zeta: 918,
            zeta: 950,
            Eta: 919,
            eta: 951,
            Theta: 920,
            theta: 952,
            Iota: 921,
            iota: 953,
            Kappa: 922,
            kappa: 954,
            Lambda: 923,
            lambda: 955,
            Mu: 924,
            mu: 956,
            Nu: 925,
            nu: 957,
            Xi: 926,
            xi: 958,
            Omicron: 927,
            omicron: 959,
            Pi: 928,
            pi: 960,
            Rho: 929,
            rho: 961,
            Sigma: 931,
            sigma: 963,
            Tau: 932,
            tau: 964,
            Upsilon: 933,
            upsilon: 965,
            Phi: 934,
            phi: 966,
            Chi: 935,
            chi: 967,
            Psi: 936,
            psi: 968,
            Omega: 937,
            omega: 969
        },
        _ENCODE_CHARS = {
            34: "quot",
            38: "amp",
            47: "frasl",
            60: "lt",
            62: "gt",
            133: "hellip",
            150: "ndash",
            151: "mdash",
            160: "nbsp",
            161: "iexcl",
            162: "cent",
            163: "pound",
            164: "curren",
            165: "yen",
            166: "brkbar",
            167: "sect",
            168: "die",
            169: "copy",
            170: "ordf",
            171: "laquo",
            172: "not",
            173: "shy",
            174: "reg",
            175: "hibar",
            176: "deg",
            177: "plusmn",
            178: "sup2",
            179: "sup3",
            180: "acute",
            181: "micro",
            182: "para",
            183: "middot",
            184: "cedil",
            185: "sup1",
            186: "ordm",
            187: "raquo",
            188: "frac14",
            189: "frac12",
            190: "frac34",
            191: "iquest",
            192: "Agrave",
            193: "Aacute",
            194: "Acirc",
            195: "Atilde",
            196: "Auml",
            197: "Aring",
            198: "AElig",
            199: "Ccedil",
            200: "Egrave",
            201: "Eacute",
            202: "Ecirc",
            203: "Euml",
            204: "Igrave",
            205: "Iacute",
            206: "Icirc",
            207: "Iuml",
            208: "ETH",
            209: "Ntilde",
            210: "Ograve",
            211: "Oacute",
            212: "Ocirc",
            213: "Otilde",
            214: "Ouml",
            215: "times",
            216: "Oslash",
            217: "Ugrave",
            218: "Uacute",
            219: "Ucirc",
            220: "Uuml",
            221: "Yacute",
            222: "THORN",
            223: "szlig",
            224: "agrave",
            225: "aacute",
            226: "acirc",
            227: "atilde",
            228: "auml",
            229: "aring",
            230: "aelig",
            231: "ccedil",
            232: "egrave",
            233: "eacute",
            234: "ecirc",
            235: "euml",
            236: "igrave",
            237: "iacute",
            238: "icirc",
            239: "iuml",
            240: "eth",
            241: "ntilde",
            242: "ograve",
            243: "oacute",
            244: "ocirc",
            245: "otilde",
            246: "ouml",
            247: "divide",
            248: "oslash",
            249: "ugrave",
            250: "uacute",
            251: "ucirc",
            252: "uuml",
            253: "yacute",
            254: "thorn",
            255: "yuml",
            913: "Alpha",
            914: "Beta",
            915: "Gamma",
            916: "Delta",
            917: "Epsilon",
            918: "Zeta",
            919: "Eta",
            920: "Theta",
            921: "Iota",
            922: "Kappa",
            923: "Lambda",
            924: "Mu",
            925: "Nu",
            926: "Xi",
            927: "Omicron",
            928: "Pi",
            929: "Rho",
            931: "Sigma",
            932: "Tau",
            933: "Upsilon",
            934: "Phi",
            935: "Chi",
            936: "Psi",
            937: "Omega",
            945: "alpha",
            946: "beta",
            947: "gamma",
            948: "delta",
            949: "epsilon",
            950: "zeta",
            951: "eta",
            952: "theta",
            953: "iota",
            954: "kappa",
            955: "lambda",
            956: "mu",
            957: "nu",
            958: "xi",
            959: "omicron",
            960: "pi",
            961: "rho",
            963: "sigma",
            964: "tau",
            965: "upsilon",
            966: "phi",
            967: "chi",
            968: "psi",
            969: "omega",
            8216: "lsquo",
            8217: "rsquo",
            8218: "sbquo",
            8220: "ldquo",
            8221: "rdquo",
            8222: "bdquo",
            8224: "dagger",
            8225: "Dagger",
            8240: "permil",
            8249: "lsaquo",
            8250: "rsaquo",
            8254: "oline",
            8364: "euro",
            8482: "trade",
            8592: "larr",
            8593: "uarr",
            8594: "rarr",
            8595: "darr",
            9824: "spades",
            9827: "clubs",
            9829: "hearts",
            9830: "diams"
        },
        _ENCODE_REGEX = /(?![^<]*>)(€|&|¡|¢|£|¤|¥|¦|§|¨|©|ª|«|¬|­|®|¯|°|±|²|³|´|µ|¶|·|¸|¹|º|»|¼|½|¾|¿|À|Á|Â|Ã|Ä|Å|Æ|Ç|È|É|Ê|Ë|Ì|Í|Î|Ï|Ð|Ñ|Ò|Ó|Ô|Õ|Ö|×|Ø|Ù|Ú|Û|Ü|Ý|Þ|ß|à|á|â|ã|ä|å|æ|ç|è|é|ê|ë|ì|í|î|ï|ð|ñ|ò|ó|ô|õ|ö|÷|ø|ù|ú|û|ü|ý|")/g;

    /***************************
     * 		OneNote API Interface
     ***************************/
    var facade = {};
    facade = {
        currentApplicationVersion: "2.0.0",
        controller: {},
        model: {
            notebooks: [],
            sections: [],
            pages: []
        },
        uploading: {}, // keep track of Notebook uploads. Key is notebookId.
        init: function _init(settings, model, uploader, timer, taskLabels) {
            console.log("[OneNoteAPI.init] settings: " + settings + ", model: " + model + ", uploader: " + uploader + ", timer: " + timer + ", taskLabels: " + taskLabels);
            _appSettings = settings;
            _accessToken = _appSettings.accessToken;
            _rememberModel = model;
            _uploader = uploader;
            _timer = timer;
            _taskLabels = taskLabels;
            return (typeof _appSettings == "object") && (typeof _rememberModel == "object") && (typeof _uploader == "object") && (typeof _timer == "object") && (typeof _taskLabels == "object");
        },
        _setSettings: function(settings) {
            _appSettings = settings;
        },
        _uploader: function(u) {
            _uploader = u;
        },
        getAccessToken: function() {
            return _accessToken;
        },
        getAccessTokenDateTime: function() {
            return _appSettings.accessTokenDateTime;
        },
        getServiceClientId: function() {
            return _serviceClientId;
        },
        getServiceAPIUrl: function() {
            return _serviceAPIUrl;
        },
        getServiceTokenRequestUrl: function() {
            return _serviceTokenRequestUrl;
        },
        getParamObjectFromURL: function _getParamObjectFromURL(aurl) {
            aurl = aurl.substring(aurl.indexOf("#") + 1);
            var result = {};
            aurl.split("&").forEach(function(part) {
                var item = part.split("=");
                result[item[0]] = decodeURIComponent(item[1]);
                //console.log("[OneNoteAPI.getParamObjectFromURL] item: " + item[0] + ", val: " + item[1])
            });
            return result;
        },
        setAccessTokenByURL: function _setAccessTokenByURL(url, isNew) {
            //console.log("[OneNoteAPI.setAccessTokenByURL] url: " + url + ", isNew: "+isNew);
            var r = facade.getParamObjectFromURL(url);
            if (r.access_token) {
                _accessToken = r.access_token;
                _appSettings.accessToken = _accessToken;
                _uploader.setAccessToken(_accessToken);
                if (isNew)
                    _appSettings.accessTokenDateTime = new Date();
            }
        },
        isAccessTokenValid: function _isAccessTokenValid() {
            var valid = false;
            if (_accessToken) {
                var msDiff = Date.now() - _appSettings.accessTokenDateTime.getTime();
                valid = (msDiff < (60 * 60 * 1000)); // Minutes * Seconds * MS = 1 hour
                console.log("[OneNoteAPI.isAccessTokenValid] msDiff: " + msDiff + ", valid: " + valid);
                if (!valid)
                    facade.resetTokens();
            }
            return valid;
        },
        resetTokens: function _resetTokens() {
            _accessToken = "";
            _appSettings.accessToken = _accessToken;
            _uploader.setAccessToken(_accessToken);
        },
        getFromModelBy: function _getFromModelBy(modelName, propName, propVal, uniqueValue) {
            //console.log("[OneNoteAPI.getFromModelBy] modelName: " + modelName + ", propName: " + propName + ", propVal: " + propVal);
            if (!facade.model[modelName])
                return null;
            var model = facade.model[modelName],
                found = [];

            for (var i = 0, l = model.length; i < l; ++i) {
                if (model[i][propName] == propVal) {
                    if (uniqueValue)
                        return facade.model[modelName][i];
                    found.push(model[i]);
                }
            }

            return uniqueValue ? null : found;
        },
        htmlPageBuilder: {
            buildPageHTMLRemoveStyle: function _buildPageHTMLRemoveStyle(str) {
                return str.replace(/<style type=\"text\/css\">(.*?)<\/style>/g, ""); // remove all inline styles
            },
            buildPageHTMLAddXMLInfo: function _buildPageHTMLEnhanceTag(str) {
                return '<?xml version="1.0" encoding="utf-8" ?>'.concat(str);
            },
            buildPageHTMLEnhanceTag: function _buildPageHTMLEnhanceTag(str) {
                return str.replace("<html>", '<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" lang="en-us">');
            },
            buildPageHTMLAddTitle: function _buildPageHTMLAddTitle(str, title, taskStatus, donePrefix) {
                if (taskStatus === _COMPLETED)
                    return str.replace("<head>", '<head><title>' + donePrefix + title + '</title>');
                else
                    return str.replace("<head>", '<head><title>' + title + '</title>');
            },
            buildPageHTMLAddMetaCreated: function _buildPageHTMLAddMetaCreated(str, lastModified) {
                return str.replace('</head>', '<meta name="created" content="' + lastModified + '" /></head>');
            },
            buildPageHTMLAddMemoirId: function _buildPageHTMLAddMemoirId(str) {
                str = str.replace('<body>', '<body><div id="memoir-content">');
                return str.replace('</body>', '</div></body>');
            },
            buildPageHTMLConvertFontTags: function _buildPageHTMLConvertFontTags(str) {
                //<font size="1" color="#ff0000">Size 1</font>
                //<span style="color:#ff0000; font-size:7.5px;">Size 1</span>
                var fixedTag = str.replace(/<font(.*?)>/gm, function(tags) {
                    var ptag = tags.replace(/\s(.*?)="(.*?)"/g, function(attr) {
                        var parts = attr.trim().replace(/\"/g, "").split("=");
                        if (parts[0] == "size") {
                            return "font-size:" + _fixUnit(parts[1]) + ";";
                        } else if (parts[0] == "color") {
                            return "color:" + parts[1] + ";";
                        }
                    });

                    return ptag.replace("<font", "<span style=\"").replace(">", "\">");
                });

                return fixedTag.replace(/<\/font>/g, "</span>");
            },
            findGeneratedId: function _findGeneratedId(desc) {
                var i = desc.indexOf('<div data-id="memoir-content"');
                if (i >= 0) {
                    var j = desc.indexOf('>', i);
                    var tag = desc.substring(i, j);
                    console.log("[OneNoteAPI.htmlPageBuilder.findGeneratedId] tag: " + tag);
                    var k = tag.indexOf(' id="');
                    var val = tag.substring(k + ' id="'.length , tag.lastIndexOf('"'));
                    return val;
                } else {
                    return "body";
                }
            },
            getMemoirContents: function _getMemoirContents(desc) {
                var i = desc.indexOf("<div id=\"memoir-content\">"),
                    lastDiv = "</div></body>";

                // Don't include the </body> or else it breaks PATCH!
                return desc.substring(i, desc.indexOf(lastDiv) + "</div>".length);
            },
            /**
             * Decodes html entities by name and number
             */
            decodeHtmlEntities: function _decodeHtmlEntities(str) {
                return str.replace(/&#?(\w+);/g, function(match, dec) {
                    if (isNaN(dec)) {
                        if (_DECODE_CHARS[dec] !== undefined) {
                            dec = _DECODE_CHARS[dec];
                        }
                    }
                    return String.fromCharCode(dec);
                });
            },
            encodeHtmlEntities: function _encodeHtmlEntities(str) {
                str = str.replace(_ENCODE_REGEX, function(match, desc) {
                    var code = match.charCodeAt(0);
                    if (_ENCODE_CHARS[code] !== undefined) {
                        match = _ENCODE_CHARS[code];
                    }
                    return "&" + match + ";";
                });

                return str.replace(String.fromCharCode(160), "&nbsp;");
            },
            buildLabel: function _buildLabel(str, dataTag, label, dateTime) {
                //console.log("[OneNoteAPI.buildPageHTML.buildLabel] dateTime: " + dateTime + ", label: " + label);
                var tag = '<br /> <p data-tag="';
                tag += dataTag;
                tag += '"><span style="font-size:12pt; font-weight:bold">';
                tag += label;
                tag += '</span><span>';
                tag += dateTime;
                tag += '</span></p></body>';
                return str.replace('</body>', tag);
            },
            buildCheckbox: function _buildCheckbox(text, taskStatus) {
                //console.log("[OneNoteAPI.buildPageHTML._buildCheckbox] text: " + text + ", taskStatus: " + taskStatus);
                var str = '<p data-tag="to-do';
                str += taskStatus === _NOT_COMPLETED ? '">' : ':completed">';
                str += '<span style="font-size:12pt">';
                str += text;
                str += '</span></p>';
                return str;
            },
            buildPageAddTaskToList: function _buildPageAddTaskToList(rememberEntry, taskListPage) {
                //console.log("[OneNoteAPI.buildPageAddTaskToList] taskStatus: " + rememberEntry.taskStatus + ", title: " + rememberEntry.title);
                var strVar = "";
                if (!taskListPage) {
                    strVar = '<?xml version="1.0" encoding="utf-8" ?>';
                    strVar += '<!DOCTYPE html> <html xmlns="http://www.w3.org/1999/xhtml" lang="en-us">';
                    strVar += '    <head>';
                    strVar += '        <title>' + _taskLabels.listTitle + '</title>';
                    strVar += '        <meta name="created" content="' + rememberEntry.lastModified + '" />';
                    strVar += '    </head>';
                    strVar += '    <body>';
                    strVar += '        <div id="memoir-content">';
                    strVar += this.buildCheckbox(rememberEntry.title, rememberEntry.taskStatus);
                    strVar += '        </div>';
                    strVar += '    </body>';
                    strVar += '</html>';
                    return strVar;
                } else {
                    strVar += '        </div>';
                    strVar += '    </body>';
                }
                return taskListPage.replace(strVar, this.buildCheckbox(rememberEntry.title, rememberEntry.taskStatus) + strVar);
            },
            buildPageTaskList: function _buildPageTaskList(plainTasks, taskListPage) {
                //console.log("[OneNoteAPI.buildPageTaskList] plainTasks.length: " + plainTasks.length + ", taskListPage: " + taskListPage);
                var temp = taskListPage;
                for (var i = 0, l = plainTasks.length; i < l; ++i) {
                    temp = facade.htmlPageBuilder.buildPageAddTaskToList(plainTasks[i], temp);
                }
                return temp;
            },
            buildPageHTML: function _buildPageHTML(rememberEntry) {
                // taskStatus = 1: NotActionable, 2: NotCompleted, 4: Completed
                console.log("[OneNoteAPI.buildPageHTML] taskStatus: " + rememberEntry.taskStatus + ", title: " + rememberEntry.title);
                //console.log(rememberEntry.description);

                if (!rememberEntry.type || _VALID_REMEMBER_ENTRY_TYPES.indexOf(rememberEntry.type) < 0) {
                    console.log("[OneNoteAPI.buildPageHTML] @#$ FUCK @#$ INVALID rememberEntry.type!");
                    throw new TypeError("INVALID rememberEntry.type!");
                }

                //console.log("[OneNoteAPI.buildPageHTML] description: "+rememberEntry.description);
                var d = this.buildPageHTMLAddXMLInfo(rememberEntry.description);
                //d = d.replace(/<style type=\"text\/css\">(.*?)<\/style>/g,""); // remove all inline styles
                d = this.buildPageHTMLRemoveStyle(d);
                //d = d.replace("<html>","<!DOCTYPE html><html xmlns=\"http:\/\/www.w3.org\/1999\/xhtml\" lang=\"en-us\">");
                d = this.buildPageHTMLEnhanceTag(d);
                //d = d.replace("<head>","<head><title>"+rememberEntry.title+"<\/title>");
                d = this.buildPageHTMLAddTitle(d, rememberEntry.title, rememberEntry.taskStatus, _taskLabels.done + " - ");
                //d = d.replace("<\/head>","<meta name=\"created\" content=\""+rememberEntry.lastModified+"\" \/> "+"<\/head>");
                d = this.buildPageHTMLAddMetaCreated(d, rememberEntry.lastModified);

                // TODO: Localize labels!
                if (rememberEntry.dueDate)
                    d = this.buildLabel(d, "critical", _taskLabels.dueDate + ": ", rememberEntry.dueDate);

                if (rememberEntry.reminder)
                    d = this.buildLabel(d, "remember-for-later", _taskLabels.reminder + ": ", rememberEntry.reminder);

                d = this.buildPageHTMLAddMemoirId(d);

                // Converts 'size="2"' to 'size="10px"' based on OneNote sizing
                // OneNote only recognizes PX units!
                //d = d.replace(/size=\"\d+\"/gm, _fixUnits);
                d = this.buildPageHTMLConvertFontTags(d);

                d = this.decodeHtmlEntities(d);
                d = this.encodeHtmlEntities(d);
                //console.log("[OneNoteAPI.buildPageHTML] d: "+d);
                return d;
            }
        },
        isValidName: function _isValidName(name) {
            console.log("[OneNoteAPI.isValidName] name: " + name);
            if (!name)
                return false;
            if (name.length > 50)
                return false;
            // illegal characters ? * \ / : < > | & # \" % ~'
            // Ref > http://stackoverflow.com/questions/12931617/check-a-textbox-for-invalid-characters-using-js-and-regular-expressions
            if (name.match("^.*?(?=[\?\*\\\\\/:<>\|&#\"%\~]).*$"))
                return false;
            return true;
        },
        fixName: function _fixName(name) {
            console.log("[OneNoteAPI.fixName] name: " + name);
            name = name.replace(/\?|\*|\\|\/|:|<|>|\||&|#|\"|%|\~/g, "");
            if (name.length > 50)
                name = name.substring(0, 50);
            return name;
        },
        initMasterNotebook: function _initMasterNotebook(initMBCB) {
            console.log("[OneNoteAPI.initMasterNotebook] initMBCB: " + (typeof initMBCB));
            // 1 - Check AppSettings.MasterNotebookId
            // 2 - IF NO ID, request all Notebooks
            // 2.1 - Get Notebook by Name  (master name)
            // 2.2 - IF NOT found, Create Master Notebook with Master Name
            // 2.3 - Save MasterNotebookId to AppSettings
            var MNPagesDone = false,
                MNSectionsDone = false,
                findMasterNotebook = function() {
                    console.log("[OneNoteAPI.initMasterNotebook.findMasterNotebook] cbf: " + (typeof cbf));
                    // Search for a notebook named "Memoir" expanding on its Sections to avoid extra REST calls
                    var c = new facade.controller.RequestAssetsCmd({
                        endpoint: "notebooks",
                        args: "?filter=name eq '" + _appSettings.masterNotebookName + "'&orderby=lastModifiedTime&select=id,name&count=false&expand=sections",
                        successCb: function(notebooks) {
                            console.log("[setUpOneNoteSheet.findMasterNotebook.successCb] notebooks.length: " + notebooks.length + ", sections.length: " + facade.model.sections.length);
                            if (notebooks.length > 0) {
                                _appSettings.masterNotebookId = notebooks[0].id;
                                MNSectionsDone = true;
                                getMasterNotebookPages();
                            } else
                                createMasterNotebook();
                        },
                        failCb: function() {
                            console.log("[setUpOneNoteSheet.findMasterNotebook.failCb] @#$ FUCK $#@!");
                        }
                    });
                    c.execute();
                },
                createMasterNotebook = function() {
                    console.log("[OneNoteAPI.initMasterNotebook.createMasterNotebook]");

                    _appSettings.masterNotebookId = "";

                    var c = new facade.controller.CreateAssetsCmd({
                        endpoint: "notebooks",
                        body: JSON.stringify({
                            "name": _appSettings.masterNotebookName
                        }),
                        successCb: function(notebook) {
                            console.log("[OneNoteAPI.initMasterNotebook.createMasterNotebook.successCb] notebook.id: " + notebook.id);
                            // Save Master Notebook OneNote ID on AppSettings
                            _appSettings.masterNotebookId = notebook.id;
                            if (typeof initMBCB === "function")
                                initMBCB();
                        },
                        failCb: function(status) {
                            console.log("[OneNoteAPI.initMasterNotebook.createMasterNotebook.failCb] status: " + status);
                        }
                    });
                    c.execute();
                },
                getNotebookSections = function() {
                    console.log("[OneNoteAPI.initMasterNotebook.getNotebookSections] masterNotebookId: " + _appSettings.masterNotebookId + ", model.sections.length: " + facade.model.sections.length);

                    function sucessCb(sections) {
                        console.log("[OneNoteAPI.initMasterNotebook.getNotebookSections.successCb] length: " + sections.length);
                        MNSectionsDone = true;
                        validateInit();
                    }

                    if (facade.model.sections.length != 0) {
                        sucessCb(facade.model.sections);
                    } else {
                        var c = new facade.controller.RequestAssetsCmd({
                            endpoint: "sections",
                            notebookId: _appSettings.masterNotebookId,
                            successCb: sucessCb,
                            failCb: function(status) {
                                console.log("[OneNoteAPI.initMasterNotebook.getNotebookSections.failCb] @#$ FUCK $#@ sections! status: " + status);
                                if (status === 404)
                                    createMasterNotebook();
                            }
                        });
                        c.execute();
                    }
                },
                getMasterNotebookPages = function _getMasterNotebookPages() {
                    console.log("[OneNoteAPI.initMasterNotebook.getMasterNotebookPages] _appSettings.masterNotebookId: " + _appSettings.masterNotebookId);
                    // Search for Pages that have parentNotebook.id == masterNotebookId. MUST EXPAND on parentNotebook!
                    var c = new facade.controller.RequestAssetsCmd({
                        endpoint: "pages",
                        args: "?filter=parentNotebook/id eq '" + _appSettings.masterNotebookId + "'&orderby=lastModifiedTime&select=id,title,lastModifiedTime,createdTime&count=true&expand=parentSection(select=id,name)",
                        successCb: function(pages) {
                            console.log("[OneNoteAPI.initMasterNotebook.getMasterNotebookPages.successCb] pages.length: " + pages.length);
                            MNPagesDone = true;
                            validateInit();
                        },
                        failCb: function() {
                            console.log("[OneNoteAPI.initMasterNotebook.getMasterNotebookPages.failCb] couldn't get MasterNotebook Pages...");
                        }
                    });
                    c.execute();
                },
                validateInit = function _validateInit() {
                    console.log("[OneNoteAPI.initMasterNotebook.validateInit] MNPagesDone: " + MNPagesDone + ", MNSectionsDone: " + MNSectionsDone + ", initMBCB: " + (typeof initMBCB));
                    if (MNPagesDone && MNSectionsDone && typeof initMBCB === "function")
                        initMBCB();
                };

            if (_appSettings.masterNotebookId.length > 0) {
                facade.model.notebooks.push({
                    id: _appSettings.masterNotebookId,
                    name: _appSettings.masterNotebookName
                });
                getNotebookSections(_appSettings.masterNotebookId);
                getMasterNotebookPages();
            } else {
                findMasterNotebook();
            }
        }
    };

    function Command(params) {
        //console.log("[Command] params: " + params);
        this.data = null;
        this.req = null;
        this.responseText = null;

        this.params = params ? params : {};
        if (!this.params.args)
            this.params.args = "";

        if (this.params.endpoint == "content") {
            if (!this.params.id)
                throw new Error("Required parameter 'id' not defined. It must be a OneNote Page Id String");
            else {
                this.params.assetName = this.params.endpoint;
                this.params.endpoint = "/pages/" + this.params.id + "/" + this.params.assetName;
                this.params.args = "?includeIDs=true";
            }
        } else {
            this.params.assetName = this.params.endpoint ? this.params.endpoint : "";
            this.params.endpoint = "/" + this.params.assetName;

            if (this.params.id)
                this.params.endpoint += "/" + this.params.id;
            else if (this.params.notebookId)
                // Some combinations will result in 404 response. E.g.: /notebooks/123/pages
                this.params.endpoint = "/notebooks/" + this.params.notebookId + this.params.endpoint;
            else if (this.params.sectionId)
                this.params.endpoint = "/sections/" + this.params.sectionId + this.params.endpoint;
        }

        /********************
        	Private variables
        ********************/
        var _result = null;

        /********************
	    	Private functions
	    ********************/
        this.getResult = function _getResult() {
            return _result;
        };

        this.done = function _done(dparams) {
            //console.log("[Command.done] dparams.result: " + dparams.result);

            _result = dparams.result;
            this.data = dparams.data;

            if (_result == "success") {
                if (this.params.successCb)
                    this.params.successCb(dparams.data);
                if (this.next)
                    this.next.execute();
            } else if (this.params.failCb && _result == "fail") {
                this.params.failCb(dparams.data);
            }
        };
    }

    Command.prototype.execute = function _execute() {
        //console.log("[OneNoteAPI.Command.execute] accessToken: " + _accessToken);
        this.sendReq();
    };

    Command.prototype.xhrFactory = function _xhrFactory() {
        return new XMLHttpRequest();
    };

    Command.prototype.sendReq = function _sendReq() {
        var req = this.xhrFactory(),
            that = this,
            url = _serviceAPIUrl + this.params.endpoint + this.params.args;

        //console.log("[Command.sendReq] params: " + JSON.stringify(this.params));
        //console.log("[Command.sendReq] url: " + url);

        req.onreadystatechange = function() {
            //console.log("[Command.sendReq.onreadystatechange] readyState: "+req.readyState);
            if (req.readyState === XMLHttpRequest.DONE) {
                //console.log("[Command.sendReq.onreadystatechange] responseText.length: "+req.responseText.length + ", status: "+ req.status);
                that.responseText = req.responseText;

                if (req.status >= 200 && req.status <= 226 && that.onSuccess) {
                    that.onSuccess(req);
                } else if ((req.status === 0 || req.status >= 400) && that.onError) {
                    // https://en.wikipedia.org/wiki/List_of_HTTP_status_codes#4xx_Client_Error
                    console.log("[Command.sendReq.onreadystatechange] statusText: " + req.statusText);
                    that.onError(req);
                }
            }
        };

        req.open(this.params.verb, url, true);

        //console.log("[Command.sendReq] Authorization: Bearer " + _accessToken.length);
        req.setRequestHeader('Authorization', 'Bearer ' + _accessToken);

        if (this.params.body) {
            //console.log("[Command.sendReq] body: " + this.params.body);
            // Only POST/pages has a a different Content-Type header. We're sending HTML code, not JSON.
            if (this.params.assetName == "pages" && this.params.verb != "PATCH")
                req.setRequestHeader('Content-Type', 'application/xhtml+xml');
            else
                req.setRequestHeader('Content-Type', 'application/json');
            req.send(this.params.body);
        } else {
            req.send(null);
        }
    };

    Command.prototype.onError = function _onError(request) {
        console.log("[OneNoteAPI.Command.onError] @#$ FUCK $#@ request.status: " + request.status + ", statusText: " + request.statusText);
        this.done({
            result: "fail",
            data: request.status
        });
    };

    // Makes the Constructor available on the facade
    facade.controller.Command = Command;

    /***************************
     * 		RequestAssetsCmd
     ***************************/
    function RequestAssetsCmd(params) {
        //console.log("[OneNoteAPI.RequestAssetsCmd]");
        Command.prototype.constructor.call(this, params);
        this.params.verb = "GET";
    }

    RequestAssetsCmd.inheritsFrom(Command);

    RequestAssetsCmd.prototype.onSuccess = function(request) {
        console.log("[OneNoteAPI.RequestAssetsCmd.onSuccess] request.responseText: " + request.responseText);
        var data;
        if (this.params.assetName == "content") {
            data = request.responseText;
        } else {
            data = JSON.parse(request.responseText).value;
            console.log("[OneNoteAPI.RequestAssetsCmd.onSuccess] " + this.params.assetName + ".length: " + data.length);
            if (this.params.assetName == "sections" && this.params.notebookId) {
                // easy way to reference its parent Notebook
                for (var i = data.length - 1; i >= 0; i--) {
                    data[i].notebookId = this.params.notebookId;
                }
            } else if (this.params.assetName == "pages" && this.params.sectionId) {
                // easy way to reference its parent Section
                for (var i = data.length - 1; i >= 0; i--) {
                    data[i].sectionId = this.params.sectionId;
                }
            } else if (this.params.assetName == "notebooks" && !this.params.id && data.length > 0) {
                var secs = data[0].sections;
                // TODO this is for when searching for Notebooks by name. Must associate the notebook id with its sections
                if (secs && secs.length > 0)
                    facade.model.sections = secs;
            }
            // TODO: this is overwritting all the previous data. Should it be appended instead?
            facade.model[this.params.assetName] = data;
        }

        this.done({
            result: "success",
            data: data
        });
    };

    facade.controller.RequestAssetsCmd = RequestAssetsCmd;

    /***************************
     * 		CreateAssetsCmd
     ***************************/
    function CreateAssetsCmd(params) {
        Command.prototype.constructor.call(this, params);
        this.params.verb = "POST";
    }

    CreateAssetsCmd.inheritsFrom(Command);

    CreateAssetsCmd.prototype.onSuccess = function(request) {
        //console.log("[OneNoteAPI.CreateAssetsCmd.onSuccess] request.responseText: " + request.responseText);
        var asset = JSON.parse(request.responseText);
        //console.log("[OneNoteAPI.CreateAssetsCmd.onSuccess] asset.id: " + asset.id + ", asset.createdTime: "+asset.createdTime);
        this.done({
            result: "success",
            data: asset
        });
    };

    facade.controller.CreateAssetsCmd = CreateAssetsCmd;

    /***************************
     * 		PatchPageCmd
     *  	FUCK: Not available in QML/JS
     *  	https://bugreports.qt.io/browse/QTBUG-38175
     *  	https://supportforums.blackberry.com/t5/Native-Development/HTTP-Patch-request/m-p/3041403/
     ***************************/
    function PatchPageCmd(params) {
        Command.prototype.constructor.call(this, params);
        this.params.verb = "PATCH";
        this.params.endpoint = "pages";
        this.params.args = "/content";
    }

    PatchPageCmd.inheritsFrom(Command);

    PatchPageCmd.prototype.onSuccess = function(request) {
        console.log("[OneNoteAPI.PatchPageCmd.onSuccess] request.status: " + request.status);
        this.responseText = "Server response: " + request.status;
        this.done({
            result: "success"
        });
    };

    facade.controller.PatchPageCmd = PatchPageCmd;

    /***************************
     * 		SyncRememberNotebookCmd
     ***************************/
    function SyncRememberNotebookCmd(params) {
        Command.prototype.constructor.call(this, params);
        this.rememberNotebook = params.rememberNotebook;
        this.nbName = this.rememberNotebook.name;
        this.oneNoteSection = null;
        this.pagesUpdated = {};
        this.pagesCreatedCount = 0;
        this.pagesPatchedCount = 0;
        this.pagesFailCount = 0;
        this.pagesToCreateCount = 0;
        this.pagesToPatchCount = 0;
        this.totalActions = 0; // pagesToCreateCount + pagesToPatchCount
        this.plainTasksCount = 0; // incremented on countActions(), decremented after added to taskListPage
        this.taskListPageAction = ""; // The Task List Page is only created if all Plain Tasks of the folder are Offline. If new Plain Tasks are added, the Task List Page is PATCHED!

        this.taskListPage = null; // All PLAIN Tasks live in 1 Page! Only 1 per Section/Notebook
        this.taskListPageONID = null; // Get the Task List Page OneNote Page Id
        // There's only 1 OneNoteUploader for all Commands and it sends evens to all Command instances. The Commands must know if the event is for them or not.
        // Every Task List Page has a unique ID which just TaskListPage + Notebook Name
        this.taskListPageUID = _TASKLIST_PAGE_ID + this.nbName;

        this.plainTasks = []; // List containing all Plain Tasks added to Task List Page

        // This will be disconnected only after isDoneCreatingPages() == true on finish()
        this.boundedCb = this.onUploaderFinished.bind(this);

        this.isUploaderBounded = false; // Only bound the Uploader when needed!
    }

    SyncRememberNotebookCmd.inheritsFrom(Command);

    SyncRememberNotebookCmd.prototype.execute = function _execute() {
        var neName = this.rememberNotebook.name;
        // 1 - look for the correspondent OneNote Section
        var oneNoteSection = facade.getFromModelBy("sections", "name", neName, true),
            foundSection = (oneNoteSection !== null),
            that = this;

        console.log("[SyncRememberNotebookCmd.execute] rememberNotebook.name: " + neName + ", foundSection: " + foundSection);

        // 1.1 - If not found, Create new Section with the Notebook Name
        if (!foundSection) {
            var c = new facade.controller.CreateAssetsCmd({
                endpoint: "sections",
                notebookId: _appSettings.masterNotebookId,
                body: JSON.stringify({
                    "name": neName
                }),
                successCb: function _createSectionSuccessCb(newSection) {
                    console.log("[SyncRememberNotebookCmd.execute._createSectionSuccessCb] newSection.id: " + newSection.id + ", rememberNotebook.name: " + neName);
                    // Add the created section to the model and to this instance.
                    facade.model.sections.push(newSection);
                    that.oneNoteSection = newSection;
                    // 2 - Sync every Entry of the Notebook
                    that.syncEntries();
                },
                failCb: function _createSectionFailCb() {
                    console.log("[SyncRememberNotebookCmd.execute._createSectionFailCb] @#$ FUCK $#@ creating Section...");
                    that.done({
                        result: "fail",
                        data: that.rememberNotebook
                    });
                }
            });
            c.execute();
        } else {
            this.oneNoteSection = oneNoteSection;
            // 2 - Sync every Entry of the Notebook
            this.syncEntries();
        }
    };

    SyncRememberNotebookCmd.prototype.countActions = function _countActions() {
        var plainTaskOfflineCount = 0;

        for (var i = 0, l = this.rememberNotebook.entries.length; i < l; ++i) {
            var entry = this.rememberNotebook.entries[i],
                s = entry.status;

            //console.log("[SyncRememberNotebookCmd.countActions] entry.type: " + entry.type + ", entry.title: " + entry.title);

            if (entry.taskStatus === _COMPLETED && _appSettings.uploadCompletedTasks === false)
                continue;

            if (entry.taskStatus === _NOT_ACTIONABLE || entry.type === _HTML_ENTRY) {
                if (s === _OFFLINE_STATUS)
                    this.pagesToCreateCount++;
                else if (s === _OUTDATED_STATUS || s === _INVALID_STATUS)
                    this.pagesToPatchCount++;
            } else {
                this.plainTasksCount++;
                this.plainTasks.push(entry);

                if (s === _OFFLINE_STATUS)
                    ++plainTaskOfflineCount;
                else if ( ! this.taskListPageONID && entry.syncInfo && entry.syncInfo.oneNotePageId) {
                    // Get the Task List Page OneNote Page Id
                    this.taskListPageONID = entry.syncInfo.oneNotePageId;
                }
            }
        }

        if(this.plainTasksCount > 0) {
            if (this.plainTasksCount === plainTaskOfflineCount) {
                this.taskListPageAction = "CREATE";
                this.pagesToCreateCount++;
            } else {
                this.taskListPageAction = "PATCH";
                this.pagesToPatchCount++;
                console.log("[SyncRememberNotebookCmd.countActions] Must PATCH Task List Page! taskListPageONID: " + this.taskListPageONID);
            }
        }

        // pagesToCreateCount + pagesToPatchCount
        this.totalActions = this.pagesToCreateCount + this.pagesToPatchCount;
        console.log("[SyncRememberNotebookCmd.countActions] rememberNotebook.entries.length: " + this.rememberNotebook.entries.length + ", totalActions: " + this.totalActions + ", pagesToCreateCount: " + this.pagesToCreateCount + ", pagesToPatchCount: " + this.pagesToPatchCount + ", plainTasksCount: " + this.plainTasksCount + ", taskListPageAction: " + this.taskListPageAction + ", nbName: " + this.nbName);
    };

    SyncRememberNotebookCmd.prototype.syncEntries = function _syncEntries() {
        console.log("[SyncRememberNotebookCmd.syncEntries] entries.length: " + this.rememberNotebook.entries.length + ", _appSettings.uploadCompletedTasks: " + _appSettings.uploadCompletedTasks + ", nbName: " + this.nbName);

        this.countActions();

        var commands = [],
            c = null;

        for (var i = 0, l = this.rememberNotebook.entries.length; i < l; ++i) {
            var entry = this.rememberNotebook.entries[i];

            if (entry.taskStatus === _COMPLETED && _appSettings.uploadCompletedTasks === false)
                continue;

            c = this.createCommand(entry);
            if (c === null) {
                console.log("[SyncRememberNotebookCmd.syncEntries] @#$ FUCK $#@  A COMMAND WAS NOT CREATED! entry.title: " + entry.title + ", entry.type: " + entry.type);
                continue;
            }

            commands.push(c);
        }

        if (this.taskListPageAction.length > 0) {
            commands.push(this.createTaskListPageCmd());
        }

        console.log("[SyncRememberNotebookCmd.syncEntries] commands.length: " + commands.length);
        // Execute all Commands!
        for (var j = 0, k = commands.length; j < k; ++j) {
            commands[j].execute();
            // 1 Uploader -> 1 NetworkAccessMngr
            // 1 Uploader -> many SyncRememberNotebookCmd
            // 1 SyncRememberNotebookCmd por Notebook
            // 1 Notebook pode ter varios Entries
            // 1 Entry -> 1 Page
            // 1 HTML Task -> 1 Page
            // 1 Notebook -> 1 Task List Page
            // 1 Plain Task -> Add to Task List Page

        }
    };

    SyncRememberNotebookCmd.prototype.createCommand = function _createCommand(entry) {
        console.log("[SyncRememberNotebookCmd.createCommand] entry.syncInfo: " + entry.syncInfo + ", nbName: " + this.nbName + ", entry.type: " + entry.type + ", entry.status: " + entry.status + ", entry.title: " + entry.title );

        if (entry.type === _PLAIN_ENTRY)
            return null;

        var c = null,
            _self = this,
            _entry = entry;

        function _failCb() {
            console.log("[SyncRememberNotebookCmd.createCommand._failCb] @#$ FUCK $#@  entry.id: " + _entry.id + ", oneNotePageId: " + _self.oneNotePageId + ", nbName: " + _self.nbName);
            _self.createEntryPageFailCb(_entry);
        }

        if ( entry.status == _OFFLINE_STATUS ) {

            entry.builtHtml = facade.htmlPageBuilder.buildPageHTML(entry);

            // If the Entry doesn't have Attachments, use this API, else use OneNoteUploader.cpp
            if (entry.attachments.length === 0) {
                c = new facade.controller.CreateAssetsCmd({
                    endpoint: "pages",
                    sectionId: _self.oneNoteSection.id,
                    body: entry.builtHtml,
                    successCb: function(newPage) {
                        _self.updateEntryPageCb(_POST_OPERATION, _entry, newPage);
                    },
                    failCb: _failCb
                });
            } else {
                c = {
                    execute: function _uploaderCommand() {
                        console.log("[SyncRememberNotebookCmd.createCommand._uploaderCommand] isUploaderBounded: " + _self.isUploaderBounded);
                        _self.bindUploader();
                        _uploader.upload(_entry, _self.oneNoteSection.id);
                    },
                    syncCmd: _self
                };
            }
        } else if (entry.status == _OUTDATED_STATUS || entry.status == _INVALID_STATUS) {
            // Must first retrieve Page Contents so we know what to PATCH
            console.log("[SyncRememberNotebookCmd.createCommand] Must PATCH/UPDATE. title: " + entry.title + ", entry.status: " + entry.status + ", oneNotePageId: " + entry.syncInfo.oneNotePageId);
            c = new facade.controller.RequestAssetsCmd({
                endpoint: "content",
                id: entry.syncInfo.oneNotePageId,
                successCb: function _pageContentsScb(data) {
                    console.log("[SyncRememberNotebookCmd.getPageContents._pageContentsScb] entry.id: " + _entry.id + ", oneNotePageId: " + _entry.syncInfo.oneNotePageId);
                    _entry.oneNotePageContents = data;
                    _self.overwriteEntryPage(_entry);
                },
                failCb: _failCb
            });
        }
        return c;
    };

    SyncRememberNotebookCmd.prototype.bindUploader = function _bindUploader() {
        console.log("[SyncRememberNotebookCmd.bindUploader] isUploaderBounded: " + this.isUploaderBounded);
        if (this.isUploaderBounded === false) {
            _uploader.finished.connect(this.boundedCb);
            this.isUploaderBounded = true;
        }
    };

    SyncRememberNotebookCmd.prototype.createTaskListPageCmd = function _createTaskListPageCmd() {
        console.log("[SyncRememberNotebookCmd.createTaskListPageCmd] taskListPageAction: " + this.taskListPageAction + ", plainTasksCount: " + this.plainTasksCount + ", nbName: " + this.nbName);

        if (this.taskListPageAction.length === 0)
            return null;

        var _self = this,
            c = null;

        // Build Task List Page HTML Code with all the plain Tasks
        this.taskListPage = facade.htmlPageBuilder.buildPageTaskList(this.plainTasks);

        function _failCb(arg) {
            console.log("[SyncRememberNotebookCmd.createTaskListPageCmd._failCb] @#$ FUCK $#@  arg: " + arg + ", oneNoteSection: ", _self.oneNoteSection);
            _self.createEntryPageFailCb({
                title: "TaskListPage",
                id: _TASKLIST_PAGE_ID
            });
        }

        if (this.taskListPageAction === "CREATE") {
            c = new facade.controller.CreateAssetsCmd({
                endpoint: "pages",
                sectionId: this.oneNoteSection.id,
                body: this.taskListPage,
                successCb: function(newPage) {
                    _self.updateTaskListPageCb(_POST_OPERATION, newPage);
                },
                failCb: _failCb
            });
        } else if (this.taskListPageAction === "PATCH") {
            c = new facade.controller.RequestAssetsCmd({
                endpoint: "content",
                id: this.taskListPageONID,
                successCb: function _pageContentsScb(data) {
                    console.log("[SyncRememberNotebookCmd.createTaskListPageCmd._pageContentsScb] taskListPageONID: " + this.taskListPageONID + ", nbName: " + this.nbName);
                    _self.overwriteTaskListPage(data);
                },
                failCb: _failCb
            });
        }

        return c;
    };

    SyncRememberNotebookCmd.prototype.getPageContents = function _getPageContents(entry) {
        console.log("[SyncRememberNotebookCmd.getPageContents] entry.id: " + entry.id + ", oneNotePageId: " + entry.syncInfo.oneNotePageId + ", nbName: " + this.nbName);
        var that = this,
            c = new facade.controller.RequestAssetsCmd({
                endpoint: "content",
                id: entry.syncInfo.oneNotePageId,
                successCb: function _pageContentsScb(data) {
                    console.log("[SyncRememberNotebookCmd.getPageContents._pageContentsScb] entry.id: " + entry.id + ", oneNotePageId: " + entry.syncInfo.oneNotePageId);
                    entry.oneNotePageContents = data;
                    that.overwriteEntryPage(entry);
                },
                failCb: function _pageContentsFcb() {
                    console.log("[SyncRememberNotebookCmd.getPageContents._pageContentsFcb] @#$ FUCK $#@ fetching Page contents... entry.id: " + entry.id + ", oneNotePageId: " + entry.syncInfo.oneNotePageId);
                    that.createEntryPageFailCb(entry);
                }
            });
        c.execute();
    };

    SyncRememberNotebookCmd.prototype.overwriteEntryPage = function _overwriteEntryPage(entry) {
        console.log("[SyncRememberNotebookCmd.overwriteEntryPage] entry.attachments.length: " + entry.attachments.length + ", nbName: " + this.nbName);

        // To PATCH a Page:
        // 1- Rebuild the whole Page HTML code of the Remember Entry
        // 2- Get OneNote's generatedId of our custom <div data-id="memoir-content">
        // 3- Replace the old memoir-content DIV with the new
        // 4- Use OneNoteUploader.cpp to send a PATCH request (since its not available from QML)
        var built = facade.htmlPageBuilder.buildPageHTML(entry);
        entry.generatedId = facade.htmlPageBuilder.findGeneratedId(entry.oneNotePageContents);
        entry.builtHtml = facade.htmlPageBuilder.getMemoirContents(built);

        this.bindUploader();

        _uploader.patchPage(entry, entry.syncInfo.oneNotePageId);
    };

    SyncRememberNotebookCmd.prototype.overwriteTaskListPage = function _overwriteTaskListPage(oneNotePageContents) {
        console.log("[SyncRememberNotebookCmd.overwriteTaskListPage] taskListPageONID: " + this.taskListPageONID + ", nbName: " + this.nbName);


        var built = facade.htmlPageBuilder.buildPageTaskList(this.plainTasks),
            entry = {
                id:  this.taskListPageUID ,
                title: _taskLabels.listTitle,
                generatedId: facade.htmlPageBuilder.findGeneratedId(oneNotePageContents), // ALWAYS Rewrite the whole page. Never append!
                builtHtml: built
            };

        this.bindUploader();
        _uploader.patchPage(entry, this.taskListPageONID);
    };

    SyncRememberNotebookCmd.prototype.onUploaderFinished = function _onUploaderFinished(response) {
        console.log("[SyncRememberNotebookCmd.onUploaderFinished] response.hasError: " + response.hasError + ", entry.id: " + response.entry.id + ", response.operation: " + response.operation + ", nbName: " + this.nbName);

        var isMine = false,
            responseEntryId = response.entry.id;

        if (responseEntryId ===  this.taskListPageUID ) {
            console.log("[SyncRememberNotebookCmd.onUploaderFinished] Found my Task List Page! id: " + responseEntryId);
            isMine = true;
        } else {
            for (var i = 0, l = this.rememberNotebook.entries.length; i < l; ++i) {
                if (this.rememberNotebook.entries[i].id == responseEntryId) {
                    isMine = true;
                    break;
                }
            }
        }

        if (!isMine) {
            console.log("[SyncRememberNotebookCmd.onUploaderFinished] Skipping. Not my Entry..., nbName: " + this.nbName);
            return;
        }

        if (response.hasError) {
            this.createEntryPageFailCb(response.entry);
        }
        else if (responseEntryId ===  this.taskListPageUID ) {
            // Operation::PostOperation == 4, Operation::CustomOperation == 6
            this.updateTaskListPageCb(response.operation, response.result);
        }
        else {
          // Operation::PostOperation == 4, Operation::CustomOperation == 6
          this.updateEntryPageCb(response.operation, response.entry, response.result);
        }
    };

    SyncRememberNotebookCmd.prototype.updateEntryPageCb = function _updateEntryPageCb(operation, entry, newPage) {
        console.log("[SyncRememberNotebookCmd.updateEntryPageCb] PAGE UPDATED! operation: " + operation + ", entry.title: " + entry.title + ", entry.id: " + entry.id + ", nbName: " + this.nbName + ", newPage: " + newPage);
        // Operation::PostOperation == 4, Operation::CustomOperation == 6
        if (operation === _POST_OPERATION) {
            this.pagesCreatedCount++;
            facade.model.pages.push(newPage);
            // Put the new OneNote Page on MAP. The key is its ID and the value is the associate Remember Entry!
            this.pagesUpdated[newPage.id] = entry;
        } else if (operation === _CUSTOM_OPERATION && entry.syncInfo) {
            this.pagesPatchedCount++;
            // PATCH Page doesn't return a newPage but the Entry has syncInfo with its OneNote Page Id! This info is used on fetchUpdatedPagesCb()
            this.pagesUpdated[entry.syncInfo.oneNotePageId] = entry;
        }
        else {
          console.log("[SyncRememberNotebookCmd.updateEntryPageCb] @#$ FUCK $#@ No syncInfo!? entry.title: " + entry.title + ", entry.syncInfo: " + entry.syncInfo + ", nbName: " + this.nbName);
        }

        if (this.isDoneCreatingPages()) {
            this.waitToFetchPages();
        }
    };

    SyncRememberNotebookCmd.prototype.updateTaskListPageCb = function _updateTaskListPageCb(operation, newPage) {
        console.log("[SyncRememberNotebookCmd.updateTaskListPageCb] TaskListPage updated! operation: " + operation + ", newPage: " + newPage + ", nbName: " + this.nbName);
        // Operation::PostOperation == 4, Operation::CustomOperation == 6
        if (operation === _POST_OPERATION) {
            this.pagesCreatedCount++;
            facade.model.pages.push(newPage);
            this.pagesUpdated[newPage.id] =  this.taskListPageUID;
        } else {
            this.pagesPatchedCount++;
            // PATCH Page doesn't return a newPage but this Command has the Task List Page ID to be updated! This info is used on fetchUpdatedPagesCb()
            this.pagesUpdated[this.taskListPageONID] =  this.taskListPageUID;
        }

        // -1 Indicates that Task List Page has been created. Checked on isDoneCreatingPages()!
        this.plainTasksCount = -1;

        if (this.isDoneCreatingPages()) {
            this.waitToFetchPages();
        }
    };

    /*
     * The server replies immediatly after a Page is created successfully. lastModifiedTime == createdTime
     * But writting to disk is an ASYNC process and it might take a few seconds for it to be fully written. lastModifiedTime == Time.now() > createdTime
     * Also, OneNote has an ASYNC process that parses the contents of the Page to augment it (for searching, image processing and etc.) which also changes lastModifiedTime.
     * Therefore, due to all the ASYNC OneNote processes, this APP must wait a few seconds until all server tasks are done so that the final lastModifiedTime is used
     * for keeping the versions in sync.
     * The waiting time is defined on main.qml/Qtimer[waitTimer]
     */
    SyncRememberNotebookCmd.prototype.waitToFetchPages = function _waitToFetchPages() {
        console.log("[SyncRememberNotebookCmd.waitToFetchPages] nbName: " + this.nbName);
        var that = this;

        function _onTimeout() {
            console.log("[SyncRememberNotebookCmd.waitToFetchPages.onTimeout] oneNoteSection.id: " + that.oneNoteSection.id + ", nbName: " + that.nbName);
            _timer.timeout.disconnect(_onTimeout);
            that.fetchUpdatedPages();
        };

        _timer.timeout.connect(_onTimeout);
        _timer.start();
    };

    SyncRememberNotebookCmd.prototype.fetchUpdatedPages = function _fetchUpdatedPages() {
        console.log("[SyncRememberNotebookCmd.fetchUpdatedPages] nbName: " + this.nbName);
        var that = this;

        var c = new facade.controller.RequestAssetsCmd({
            endpoint: "pages",
            sectionId: this.oneNoteSection.id,
            args: "?orderby=lastModifiedTime&select=id,title,lastModifiedTime&count=true",
            successCb: this.fetchUpdatedPagesCb.bind(this),
            failCb: function() {
                console.log("[SyncRememberNotebookCmd.fetchUpdatedPages.failCb] couldn't retrieve Pages of Section " + that.oneNoteSection.id);
                // try again...
                that.fetchUpdatedPages();
            }
        });
        c.execute();
    };

    SyncRememberNotebookCmd.prototype.fetchUpdatedPagesCb = function _fetchUpdatedPagesCb(pages) {
        var l = pages.length;

        console.log("[SyncRememberNotebookCmd.fetchUpdatedPagesCb] pages.length: " + l + ", totalActions: " + this.totalActions + ", nbName: " + this.nbName);

        // Some times not all Pages of a Section is fetched. Other times, there could be more Pages retrieved.
        // Must keep trying to retrieve all created/updated Pages of this Command.
        if( l < this.totalActions ){
            console.log("[SyncRememberNotebookCmd.fetchUpdatedPagesCb] @#$ FUCK $#@ Didn't find all Pages created/updated yet !? Retrying... totalActions: " + this.totalActions + ", nbName: " + this.nbName);
            // Never stop trying to fetch those Pages!!!
            return this.waitToFetchPages();
        }

        for (var i = 0; i < l; ++i) {
            var p = pages[i];
            var entry = this.findEntryByOneNotePageId(p);

            console.log("[SyncRememberNotebookCmd.fetchUpdatedPagesCb] entry: " + entry + ", nbName: " + this.nbName);

            if (!entry) {
                console.log("[SyncRememberNotebookCmd.fetchUpdatedPagesCb] @#$ FUCK $#@ Page ignored... p.id: " + p.id + ", p.title: " + p.title);
                continue;
            } else if (entry ===  this.taskListPageUID ) {
                // All Plain Tasks are stored on the same Page: the Task List Page!
                for (var k = 0, t = this.plainTasks.length; k < t; ++k) {
                    this.updateEntrySyncInfo(this.plainTasks[k], p, true);
                }
            } else {
                this.updateEntrySyncInfo(entry, p, true);
            }
        }
        this.finish();
    };

    SyncRememberNotebookCmd.prototype.updateEntrySyncInfo = function _updateEntrySyncInfo(entry, newPage, saveSyncInfo) {
        console.log("[SyncRememberNotebookCmd.updateEntrySyncInfo] title: " + entry.title + ", newPage.lastModifiedTime: " + newPage.lastModifiedTime + ", saveSyncInfo: " + saveSyncInfo + ", nbName: " + this.nbName);

        var newSyncInfo = {
           oneNoteSectionId: this.oneNoteSection.id,
           oneNotePageId: newPage.id,
           lastModifiedTime: newPage.lastModifiedTime,
           createdTime: newPage.createdTime,
           rememberLastModified: entry.lastModified // the version (time) of Remember's Entry that was uploaded to OneNote
       };

        entry.syncInfo = newSyncInfo;

        if( saveSyncInfo === true ){
            //Use the Entry's ID as key for the syncInfo object. It's retrieved on RememberModel.refresh()
            _appSettings.saveSyncInfo(entry.id, newSyncInfo);
        }
    };

    SyncRememberNotebookCmd.prototype.findEntryByOneNotePageId = function _findEntryByOneNotePageId(page) {
        var pageId = page.id,
            entry = this.pagesUpdated[pageId];

        console.log("[SyncRememberNotebookCmd.findEntryByOneNotePageId] pageId: " + pageId + ", nbName: " + this.nbName);

        if (!entry) {
            // For some fucking reason the the Page could not be found on the MAP with its id, try to look on Model.Pages
            for( var i = 0, l = facade.model.pages.length; i < l; ++i ){
                if(facade.model.pages[i].id === pageId) {
                    entry = facade.model.pages[i];
                    break;
                }
            }

            if (!entry)
              console.log("[SyncRememberNotebookCmd.findEntryByOneNotePageId] @#$ FUCK $#@ No Entry found! nbName: " + this.nbName);
        }
        return entry;
    };

    SyncRememberNotebookCmd.prototype.createEntryPageFailCb = function _createEntryPageFailCb(entry) {
        console.log("[SyncRememberNotebookCmd.createEntryPageFailCb] @#$ FUCK $#@ creating Page for entry: " + entry.title + ", nbName: " + this.nbName);
        this.pagesFailCount++;
        if (this.isDoneCreatingPages()) {
            this.finish();
        }
    };

    SyncRememberNotebookCmd.prototype.finish = function _finish() {
        console.log("[OneNoteAPI.SyncRememberNotebookCmd.finish] pagesFailCount: " + this.pagesFailCount + ", nbName: " + this.nbName + ", isUploaderBounded: " + this.isUploaderBounded);
        if (this.isUploaderBounded === true) {
            _uploader.finished.disconnect(this.boundedCb);
            this.isUploaderBounded = false;
        }
        this.done({
            result: (this.pagesFailCount > 0 ? "fail" : "success"),
            data: this.rememberNotebook
        });
    };

    SyncRememberNotebookCmd.prototype.isDoneCreatingPages = function _isDoneCreatingPages() {
        console.log("[OneNoteAPI.SyncRememberNotebookCmd.isDoneCreatingPages] pagesToCreateCount: " + this.pagesToCreateCount + ", pagesCreatedCount: " + this.pagesCreatedCount + ", pagesToPatchCount: " + this.pagesToPatchCount + ", pagesPatchedCount: " + this.pagesPatchedCount + ", pagesFailCount: " + this.pagesFailCount + ", plainTasks: " + this.plainTasks + ", nbName: " + this.nbName);
        if (this.pagesFailCount === 0)
            return this.pagesToCreateCount === this.pagesCreatedCount && this.pagesToPatchCount === this.pagesPatchedCount && (this.taskListPageAction === "CREATE" ? this.plainTasksCount === -1 : true);
        else
            return (this.pagesToCreateCount + this.pagesToPatchCount === this.pagesCreatedCount + this.pagesPatchedCount + this.pagesFailCount);
    };

    facade.controller.SyncRememberNotebookCmd = SyncRememberNotebookCmd;

    return facade;
}();
