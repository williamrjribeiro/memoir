import bb.cascades 1.4

Sheet {
    id: helpSheet
    property real paddings: ui.sdu(2.0)
    onCreationCompleted: helpSheet.open()
    // Destroy onClosed because WebView uses too much memory
    onClosed: helpSheet.destroy()
    Page {
        titleBar: TitleBar {
            title: qsTr("Help")
            dismissAction: ActionItem {
                title: qsTr("Close")
                ActionBar.placement: ActionBarPlacement.OnBar
                onTriggered: helpSheet.close()
            }
        }
        ScrollView {
            scrollRole: ScrollRole.Main
            scrollViewProperties.scrollMode: ScrollMode.Vertical
            scrollViewProperties.pinchToZoomEnabled: false
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill
            implicitLayoutAnimationsEnabled: false
            WebView {
                url: "http://williamrjribeiro.com/memoir/help.html"
                settings.javaScriptEnabled: true
                settings.minimumFontSize: 5
                settings.cookiesEnabled: false
                settings.binaryFontDownloadingEnabled: false
                settings.defaultTextCodecName: "utf-8"
            }
        }
    }
}