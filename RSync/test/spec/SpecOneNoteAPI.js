describe('the global object ONC (OneNoteCommandAPI)', function() {

    describe('when loaded and initialized', function() {

        it('should be an Object', function() {
            expect(typeof window.facade).toBe("object");
        });

        it('should add inheritsFrom to Function', function() {
            expect(typeof Function.inheritsFrom).toBe("function");
        });

        it('should have a valid value for serviceClientId', function() {
            expect(window.facade.getServiceClientId()).toEqual('0000000040156DA8');
        });

        it('should have a valid value for serviceAPIUrl', function() {
            expect(window.facade.getServiceAPIUrl()).toEqual('https://www.onenote.com/api/v1.0/me/notes');
        });

        it('should have a valid value for serviceTokenRequestUrl', function() {
            expect(window.facade.getServiceTokenRequestUrl()).toEqual("https://login.live.com/oauth20_authorize.srf?client_id=0000000040156DA8&display=touch&scope=office.onenote_update&response_type=token&redirect_uri=https://login.live.com/oauth20_desktop.srf");
        });

        it('should have a valid value for accessToken', function() {
            expect(window.facade.getAccessToken()).toEqual("");
        });

        it('should have a valid value for accessTokenDateTime', function() {
            expect(facade.init({
                accessTokenDateTime: "fakeDateTime"
            }, {}, {}, {}, {})).toEqual(true);
            expect(facade.getAccessTokenDateTime()).toEqual("fakeDateTime");
        });

        describe('facade.init()', function() {
            it('should accept 3 Objects as arguments and return TRUE if all are defined', function() {
                expect(facade.init({}, {}, {}, {}, {})).toEqual(true);
            });

            it('should return FALSE if any argument is invalid', function() {
                expect(facade.init({})).toEqual(false);
                expect(facade.init({}, {})).toEqual(false);
                expect(facade.init(false, {}, {})).toEqual(false);
                expect(facade.init(false, 1, {})).toEqual(false);
                expect(facade.init(false, 1, {}, false)).toEqual(false);
                expect(facade.init(false, 1, {}, false, 3)).toEqual(false);
            });
        });

        describe('facade.model', function() {

            it('should have a valid model Object', function() {
                expect(typeof window.facade.model).toBe('object');
            });

            it('should have an empty array for model.notebooks', function() {
                expect(facade.model.notebooks).toEqual([]);
            });

            it('should have an empty array for model.sections', function() {
                expect(facade.model.sections).toEqual([]);
            });

            it('should have an empty array for model.pages', function() {
                expect(facade.model.pages).toEqual([]);
            });
        });

        describe('facade.getParamObjectFromURL()', function() {
            it('should create an Object with property and values with the URL GET param variables and decoded values', function() {
                var url = "http://www.williamrjribeiro.com/index.html#a=123&b=false&c=will&name=st%C3%A5le",
                    expected = {
                        a: '123',
                        b: 'false',
                        c: 'will',
                        name: 'ståle'
                    },
                    result;

                result = facade.getParamObjectFromURL(url);
                expect(result).toEqual(expected);
            });
        });

        describe('facade.setAccessTokenByURL()', function() {
            it('should NOT change accessToken property if URL is INvalid', function() {
                facade.setAccessTokenByURL("");
                expect(facade.getAccessToken()).toEqual(undefined);
            });

            it('should set accessToken property if URL is valid', function() {
                var spy = jasmine.createSpyObj('uploader', ['setAccessToken']);
                facade.init({
                    accessTokenDateTime: false
                }, {}, spy);

                var url = "#access_token=1234567890&token_type=";
                facade.setAccessTokenByURL(url);
                expect(facade.getAccessToken()).toEqual("1234567890");
                expect(spy.setAccessToken).toHaveBeenCalledWith("1234567890");
            });

            it('should set accessToken property if URL is valid AND if isNew is TRUE, a the current time should be stored on _appSettings.accessTokenDateTime', function() {
                var spy = jasmine.createSpyObj('uploader', ['setAccessToken']);
                facade.init({
                    accessTokenDateTime: false
                }, {}, spy);
                expect(facade.getAccessTokenDateTime()).toBe(false);

                var url = "#access_token=1234567890&token_type=";
                facade.setAccessTokenByURL(url, true);

                expect(facade.getAccessToken()).toEqual("1234567890");
                expect(facade.getAccessTokenDateTime()).toBeTruthy();
                expect(spy.setAccessToken).toHaveBeenCalledWith("1234567890");
            });
        });

        describe('facade.isAccessTokenValid()', function() {
            it('should return FALSE if _accessToken is NOT defined', function() {
                facade.resetTokens();
                expect(facade.isAccessTokenValid()).toBe(false);
            });

            it('should return FALSE if _accessToken IS defined BUT is older than 1 hour (3600000ms)', function() {
                var spy = jasmine.createSpyObj('uploader', ['setAccessToken']);
                facade.init({
                    accessTokenDateTime: new Date("1985-03-06T15:45:00")
                }, {}, spy);

                var url = "#access_token=1234567890&token_type=";
                facade.setAccessTokenByURL(url);

                expect(facade.isAccessTokenValid()).toBe(false);
                expect(spy.setAccessToken).toHaveBeenCalledWith("1234567890");
            });

            it('should return TRUE if _accessToken IF defined AND is newer than 1 hour (3600000ms)', function() {
                var spy = jasmine.createSpyObj('uploader', ['setAccessToken']);
                facade.init({
                    accessTokenDateTime: new Date()
                }, {}, spy);

                var url = "#access_token=1234567890&token_type=";
                facade.setAccessTokenByURL(url);

                expect(facade.isAccessTokenValid()).toBe(true);
                expect(spy.setAccessToken).toHaveBeenCalledWith("1234567890");
            });

            it('should call facade.resetTokens() if accessToken is invalid', function() {
                spyOn(facade, "resetTokens");

                var spy = jasmine.createSpyObj('uploader', ['setAccessToken']);
                facade.init({
                    accessTokenDateTime: new Date("1985-03-06T15:45:00")
                }, {}, spy);

                var url = "#access_token=1234567890&token_type=";
                facade.setAccessTokenByURL(url);

                expect(facade.isAccessTokenValid()).toBe(false);
                expect(facade.resetTokens).toHaveBeenCalled();
            });
        });

        describe('facade.getFromModelBy()', function() {
            it('should return NULL if facade.model[name] is INvalid', function() {
                expect(facade.getFromModelBy('missing')).toEqual(null);
            });

            it('should return an empty array if no values match', function() {
                expect(facade.getFromModelBy('notebooks', 'id', 'invalid')).toEqual([]);
            });

            it('should return NULL if no values match and uniqueValue is true', function() {
                expect(facade.getFromModelBy('notebooks', 'id', 'invalid', true)).toEqual(null);
            });

            it('should return an array with all elements matching the value', function() {
                facade.model.notebooks = [{
                    parentId: 1,
                    id: 1
                }, {
                    parentId: 1,
                    id: 2
                }, {
                    parentId: 2,
                    id: 3
                }];

                expect(facade.getFromModelBy('notebooks', 'parentId', 1)).toEqual([{
                    parentId: 1,
                    id: 1
                }, {
                    parentId: 1,
                    id: 2
                }]);
            });
        });

        describe('facade.resetTokens()', function() {
            it('should clear the values of _accessToken and the references from _appSettings and _uploader', function() {
                var spy = jasmine.createSpyObj('uploader', ['setAccessToken']),
                    settings = {
                        accessToken: "123",
                        accessTokenDateTime: new Date()
                    };
                facade.init(settings, {}, spy);

                var url = "#access_token=1234567890&token_type=test";
                facade.setAccessTokenByURL(url);
                expect(facade.getAccessToken()).toEqual("1234567890");

                facade.resetTokens();

                expect(facade.getAccessToken()).toEqual("");
                expect(settings.accessToken).toEqual("");
                expect(spy.setAccessToken).toHaveBeenCalledWith("");
            });
        });

        describe('facade.isValidName()', function() {
            it('should return FALSE if name is empty', function() {
                var s = "";
                expect(s.length == 0).toEqual(true);
                expect(facade.isValidName(s)).toEqual(false);
            });

            it('should return FALSE if name has more than 50 characters', function() {
                var s = "Lorem ipsum dolor sit amet, id est graeci utroque omittam, tempor audiam vel ea. Legere noster sea ex, eum no.";
                expect(s.length > 50).toEqual(true);
                expect(facade.isValidName(s)).toEqual(false);
            });

            it('should return FALSE if name includes any of the characters ? * \ / : < > | & # " % ~', function() {
                var s = 'A?b*c\d/e:f<g>h|i&j#k"l%m~n';
                expect(facade.isValidName(s)).toEqual(false);
                expect(facade.isValidName('A?')).toEqual(false);
                expect(facade.isValidName('A*')).toEqual(false);
                expect(facade.isValidName('A\\B')).toEqual(false);
                expect(facade.isValidName('A/')).toEqual(false);
                expect(facade.isValidName('A:')).toEqual(false);
                expect(facade.isValidName('A<')).toEqual(false);
                expect(facade.isValidName('A>')).toEqual(false);
                expect(facade.isValidName('A|')).toEqual(false);
                expect(facade.isValidName('A&')).toEqual(false);
                expect(facade.isValidName('A#')).toEqual(false);
                expect(facade.isValidName('A"')).toEqual(false);
                expect(facade.isValidName('A%')).toEqual(false);
                expect(facade.isValidName('A~')).toEqual(false);
            });

            it('should return TRUE if name is less than 50 chars long and has no illegal chars', function() {
                var s = 'William R. J. Ribeiro';
                expect(facade.isValidName(s)).toEqual(true);
            });
        });

        describe('facade.fixName()', function() {
            it('should trim the name to 50 characters', function() {
                var s = "Lorem ipsum dolor sit amet, id est graeci utroque omittam, tempor audiam vel ea. Legere noster sea ex, eum no.";
                expect(s.length > 50).toEqual(true);
                s = facade.fixName(s);
                expect(s.length).toEqual(50);
            });

            it('should remove all illegal characters', function() {
                var s = 'A?b*c\\d/e:f<g>h|i&j#k"l%m~n';
                s = facade.fixName(s);
                expect(s).toEqual('Abcdefghijklmn');
            });
        });

        describe('facade.htmlPageBuilder', function() {
            describe('buildPageHTMLAddXMLInfo()', function() {
                it("should return a copy of the given string WITH the whole <xml> tag", function() {
                    expect(facade.htmlPageBuilder.buildPageHTMLAddXMLInfo("<html></html>")).toEqual('<?xml version="1.0" encoding="utf-8" ?><html></html>');
                });
            });

            describe('buildPageHTMLRemoveStyle()', function() {
                it("should return a copy of the given string WITHOUT the whole <style> tag", function() {
                    expect(facade.htmlPageBuilder.buildPageHTMLRemoveStyle('<html><style type="text/css">body {word-wrap: break-word;}</style></html>')).toEqual("<html></html>");
                });
            });

            describe('buildPageHTMLEnhanceTag()', function() {
                it("should return a copy of the given string WITH enhanced HTML tag", function() {
                    expect(facade.htmlPageBuilder.buildPageHTMLEnhanceTag("<html></html>")).toEqual('<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" lang="en-us"></html>');
                });
            });

            describe('buildPageHTMLAddTitle()', function() {
                it("should add the <title> to the <head> with given Text", function() {
                    expect(facade.htmlPageBuilder.buildPageHTMLAddTitle("<head></head>", "The Title")).toEqual("<head><title>The Title<\/title></head>");
                });

                it("should add the <title> to the <head> with given Text when taskStatus is NOT Completed", function() {
                    expect(facade.htmlPageBuilder.buildPageHTMLAddTitle("<head></head>", "The Title", 2)).toEqual("<head><title>The Title<\/title></head>");
                    expect(facade.htmlPageBuilder.buildPageHTMLAddTitle("<head></head>", "The Title", 10)).toEqual("<head><title>The Title<\/title></head>");
                    expect(facade.htmlPageBuilder.buildPageHTMLAddTitle("<head></head>", "The Title", true)).toEqual("<head><title>The Title<\/title></head>");
                });

                it("should add the <title> to the <head> with given Text AND donePrefix when taskStatus is Completed", function() {
                    expect(facade.htmlPageBuilder.buildPageHTMLAddTitle("<head></head>", "The Title", 4, "DONE - ")).toEqual("<head><title>DONE - The Title<\/title></head>");
                });
            });

            describe('buildPageHTMLAddMetaCreated()', function() {
                it("should return a copy of the given string WITH the <meta> tag and the given value", function() {
                    expect(facade.htmlPageBuilder.buildPageHTMLAddMetaCreated("<head></head>", "2015-12-29T14:30:25")).toEqual('<head><meta name="created" content="2015-12-29T14:30:25" \/><\/head>');
                });
            });

            describe('buildPageHTMLConvertFontTags()', function() {
                it("should return a copy of the given string WITH the <font> tag swapped and converted to <p>", function() {
                    var str = '<font size="1" color="#ff0000">Size 1</font>';
                    var expected = '<span style="font-size:7.5px;color:#ff0000;">Size 1</span>';
                    expect(facade.htmlPageBuilder.buildPageHTMLConvertFontTags(str)).toEqual(expected);
                });
            });

            describe('buildPageHTMLAddMemoirId()', function() {
                it('should add <div id="memoir-content"> right after <body> and close both tags', function() {
                    expect(facade.htmlPageBuilder.buildPageHTMLAddMemoirId("<body>Memoir</body>")).toEqual('<body><div id="memoir-content">Memoir</div></body>');
                });
            });

            describe('buildLabel()', function() {
                it('should add Paragraph with given dataTag, a Bold Label and the Date value', function() {
                    expect(facade.htmlPageBuilder.buildLabel("<body></body>", "remember-for-later", "Reminder:", "18/12/2016")).toEqual('<body><br /> <p data-tag="remember-for-later"><span style="font-size:12pt; font-weight:bold">Reminder:</span><span>18/12/2016</span></p></body>');
                });
            });

            describe('buildCheckbox()', function() {
                it('should add Paragraph with To-do data for NOT DONE Tasks, a checkbox and a bigger text', function() {
                    expect(facade.htmlPageBuilder.buildCheckbox("Buy milk", 2)).toEqual('<p data-tag="to-do"><span style="font-size:12pt">Buy milk</span></p>');
                });

                it('should add Paragraph with To-do:completed data for DONE Tasks, a checkbox and a bigger text', function() {
                    expect(facade.htmlPageBuilder.buildCheckbox("Buy eggs", 1)).toEqual('<p data-tag="to-do:completed"><span style="font-size:12pt">Buy eggs</span></p>');
                });
            });

            describe('findGeneratedId', function() {
                it('should return the id String from <div data-id="memoir-content">', function() {
                    var desc = '<div id="div:{4329b694-7645-47b0-a6ad-1989d66e44db}{43}" data-id="_default" style="position:absolute;left:48px;top:120px;width:624px">';
                    desc += '<div data-id="memoir-content" id="div:{4329b694-7645-47b0-a6ad-1989d66e44db}{49}:{4329b694-7645-47b0-a6ad-1989d66e44db}{49}">';
                    desc += '<p id="p:{4329b694-7645-47b0-a6ad-1989d66e44db}{49}">Has some text</p></div></div>';
                    var expected = 'div:{4329b694-7645-47b0-a6ad-1989d66e44db}{49}:{4329b694-7645-47b0-a6ad-1989d66e44db}{49}';
                    expect(facade.htmlPageBuilder.findGeneratedId(desc)).toEqual(expected);
                });

                it('should return "body" if tag <div data-id="memoir"> is NOT found', function() {
                    var desc = '<div id="div:{4329b694-7645-47b0-a6ad-1989d66e44db}{43}" data-id="_default" style="position:absolute;left:48px;top:120px;width:624px"></div>';
                    var expected = 'body';
                    expect(facade.htmlPageBuilder.findGeneratedId(desc)).toEqual(expected);
                });
            });

            describe('getMemoirContents', function() {
                it('should return the <div data-id="memoir-content"></div></body>', function() {
                    var desc = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><!DOCTYPE html><html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en-us\"><head><title>Only note</title><meta name=\"created\" content=\"2016-02-22T17:32:26\" /></head><body><div id=\"memoir-content\">Now it's different </div></body></html>";
                    var expected = "<div id=\"memoir-content\">Now it's different </div>";
                    expect(facade.htmlPageBuilder.getMemoirContents(desc)).toEqual(expected);
                });
            });

            describe('encodeHtmlEntities', function() {
                it('should escape all strange HTML characters', function() {
                    var desc = "<body><div id=\"memoir-content\">Caçhão é ô ñãõ € &</div></body>";
                    var expected = "<body><div id=\"memoir-content\">Ca&ccedil;h&atilde;o &eacute; &ocirc; &ntilde;&atilde;&otilde; &euro; &amp;</div></body>";
                    expect(facade.htmlPageBuilder.encodeHtmlEntities(desc)).toEqual(expected);
                });
            });

            describe('decodeHtmlEntities', function() {
                it('should unescape all strange HTML characters', function() {
                    var desc = "<body><div id=\"memoir-content\">Ca&ccedil;h&atilde;o &eacute; &ocirc; &ntilde;&atilde;&otilde; &euro; &amp;</div></body>";
                    var expected = "<body><div id=\"memoir-content\">Caçhão é ô ñãõ € &</div></body>";
                    expect(facade.htmlPageBuilder.decodeHtmlEntities(desc)).toEqual(expected);
                });
            });

            describe('buildPageAddTaskToList()', function() {
                it('should generate HTML code compatible with OneNote when type is PLAIN', function() {
                    var expected = '<?xml version="1.0" encoding="utf-8" ?>';
                    expected += '<!DOCTYPE html> <html xmlns="http://www.w3.org/1999/xhtml" lang="en-us">';
                    expected += '    <head>';
                    expected += '        <title>Task List</title>';
                    expected += '        <meta name="created" content="2015-12-29T14:30:25" />';
                    expected += '    </head>';
                    expected += '    <body>';
                    expected += '        <div id="memoir-content">';
                    expected += '<p data-tag="to-do"><span style="font-size:12pt">Buy vegetables</span></p>'
                    expected += '        </div>';
                    expected += '    </body>';
                    expected += '</html>';

                    var entry = {
                        type: "plain",
                        title: "Buy vegetables",
                        lastModified: "2015-12-29T14:30:25",
                        taskStatus: 2,
                        description: null
                    };

                    facade.init({},{},{},{},{listTitle: "Task List"});
                    expect(facade.htmlPageBuilder.buildPageAddTaskToList(entry)).toEqual(expected);
                });

                it('should add more Tasks to the Tasklist Page', function() {
                    var strVar = '<?xml version="1.0" encoding="utf-8" ?>';
                    strVar += '<!DOCTYPE html> <html xmlns="http://www.w3.org/1999/xhtml" lang="en-us">';
                    strVar += '    <head>';
                    strVar += '        <title>Task List</title>';
                    strVar += '        <meta name="created" content="2016-12-25T09:47:15" />';
                    strVar += '    </head>';
                    strVar += '    <body>';
                    strVar += '        <div id="memoir-content">';
                    strVar += '<p data-tag="to-do"><span style="font-size:12pt">Buy vegetables</span></p>'
                    strVar += '        </div>';
                    strVar += '    </body>';
                    strVar += '</html>';

                    var expected = '<?xml version="1.0" encoding="utf-8" ?>';
                    expected += '<!DOCTYPE html> <html xmlns="http://www.w3.org/1999/xhtml" lang="en-us">';
                    expected += '    <head>';
                    expected += '        <title>Task List</title>';
                    expected += '        <meta name="created" content="2016-12-25T09:47:15" />';
                    expected += '    </head>';
                    expected += '    <body>';
                    expected += '        <div id="memoir-content">';
                    expected += '<p data-tag="to-do"><span style="font-size:12pt">Buy vegetables</span></p>'
                    expected += '<p data-tag="to-do:completed"><span style="font-size:12pt">Wash car</span></p>'
                    expected += '        </div>';
                    expected += '    </body>';
                    expected += '</html>';

                    var entry = {
                        type: "plain",
                        title: "Wash car",
                        lastModified: "2016-12-25T09:47:15",
                        taskStatus: 4,
                        description: null
                    };
                    expect(facade.htmlPageBuilder.buildPageAddTaskToList(entry, strVar)).toEqual(expected);
                });
            });

            describe('buildPageHTML()', function() {

                it("should trhow a TypeError if rememberEntry.type is invalid", function() {
                    var expected = "INVALID rememberEntry.type!";
                    expect( function() { facade.htmlPageBuilder.buildPageHTML({}) }).toThrowError( TypeError, expected);
                    expect(  function() { facade.htmlPageBuilder.buildPageHTML({
                        type: "invalid"
                    }) }).toThrowError(TypeError, expected);
                });

                it('should generate HTML code compatible with OneNote when type is HTML', function() {
                    var entry = {
                        type: "html",
                        title: "The Title",
                        lastModified: "2015-12-29T14:30:25",
                        dueDate: false,
                        reminder: false,
                        description: '<html><head><style type="text/css">body {word-wrap: break-word;}</style></head><body><div>Descrição&nbsp;</div><div><font size="1" color="#ff0000">Size 1</font></div><div><font size="2" color="#00ff00">Size 2</font></div><div><font size="3" color="#0000ff">Size 3</font></div><div><font size="4">Size 4</font></div><div><font size="5">Size 5</font></div><div><font size="6">Size 6</font></div><div><font size="7">Size 7</font></div></body></html>'
                    };
                    var expected = '<?xml version="1.0" encoding="utf-8" ?><!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" lang="en-us"><head><title>The Title</title><meta name="created" content="2015-12-29T14:30:25" /></head><body><div id="memoir-content"><div>Descri&ccedil;&atilde;o&nbsp;</div><div><span style="font-size:7.5px;color:#ff0000;">Size 1</span></div><div><span style="font-size:10px;color:#00ff00;">Size 2</span></div><div><span style="font-size:12px;color:#0000ff;">Size 3</span></div><div><span style="font-size:13.5px;">Size 4</span></div><div><span style="font-size:18px;">Size 5</span></div><div><span style="font-size:24px;">Size 6</span></div><div><span style="font-size:36px;">Size 7</span></div></div></body></html>';

                    expect(facade.htmlPageBuilder.buildPageHTML(entry)).toEqual(expected);

                    entry.dueDate = "25/12/2016";
                    entry.reminder = "22/12/2016";

                    expected = '<?xml version="1.0" encoding="utf-8" ?><!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" lang="en-us"><head><title>The Title</title><meta name="created" content="2015-12-29T14:30:25" /></head><body><div id="memoir-content"><div>Descri&ccedil;&atilde;o&nbsp;</div><div><p style="font-size:7.5px;color:#ff0000;">Size 1</p></div><div><p style="font-size:10px;color:#00ff00;">Size 2</p></div><div><p style="font-size:12px;color:#0000ff;">Size 3</p></div><div><p style="font-size:13.5px;">Size 4</p></div><div><p style="font-size:18px;">Size 5</p></div><div><p style="font-size:24px;">Size 6</p></div><div><p style="font-size:36px;">Size 7</p></div><br /> <p data-tag="critical"><span style="font-weight:bold">Due Date:</span><span>25/12/2016</span></p><br /> <p data-tag="remember-for-later"><span style="font-weight:bold">Reminder:</span><span>22/12/2016</span></p></div></body></html>'
                });
            });
        });

        describe('facade.controller', function() {

            it('should have a valid controller Object', function() {
                expect(typeof window.facade.controller).toBe('object');
            });

            describe("Command Class", function() {
                it('should have facade.controller.Command constructor', function() {
                    expect(typeof window.facade.controller.Command).toBe('function');
                });

                it('should create a default Object with valid values', function() {
                    var c = new facade.controller.Command();
                    expect(c.params.assetName).toEqual("");
                    expect(c.params.args).toEqual("");
                    expect(c.params.endpoint).toEqual("/");
                });

                it('should have a null value for data', function() {
                    var c = new facade.controller.Command();
                    expect(c.data).toEqual(null);
                });

                it('should have a null value for property result', function() {
                    var c = new facade.controller.Command();
                    expect(c.getResult()).toEqual(null);
                });

                describe('Command.execute()', function() {
                    it('should call sendReq()', function() {
                        var c = new facade.controller.Command();
                        spyOn(c, "sendReq");
                        c.execute();
                        expect(c.sendReq).toHaveBeenCalled();
                    });
                });

                describe('Command.sendReq()', function() {
                    it('should create a valid XMLHttpRequest', function() {
                        var params = {
                            verb: "POST",
                            endpoint: "endpoint",
                            args: "args",
                            body: "body"
                        };

                        var c = new facade.controller.Command(params),
                            fakeXHR = jasmine.createSpyObj('fakeXMLHttpRequest', ['open', 'setRequestHeader', 'send']);

                        spyOn(c, 'xhrFactory').and.callFake(function() {
                            return fakeXHR;
                        });

                        c.sendReq();

                        expect(fakeXHR.open).toHaveBeenCalledWith(params.verb, (facade.getServiceAPIUrl() + params.endpoint + params.args), true);
                        expect(fakeXHR.setRequestHeader.calls.argsFor(0)).toEqual(['Authorization', 'Bearer ' + facade.getAccessToken()]);
                        expect(fakeXHR.send).toHaveBeenCalledWith(params.body);
                    });

                    it('should set Content-Type header to application/json by default', function() {
                        var params = {
                            verb: "POST",
                            endpoint: "endpoint",
                            args: "args",
                            body: "body"
                        };

                        var c = new facade.controller.Command(params),
                            fakeXHR = jasmine.createSpyObj('fakeXMLHttpRequest', ['open', 'setRequestHeader', 'send']);

                        spyOn(c, 'xhrFactory').and.callFake(function() {
                            return fakeXHR;
                        });

                        c.sendReq();

                        expect(fakeXHR.open).toHaveBeenCalledWith(params.verb, (facade.getServiceAPIUrl() + params.endpoint + params.args), true);
                        expect(fakeXHR.setRequestHeader.calls.argsFor(1)).toEqual(['Content-Type', 'application/json']);
                        expect(fakeXHR.send).toHaveBeenCalledWith(params.body);
                    });

                    it('should call Command.onSuccess when XHR request is DONE and successful', function() {

                        var params = {
                                verb: "POST",
                                endpoint: "endpoint",
                                args: "args"
                            },
                            c = new facade.controller.Command(params),
                            fakeXHR = jasmine.createSpyObj('fakeXMLHttpRequest', ['open', 'setRequestHeader', 'send']);

                        fakeXHR.send.and.callFake(function() {
                            fakeXHR.readyState = XMLHttpRequest.DONE;
                            fakeXHR.status = 200;
                            fakeXHR.responseText = "fake response";

                            fakeXHR.onreadystatechange();
                        });

                        spyOn(c, 'xhrFactory').and.callFake(function() {
                            return fakeXHR;
                        });

                        c.onSuccess = jasmine.createSpy('onSuccess');

                        c.sendReq();

                        expect(c.onSuccess).toHaveBeenCalledWith(fakeXHR);
                        expect(fakeXHR.send).toHaveBeenCalledWith(null);
                    });

                    it('should call Command.onError when XHR request is DONE and HTTP reply is >= 400', function() {

                        var params = {
                                verb: "POST",
                                endpoint: "endpoint",
                                args: "args",
                                body: "body"
                            },
                            c = new facade.controller.Command(params),
                            fakeXHR = jasmine.createSpyObj('fakeXMLHttpRequest', ['open', 'setRequestHeader', 'send']);

                        fakeXHR.send.and.callFake(function() {
                            fakeXHR.readyState = XMLHttpRequest.DONE;
                            fakeXHR.status = 404;
                            fakeXHR.responseText = "fake response";

                            fakeXHR.onreadystatechange();
                        });

                        spyOn(c, 'xhrFactory').and.callFake(function() {
                            return fakeXHR;
                        });

                        c.onError = jasmine.createSpy('onError');

                        c.sendReq();

                        expect(c.onError).toHaveBeenCalledWith(fakeXHR);
                    });
                });

                describe('Command.done()', function() {
                    it('should set the Command.result to SUCCESS when {result:sucess}', function() {
                        var c = new facade.controller.Command();
                        c.done({
                            result: "success"
                        });
                        expect(c.getResult()).toEqual("success");
                    });

                    it('should set the Command.data when {data:Data}', function() {
                        var c = new facade.controller.Command();
                        c.done({
                            data: "Data"
                        });
                        expect(c.data).toEqual("Data");
                    });

                    it('should set the Command.result to FAIL when {result:fail}', function() {
                        var c = new facade.controller.Command();
                        c.done({
                            result: "fail"
                        });
                        expect(c.getResult()).toEqual("fail");
                    });

                    it('should call params.successCb() if is defined and status is success', function() {
                        var c = new facade.controller.Command({
                            successCb: jasmine.createSpy("fakeSuccessCb")
                        });

                        c.done({
                            result: "success",
                            data: "data"
                        });

                        expect(c.params.successCb).toHaveBeenCalledWith("data");
                    });

                    it('should call params.failCb() if status is fail', function() {
                        var c = new facade.controller.Command({
                            failCb: jasmine.createSpy("fakeFailCb")
                        });

                        c.done({
                            result: "fail",
                            data: "data"
                        });

                        expect(c.params.failCb).toHaveBeenCalledWith("data");
                    });
                });
            });

            describe('RequestAssetsCmd', function() {
                it('should have facade.controller.RequestAssetsCmd constructor', function() {
                    expect(typeof window.facade.controller.RequestAssetsCmd).toBe('function');
                });

                it('should extend Command class', function() {
                    expect(new facade.controller.RequestAssetsCmd({}) instanceof facade.controller.Command).toEqual(true);
                });

                it("should use verb GET", function() {
                    var c = new facade.controller.RequestAssetsCmd({});
                    expect(c.params.verb).toEqual("GET");
                });

                describe('notebook assets', function() {
                    it('should point to the correct endpoint', function() {
                        var c = new facade.controller.RequestAssetsCmd({
                            endpoint: "notebooks"
                        });

                        expect(c.params.endpoint).toEqual("/notebooks");
                    });

                    it('should support specific id', function() {
                        var c = new facade.controller.RequestAssetsCmd({
                            endpoint: "notebooks",
                            id: "123"
                        });

                        expect(c.params.endpoint).toEqual("/notebooks/123");
                    });
                });

                describe('sections assets', function() {
                    it('should point to the correct endpoint', function() {
                        var c = new facade.controller.RequestAssetsCmd({
                            endpoint: "sections"
                        });

                        expect(c.params.endpoint).toEqual("/sections");
                    });

                    it('should support specific id', function() {
                        var c = new facade.controller.RequestAssetsCmd({
                            endpoint: "sections",
                            id: "123"
                        });

                        expect(c.params.endpoint).toEqual("/sections/123");
                    });

                    it('should support specific notebookId', function() {
                        var c = new facade.controller.RequestAssetsCmd({
                            endpoint: "sections",
                            notebookId: "123"
                        });

                        expect(c.params.endpoint).toEqual("/notebooks/123/sections");
                    });
                });

                describe('sectiongroups assets', function() {
                    it('should point to the correct endpoint', function() {
                        var c = new facade.controller.RequestAssetsCmd({
                            endpoint: "sectiongroups"
                        });

                        expect(c.params.endpoint).toEqual("/sectiongroups");
                    });

                    it('should support specific id', function() {
                        var c = new facade.controller.RequestAssetsCmd({
                            endpoint: "sectiongroups",
                            id: "123"
                        });

                        expect(c.params.endpoint).toEqual("/sectiongroups/123");
                    });

                    it('should support specific notebookId', function() {
                        var c = new facade.controller.RequestAssetsCmd({
                            endpoint: "sectiongroups",
                            notebookId: "123"
                        });

                        expect(c.params.endpoint).toEqual("/notebooks/123/sectiongroups");
                    });
                });

                describe('pages assets', function() {
                    it('should point to the correct endpoint', function() {
                        var c = new facade.controller.RequestAssetsCmd({
                            endpoint: "pages"
                        });

                        expect(c.params.endpoint).toEqual("/pages");
                    });

                    it('should support specific id', function() {
                        var c = new facade.controller.RequestAssetsCmd({
                            endpoint: "pages",
                            id: "123"
                        });

                        expect(c.params.endpoint).toEqual("/pages/123");
                    });

                    it('should support specific sectionId', function() {
                        var c = new facade.controller.RequestAssetsCmd({
                            endpoint: "pages",
                            sectionId: "123"
                        });

                        expect(c.params.endpoint).toEqual("/sections/123/pages");
                    });
                });

                describe('page contents assets', function() {
                    it('should require a PageID', function() {
                        function invalidCmd() {
                            return new facade.controller.RequestAssetsCmd({
                                endpoint: "content"
                            });
                        }

                        expect(invalidCmd).toThrow();
                    });

                    it('should point to the correct endpoint', function() {
                        var c = new facade.controller.RequestAssetsCmd({
                            endpoint: "content",
                            id: "123"
                        });

                        expect(c.params.endpoint).toEqual("/pages/123/content");
                    });

                    it('should support only the argument includeIDs=true and ignore all the others', function() {
                        var c = new facade.controller.RequestAssetsCmd({
                            endpoint: "content",
                            id: "123",
                            args: "must be ignored"
                        });

                        expect(c.params.args).toEqual("?includeIDs=true");
                    });
                });

                describe('RequestAssetsCmd.onSuccess()', function() {
                    it('should set all Sections from a Notebook when its Id is specified', function() {
                        var c = new facade.controller.RequestAssetsCmd({
                                endpoint: "sections",
                                notebookId: "123"
                            }),
                            data = {
                                value: [{
                                    name: "fake section",
                                    notebookId: "123"
                                }]
                            },
                            req = {
                                responseText: JSON.stringify(data)
                            };

                        facade.model.notebooks.push({
                            id: "123"
                        });

                        spyOn(c, "done");

                        c.onSuccess(req);

                        expect(c.done).toHaveBeenCalledWith({
                            result: "success",
                            data: data.value
                        });
                    });

                    it('should set all Pages from a Section when its Id is specified', function() {
                        var c = new facade.controller.RequestAssetsCmd({
                                endpoint: "pages",
                                sectionId: "321"
                            }),
                            data = {
                                value: [{
                                    name: "fake page",
                                    sectionId: '321'
                                }]
                            },
                            req = {
                                responseText: JSON.stringify(data)
                            };

                        facade.model.sections.push({
                            id: "321"
                        });

                        spyOn(c, "done");

                        c.onSuccess(req);

                        expect(c.done).toHaveBeenCalledWith({
                            result: "success",
                            data: data.value
                        });
                    });

                    it('should pass the raw HTML when Content is requested', function() {
                        var c = new facade.controller.RequestAssetsCmd({
                                endpoint: "content",
                                id: "321"
                            }),
                            data = "fake HTML",
                            req = {
                                responseText: data
                            };

                        spyOn(c, "done");

                        c.onSuccess(req);

                        expect(c.done).toHaveBeenCalledWith({
                            result: "success",
                            data: data
                        });
                    });
                });
            });

            describe('CreateAssetsCmd', function() {
                it('should have facade.controller.CreateAssetsCmd constructor', function() {
                    expect(typeof window.facade.controller.CreateAssetsCmd).toBe('function');
                });

                it('should extend Command class', function() {
                    expect(new facade.controller.CreateAssetsCmd({}) instanceof facade.controller.Command).toEqual(true);
                });

                it("should use verb POST", function() {
                    var c = new facade.controller.CreateAssetsCmd({});
                    expect(c.params.verb).toEqual("POST");
                });

                describe('CreateAssetsCmd.onSuccess()', function() {
                    it('should call done(result:success, data:asset)', function() {
                        var c = new facade.controller.CreateAssetsCmd(),
                            data = {
                                name: "fake asset"
                            },
                            req = {
                                responseText: JSON.stringify(data)
                            };

                        spyOn(c, "done");

                        c.onSuccess(req);

                        expect(c.done).toHaveBeenCalledWith({
                            result: "success",
                            data: data
                        });
                    });
                });
            });

            describe('PatchPageCmd', function() {
                it('should have facade.controller.PatchPageCmd constructor', function() {
                    expect(typeof window.facade.controller.PatchPageCmd).toBe('function');
                });

                it('should extend Command class', function() {
                    expect(new facade.controller.PatchPageCmd({}) instanceof facade.controller.Command).toEqual(true);
                });

                it("should use verb PATCH", function() {
                    var c = new facade.controller.PatchPageCmd({});
                    expect(c.params.verb).toEqual("PATCH");
                });

                it("should set the endpoint to 'pages'", function() {
                    var c = new facade.controller.PatchPageCmd({});
                    expect(c.params.endpoint).toEqual("pages");
                });

                it("should add 'content' as an argument", function() {
                    var c = new facade.controller.PatchPageCmd({});
                    expect(c.params.args).toEqual("/content");
                });

                describe('PatchPageCmd.onSuccess()', function() {
                    it('should call done(result:success, data:asset)', function() {
                        var c = new facade.controller.PatchPageCmd({}),
                            data = {
                                name: "fake asset"
                            },
                            req = {
                                responseText: JSON.stringify(data)
                            };;

                        spyOn(c, "done");

                        c.onSuccess(req);

                        expect(c.done).toHaveBeenCalledWith({
                            result: "success"
                        });
                    });
                });
            });

            describe('SyncRememberNotebookCmd', function() {
                beforeEach(function() {
                    var fakeUploader = {
                        finished: jasmine.createSpyObj('finished', ['connect'])
                    };
                    facade.init({}, {}, fakeUploader);
                });

                it('should have facade.controller.SyncRememberNotebookCmd constructor', function() {
                    expect(typeof window.facade.controller.SyncRememberNotebookCmd).toBe('function');
                });

                it('should extend Command class', function() {
                    expect(new facade.controller.SyncRememberNotebookCmd({
                        rememberNotebook: true
                    }) instanceof facade.controller.Command).toEqual(true);
                });

                it("should have valid initial state", function() {
                    var c = new facade.controller.SyncRememberNotebookCmd({
                        rememberNotebook: "rememberNotebook"
                    });
                    expect(c.rememberNotebook).toEqual("rememberNotebook");
                    expect(c.oneNoteSection).toEqual(null);
                    expect(c.pagesUpdated).toEqual({});

                    expect(c.pagesCreatedCount).toEqual(0);
                    expect(c.pagesPatchedCount).toEqual(0);
                    expect(c.pagesFailCount).toEqual(0);
                    expect(c.pagesToCreateCount).toEqual(0);
                    expect(c.pagesToPatchCount).toEqual(0);

                    expect(typeof c.boundedCb).toEqual("function");

                });

                describe('SyncRememberNotebookCmd.countActions()', function() {

                    var testCount = 0,
                        c,
                        rememberNotebook = {
                            entries: [{
                                    title: "Page To Create",
                                    status: "offline",
                                    syncInfo: false,
                                    type: "html",
                                    taskStatus: 1 // taskStatus = 1: NotActionable, 2: NotCompleted, 4: Completed
                                },
                                {
                                    title: "Page To Update/Patch",
                                    status: "outdated",
                                    type: "html",
                                    taskStatus: 1
                                },
                                {
                                    title: "Page To Overwrite/Patch",
                                    status: "invalid",
                                    type: "html",
                                    taskStatus: 1
                                },
                                {
                                    title: "TASK Not Done HTML To Create",
                                    status: "offline",
                                    syncInfo: false,
                                    type: "html",
                                    taskStatus: 2
                                },
                                {
                                    title: "TASK Not Done HTML To Update/Patch",
                                    status: "outdated",
                                    syncInfo: false,
                                    type: "html",
                                    taskStatus: 2
                                },
                                {
                                    title: "TASK Not Done HTML Overwrite/Patch",
                                    status: "invalid",
                                    syncInfo: false,
                                    type: "html",
                                    taskStatus: 2
                                },
                                {
                                    title: "TASK Not Done PLAIN To Create",
                                    status: "offline",
                                    syncInfo: false,
                                    type: "plain",
                                    taskStatus: 2
                                },
                                {
                                    title: "TASK Not Done PLAIN To Update/Patch",
                                    status: "outdated",
                                    syncInfo: false,
                                    type: "plain",
                                    taskStatus: 2
                                },
                                {
                                    title: "TASK Not Done PLAIN Overwrite/Patch",
                                    status: "invalid",
                                    syncInfo: false,
                                    type: "plain",
                                    taskStatus: 2
                                },
                                {
                                    title: "TASK Done HTML To Create",
                                    status: "offline",
                                    syncInfo: false,
                                    type: "html",
                                    taskStatus: 4
                                },
                                {
                                    title: "TASK Done HTML To Update/Patch",
                                    status: "outdated",
                                    syncInfo: false,
                                    type: "html",
                                    taskStatus: 4
                                },
                                {
                                    title: "TASK Done HTML Overwrite/Patch",
                                    status: "invalid",
                                    syncInfo: false,
                                    type: "html",
                                    taskStatus: 4
                                },
                                {
                                    title: "TASK Done PLAIN To Create",
                                    status: "offline",
                                    syncInfo: false,
                                    type: "plain",
                                    taskStatus: 4
                                },
                                {
                                    title: "TASK Done PLAIN To Update/Patch",
                                    status: "outdated",
                                    syncInfo: false,
                                    type: "plain",
                                    taskStatus: 4
                                },
                                {
                                    title: "TASK Done PLAIN Overwrite/Patch",
                                    status: "invalid",
                                    syncInfo: false,
                                    type: "plain",
                                    taskStatus: 4
                                },
                            ] // entries
                        };

                    beforeEach(function() {
                        // The uploadCompletedTasks flag changes after a few tests! MUST RUN IN ORDER AND SYNC!
                        testCount++;

                        if (testCount > 5)
                            facade._setSettings({
                                uploadCompletedTasks: false
                            });
                        else
                            facade._setSettings({
                                uploadCompletedTasks: true
                            });

                        c = new facade.controller.SyncRememberNotebookCmd({
                            rememberNotebook: rememberNotebook
                        });

                        c.countActions();
                    });

                    it('should NOT CREATE a Task List Page if there is NO Plain Tasks', function() {
                        c = new facade.controller.SyncRememberNotebookCmd({
                          rememberNotebook: { entries: [{
                                  title: "Page To Create",
                                  status: "offline",
                                  syncInfo: false,
                                  type: "html",
                                  taskStatus: 1
                              }] }
                        });

                        c.countActions();
                        expect(c.taskListPageAction).toEqual("");
                        expect(c.plainTasksCount).toEqual(0);
                        expect(c.pagesToCreateCount).toEqual(1);
                    });

                    it('should CREATE a Task List Page', function() {
                        expect(c.taskListPageAction).toEqual("PATCH");
                    });

                    it('should find 6 Plain Tasks', function() {
                        expect(c.plainTasksCount).toEqual(6);
                    });

                    it('should CREATE 4 Pages', function() {
                        expect(c.pagesToCreateCount).toEqual(3);
                    });

                    it('should PATCH 6 Pages', function() {
                        expect(c.pagesToPatchCount).toEqual(7);
                    });

                    //
                    // uploadCompletedTasks: FALSE, testCount > 5)
                    //

                    it('should CREATE a Task List Page when uploadCompletedTasks FALSE', function() {
                        expect(c.taskListPageAction).toEqual("PATCH");
                    });

                    it('should find 3 Plain Tasks when uploadCompletedTasks FALSE', function() {
                        expect(c.plainTasksCount).toEqual(3);
                    });

                    it('should CREATE 3 Pages when uploadCompletedTasks FALSE', function() {
                        expect(c.pagesToCreateCount).toEqual(2);
                    });

                    it('should PATCH 4 Pages when uploadCompletedTasks FALSE', function() {
                        expect(c.pagesToPatchCount).toEqual(5);
                    });

                    describe('a Task List Page já existe e novas PLAIN Tasks precisarem ser adicionadas', function() {

                        beforeEach(function() {
                            rememberNotebook = {
                                entries: [{
                                        title: "PLAIN Task Updated",
                                        status: "updated",
                                        type: "plain",
                                        taskStatus: 2 // taskStatus = 1: NotActionable, 2: NotCompleted, 4: Completed
                                    },
                                    {
                                        title: "DONE PLAIN Task Offline",
                                        status: "offline",
                                        type: "plain",
                                        taskStatus: 4, // taskStatus = 1: NotActionable, 2: NotCompleted, 4: Completed
                                        syncInfo: false
                                    },
                                    {
                                        title: "PLAIN Task Outdated",
                                        status: "outdated",
                                        type: "plain",
                                        taskStatus: 2 // taskStatus = 1: NotActionable, 2: NotCompleted, 4: Completed
                                    }
                                ]
                            };

                            c = new facade.controller.SyncRememberNotebookCmd({
                                rememberNotebook: rememberNotebook
                            });

                            c.countActions();
                        });

                        it("should NOT Create a new Task List Page", function() {
                            expect(c.pagesToCreateCount).toEqual(0);
                        });

                        it("should PATCH the contents of the current Task List Page", function() {
                            expect(c.taskListPageAction).toEqual("PATCH");
                            expect(c.pagesToPatchCount).toEqual(1);
                        });
                    });
                });

                describe('SyncRememberNotebookCmd.isDoneCreatingPages()', function() {
                    var c;

                    beforeEach(function() {
                        c = new facade.controller.SyncRememberNotebookCmd({
                            rememberNotebook: "rememberNotebook"
                        });
                    });

                    it('should return TRUE if ALL Pages to be Created and Patched HAS been done including failures', function() {
                        c.pagesToCreateCount = 2;
                        c.pagesToPatchCount = 1;

                        c.pagesFailCount = 1;

                        c.pagesCreatedCount = 1;
                        c.pagesPatchedCount = 1;

                        c.taskListPageAction = false;

                        expect(c.isDoneCreatingPages()).toEqual(true);
                    });

                    it('should return FALSE if pages to create/patch DO NOT match created/patched count', function() {
                        //pagesToCreateCount: 0, pagesCreatedCount: 1, pagesToPatchCount: 1, pagesPatchedCount: 0, pagesFailCount: 0

                        c.pagesToCreateCount = 0;
                        c.pagesToPatchCount = 1;

                        c.pagesFailCount = 0;

                        c.pagesCreatedCount = 1;
                        c.pagesPatchedCount = 0;

                        c.taskListPageAction = false;

                        expect(c.isDoneCreatingPages()).toEqual(false);
                    });

                    it('should return FALSE if ANY Pages to be Created and Patched HAS NOT been done including failures', function() {

                        c.pagesToCreateCount = 2;
                        c.pagesToPatchCount = 2;

                        c.pagesFailCount = 1;

                        c.pagesCreatedCount = 0;
                        c.pagesPatchedCount = 0;

                        c.taskListPageAction = false;

                        expect(c.isDoneCreatingPages()).toEqual(false);
                    });

                    it('should return TRUE if Must Create Task List Page and count is -1', function() {
                        c.pagesToCreateCount = 1;
                        c.pagesToPatchCount = 0;

                        c.pagesFailCount = 0;

                        c.pagesCreatedCount = 1;
                        c.pagesPatchedCount = 0;

                        c.plainTasksCount = -1;
                        c.taskListPageAction = true;

                        expect(c.isDoneCreatingPages()).toEqual(true);
                    });

                    it('should return FALSE if Must Create Task List Page and count is NOT -1', function() {
                        c.pagesToCreateCount = 1;
                        c.pagesToPatchCount = 0;

                        c.pagesFailCount = 0;

                        c.pagesCreatedCount = 1;
                        c.pagesPatchedCount = 0;

                        c.plainTasksCount = 9;
                        c.taskListPageAction = "CREATE";

                        expect(c.isDoneCreatingPages()).toEqual(false);
                    });

                });

                describe('SyncRememberNotebookCmd.createCommand()', function() {
                    var c = null,
                        spy = null,
                        syncCmd = null,
												oneNotePageId = "oneNotePageId",
												testingStatus = "offline",
                        fakeUploader = null,
                        fakeBuiltPageHTML = "fakeBuiltPageHTML",
                        rememberNotebook = {
                            name: "rememberNotebook"
                        },
                        e = {
                            id: "rememberEntryId",
                            type: "html",
														status: "offline",
                            taskStatus: 1,
                            attachments: [],
                            syncInfo: false,
														builtHtml: null
                        };

                    describe('HTML', function() {
												function testCreateCmd() {
														expect(c instanceof facade.controller.CreateAssetsCmd).toEqual(true);
														expect(c.params.assetName).toEqual("pages");
														expect(c.params.sectionId).toEqual(syncCmd.oneNoteSection.id);
														expect(c.params.body).toEqual(fakeBuiltPageHTML);
														expect(spy).toHaveBeenCalled();
												}

												function testRequestPageContentCmd(){
													expect(c instanceof facade.controller.RequestAssetsCmd).toEqual(true);
													expect(c.params.assetName).toEqual("content");
													expect(c.params.id).toEqual( oneNotePageId );
												}

												function testUploaderCmd() {
														expect(typeof c.execute).toEqual("function");
                            expect(c.syncCmd.isUploaderBounded).toEqual(false);

														c.execute();

														expect(fakeUploader.upload).toHaveBeenCalledWith(e, syncCmd.oneNoteSection.id);
                            expect(fakeUploader.finished.connect.calls.count()).toEqual(1);
                            expect(c.syncCmd.isUploaderBounded).toEqual(true);
												}

												beforeEach(function() {
														syncCmd = new facade.controller.SyncRememberNotebookCmd({
																rememberNotebook: rememberNotebook
														});
														syncCmd.oneNoteSection = { id: "oneNoteSectionID" };

														spy = spyOn(facade.htmlPageBuilder, "buildPageHTML").and.returnValue(fakeBuiltPageHTML);
														fakeUploader = jasmine.createSpyObj("fakeUploader", ['upload']);
                            fakeUploader.finished = jasmine.createSpyObj("fakeUploaderSignal", ['connect']);
														facade._uploader(fakeUploader);

														e.builtHtml = null;
														e.status = testingStatus;

														if(testingStatus != "offline"){
															e.syncInfo = { oneNotePageId: oneNotePageId };
														}
												});

                        it("should create a new Page for an OFFLINE Note with NO attachments", function() {
                            c = syncCmd.createCommand(e);
                            testCreateCmd();
                        });

                        it("should create a new Page for an OFFLINE Task with NO attachments", function() {
                            e.taskStatus = 2;
                            c = syncCmd.createCommand(e);
                            testCreateCmd();
                        });

                        it("should create a new Page for an OFFLINE DONE Task with NO attachments", function() {
                            e.taskStatus = 4;
                            c = syncCmd.createCommand(e);
                            testCreateCmd();
                        });

                        it("should create a new Page for an OFFLINE Note WITH attachments", function() {
                            e.taskStatus = 1;
                            e.attachments.push(1);
                            c = syncCmd.createCommand(e);
                            testUploaderCmd();
                        });

                        it("should create a new Page for an OFFLINE Task WITH attachments", function() {
                            e.taskStatus = 2;
                            e.attachments.push(1);
                            c = syncCmd.createCommand(e);
                            testUploaderCmd();
                        });

                        it("should create a new Page for an OFFLINE DONE Task WITH attachments", function() {
                            e.taskStatus = 4;
                            e.attachments.push(1);
                            c = syncCmd.createCommand(e);
                            testUploaderCmd();

														testingStatus = "outdated";
                        });

												it("should get the Page for an OUTDATED Note with NO attachments", function() {
                            c = syncCmd.createCommand(e);
                            testRequestPageContentCmd();
                        });

                        it("should get the Page for an OUTDATED Task with NO attachments", function() {
                            e.taskStatus = 2;
                            c = syncCmd.createCommand(e);
                            testRequestPageContentCmd();
                        });

                        it("should get the Page for an OUTDATED DONE Task with NO attachments", function() {
                            e.taskStatus = 4;
                            c = syncCmd.createCommand(e);
                            testRequestPageContentCmd();
                        });

                        it("should get the Page for an OUTDATED Note WITH attachments", function() {
                            e.taskStatus = 1;
                            e.attachments.push(1);
                            c = syncCmd.createCommand(e);
                            testRequestPageContentCmd();
                        });

                        it("should get the Page for an OUTDATED Task WITH attachments", function() {
                            e.taskStatus = 2;
                            e.attachments.push(1);
                            c = syncCmd.createCommand(e);
                            testRequestPageContentCmd();
                        });

                        it("should get the Page for an OUTDATED DONE Task WITH attachments", function() {
                            e.taskStatus = 4;
                            e.attachments.push(1);
                            c = syncCmd.createCommand(e);
                            testRequestPageContentCmd();

														testingStatus = "invalid";
												});

												it("should get the Page for an INVALID Note with NO attachments", function() {
														c = syncCmd.createCommand(e);
														testRequestPageContentCmd();
												});

												it("should get the Page for an INVALID Task with NO attachments", function() {
														e.taskStatus = 2;
														c = syncCmd.createCommand(e);
														testRequestPageContentCmd();
												});

												it("should get the Page for an INVALID DONE Task with NO attachments", function() {
														e.taskStatus = 4;
														c = syncCmd.createCommand(e);
														testRequestPageContentCmd();
												});

												it("should get the Page for an INVALID Note WITH attachments", function() {
														e.taskStatus = 1;
														e.attachments.push(1);
														c = syncCmd.createCommand(e);
														testRequestPageContentCmd();
												});

												it("should get the Page for an INVALID Task WITH attachments", function() {
														e.taskStatus = 2;
														e.attachments.push(1);
														c = syncCmd.createCommand(e);
														testRequestPageContentCmd();
												});

												it("should get the Page for an INVALID DONE Task WITH attachments", function() {
														e.taskStatus = 4;
														e.attachments.push(1);
														c = syncCmd.createCommand(e);
														testRequestPageContentCmd();

														testingStatus = "offline";
												});
                    });

										describe('PLAIN', function() {

											beforeEach(function(){
												e.attachments = [];
												e.taskStatus = 2;
												e.type = "plain";
												e.status = testingStatus;

												if(testingStatus != "offline")
													e.syncInfo = { oneNotePageId: "taskListPageONID" };
												else
													e.syncInfo = false;

												syncCmd = new facade.controller.SyncRememberNotebookCmd({
														rememberNotebook: rememberNotebook
												});
											});

											it("should return NULL for an OFFLINE Task ", function() {
													c = syncCmd.createCommand(e);
													expect( c ).toEqual( null );
											});

											it("should return NULL for an OFFLINE DONE Task ", function() {
													e.taskStatus = 4;
													c = syncCmd.createCommand(e);
													expect( c ).toEqual( null );
													testingStatus = "outdated";
											});

											it("should return NULL for an OUTDATED Task ", function() {
													e.taskStatus = 2;
													c = syncCmd.createCommand(e);
													expect( c ).toEqual( null );
											});

											it("should return NULL for an OUTDATED DONE Task ", function() {
													e.taskStatus = 4;
													c = syncCmd.createCommand(e);
													expect( c ).toEqual( null );
													testingStatus = "invalid";
											});

											it("should return NULL for an INVALID Task ", function() {
													e.taskStatus = 2;
													c = syncCmd.createCommand(e);
													expect( c ).toEqual( null );
											});

											it("should return NULL for an INVALID DONE Task ", function() {
													e.taskStatus = 4;
													c = syncCmd.createCommand(e);
													expect( c ).toEqual( null );
											});

										});
                });

								describe('SyncRememberNotebookCmd.createTaskListPageCmd()', function() {
                  var c = null,
                      syncCmd = null,
                      fakeHtmlPageBuilder = null;

                  beforeEach(function() {
                      syncCmd = new facade.controller.SyncRememberNotebookCmd({
                          rememberNotebook: { name: "rememberNotebook" }
                      });
                      syncCmd.oneNoteSection = { id: "oneNoteSectionID" };
                      syncCmd.plainTasks = ["fakeEntry1","fakeEntry2"];

                      fakeHtmlPageBuilder = jasmine.createSpyObj("fakeHtmlPageBuilder",["buildPageTaskList"]);
                      facade.htmlPageBuilder = fakeHtmlPageBuilder;
                    });

                  it("should create a Command only when needed", function(){
                      // No Task List Page Action
                      expect(syncCmd.taskListPageAction.length).toEqual(0);
                      c = syncCmd.createTaskListPageCmd();
                      expect(c).toEqual(null);
                      expect(fakeHtmlPageBuilder.buildPageTaskList).not.toHaveBeenCalled();
									});

									it("should CREATE a new Task List Page ONLY WHEN all Tasks are OFFLINE", function(){
                      syncCmd.taskListPageAction = "CREATE";

                      c = syncCmd.createTaskListPageCmd();

                      expect(c instanceof facade.controller.CreateAssetsCmd).toEqual(true);
                      expect(c.params.assetName).toEqual("pages");
                      expect(c.params.sectionId).toEqual(syncCmd.oneNoteSection.id);
                      expect(fakeHtmlPageBuilder.buildPageTaskList).toHaveBeenCalled();
									});

									it("should REQUEST the Task List Page CONTENTS when ANY Task is NOT OFFLINE (the contents are patched later)", function(){
                    syncCmd.taskListPageAction = "PATCH";
                    syncCmd.taskListPageONID = "taskListPageONID";

                    c = syncCmd.createTaskListPageCmd();

                    expect(c instanceof facade.controller.RequestAssetsCmd).toEqual(true);
                    expect(c.params.assetName).toEqual("content");
                    expect(c.params.id).toEqual(syncCmd.taskListPageONID);
                    expect(fakeHtmlPageBuilder.buildPageTaskList).toHaveBeenCalled();
									});
								});
            });

        }); // end of describe facade.controller

        // TODO Implement Test Suite for ONC.initMasterNotebook
        xdescribe('initMasterNotebook', function() {
            it("should look for the Master Notebook if its ID is not known", function() {
                expect(false).toEqual(true);
            });
            it("should create the Master Notebook if it doesn't exist yet and save its ID", function() {
                expect(false).toEqual(true);
            });
            it("should look for Sections of Master Notebook if its ID is known", function() {
                expect(false).toEqual(true);
            });
            it("should look for the Pages of every Section of Master Notebook", function() {
                expect(false).toEqual(true);
            });
        });
    });

});
