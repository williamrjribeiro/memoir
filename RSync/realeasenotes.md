Memoir
======
Release Notes & Developer Notes

Added on release v1.2
-----------------------
* All Devices
  1. Added support for Tasks. The application uploads all completed and incompleted tasks to OneNote.
  2. Task List Page: all simple Tasks, plain, are put on a consive list. One list per Notebook.
  3. Added many references to Tasks in conjunction with Notes.
  4. Removed Fix Invalid Names alert functionality because it was just adding enthropy without any real gains. All names are fixed automatically.
  5. Added much more resilient way of checking OneNote (but some times it takes much more time to Upload/Patch an Item or check status)
  6. Fixed a few bugs and improved text labels.
  
Open Issues
-----------
* All Devices
 1. Some times, right after uploading a Folder, its status changes to CONFLICT after Checking Folders. This is internal behaviour of OneNote. Remember data uploads normally.
 
OneNote API Ref: 		https://dev.onenote.com/docs
OneNote Error Code Ref: https://msdn.microsoft.com/en-us/office/office365/howto/onenote-error-codes

Added on release v1.0.2
-----------------------
* All Devices
  1. Improved Settings screens: better messages and more organized.
  2. Added Help section on the top menu.
  3. Improved overall performance using optimization compilation flags.

Fixed on release v1.0.2
-----------------------
All issues found on **v1.0.0**
* All Devices
  1. "Upload All" button is enabled even when there's no Notes shown. It should be disabled.
  2. App doesn't show Conflict status for Notes Folders when there are conflicting Pages on Clean Install.
  3. App messages are not clear and confusing.
  4. App toast messages disappear to quickly and user can't read it.
  
* Passport
  1. Note Folder Item is not tall enough, it doesn't show complete cell content. It should show the whole item content.
  2. Not possible to scroll Notes Folder list nor Settings using touch keyboard.
  
* Classic
  1. Not possible to navigate Notes Folder list nor Settings using trackpad.
 
Open Issues
-----------
* All Devices
  1. OneNote asks permission from user everytime Memoir is used.

Nothing here. Huraay!

1.0.0 - 01/03/2016
--------------------

Notes
-----
You can edit and preview this Markdown file on [Dillinger](http://dillinger.io/)