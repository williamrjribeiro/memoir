APP_NAME = RSync

CONFIG += qt warn_on cascades10

LIBS += -lbbdata -lbbpim -lbbsystem -lbbdevice -lbbutilityi18n

DEFINES += QT_USE_QSTRINGBUILDER
# QT_USE_FAST_CONCATENATION QT_USE_FAST_OPERATOR_PLUS

QMAKE_CFLAGS_RELEASE += -fPIC
QMAKE_CXXFLAGS_RELEASE += -fPIC
QMAKE_LFLAGS_RELEASE += -Wl,-z,relro -pie
QMAKE_POST_LINK = ntoarm-objcopy --only-keep-debug ${DESTDIR}/${QMAKE_TARGET} ${DESTDIR}/${QMAKE_TARGET}.sym && ntoarm-objcopy --strip-all -R.ident --add-gnu-debuglink "${DESTDIR}/${QMAKE_TARGET}.sym" "$@" "${DESTDIR}/${QMAKE_TARGET}"

include(config.pri)

device {
	CONFIG(release, debug|release) {
		DEFINES += QT_NO_WARNING_OUTPUT QT_NO_DEBUG_OUTPUT
	}
}
