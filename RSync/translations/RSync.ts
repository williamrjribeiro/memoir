<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>Help</name>
    <message>
        <location filename="../assets/Help.qml" line="11"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Help.qml" line="13"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LocalNotes</name>
    <message>
        <location filename="../assets/LocalNotes.qml" line="23"/>
        <source>Local Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/LocalNotes.qml" line="56"/>
        <source>Notes found: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/LocalNotes.qml" line="62"/>
        <source>Upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/LocalNotes.qml" line="90"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MasterNotebookHeader</name>
    <message>
        <location filename="../assets/MasterNotebookHeader.qml" line="5"/>
        <source>Remember items are uploaded to OneNote&apos;s Notebook:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NotebookGridCell</name>
    <message>
        <location filename="../assets/NotebookGridCell.qml" line="36"/>
        <source>Tap to Upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebookGridCell.qml" line="38"/>
        <source>Outdated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebookGridCell.qml" line="40"/>
        <source>Uploading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebookGridCell.qml" line="42"/>
        <source>Updated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebookGridCell.qml" line="44"/>
        <source>Conflict!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NotebooksList</name>
    <message>
        <location filename="../assets/NotebooksList.qml" line="67"/>
        <source>The Notebook name &apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebooksList.qml" line="67"/>
        <source>&apos; is invalid. Fix name and proceed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebooksList.qml" line="93"/>
        <source>&apos; will be added as a Section of the Notebook &apos;BB10 Notes by Memoir&apos; on OneNote. Proceed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebooksList.qml" line="93"/>
        <source>The Remember Folder &apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebooksList.qml" line="108"/>
        <source>There are </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebooksList.qml" line="108"/>
        <source> completed Tasks on the selected folder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebooksList.qml" line="157"/>
        <source>Failed to upload Notebook &apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebooksList.qml" line="213"/>
        <source>No Notes found on Remember app
Nothing to upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebooksList.qml" line="221"/>
        <source>Tasks are not supported yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebooksList.qml" line="326"/>
        <source>Proceed with upload?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebooksList.qml" line="328"/>
        <source>Don&apos;t ask me again. Always upload.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebooksList.qml" line="344"/>
        <source>Fix invalid names?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebooksList.qml" line="363"/>
        <source>Proceed with upload of all Folders?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebooksList.qml" line="366"/>
        <source>Don&apos;t ask me again. Always upload all.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebooksList.qml" line="383"/>
        <source>Newer versions of some items of this Folder are available on OneNote. If you choose to continue, the older items will overwrite the newer Notes on OneNote. ATTENTION: Important data might be lost. Continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebooksList.qml" line="403"/>
        <source>Upload Completed Tasks?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebooksList.qml" line="422"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebooksList.qml" line="423"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebooksList.qml" line="460"/>
        <source>Check Notes &amp; Tasks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebooksList.qml" line="502"/>
        <source>Check completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebooksList.qml" line="346"/>
        <source>Don&apos;t ask me again. Always fix names.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebooksList.qml" line="364"/>
        <source>Invalid names will be fixed automatically.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebooksList.qml" line="382"/>
        <source>Proceed with OVERWRITE?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebooksList.qml" line="385"/>
        <source>Don&apos;t ask me again. Always overwrite.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebooksList.qml" line="405"/>
        <source>Don&apos;t ask me again. Remember my decision.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebooksList.qml" line="429"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebooksList.qml" line="438"/>
        <source>Upload All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/NotebooksList.qml" line="516"/>
        <source>Something went wrong. Try again later or contact support.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OneNoteSetup</name>
    <message>
        <location filename="../assets/OneNoteSetup.qml" line="15"/>
        <source>Configuring OneNote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/OneNoteSetup.qml" line="17"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../assets/Settings.qml" line="9"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Settings.qml" line="11"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Settings.qml" line="26"/>
        <source>Confirmations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Settings.qml" line="32"/>
        <source>Enable or disable confirmation alert dialogs. If an option is enabled, an alert dialog will appear when needed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Settings.qml" line="78"/>
        <source>A Notebook named &apos;BB10 Notes by Memoir&apos; is automatically added to your OneNote account after you set it up to work with Memoir. Notes &amp; Tasks Folders are created as Sections of this Notebook. Check the Help page for more information.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Settings.qml" line="169"/>
        <source>A Folder, Notes or Tasks, is in Conflict when one of its items has already been sent to OneNote and the online version is newer than the one on the device. When the older Folder from the device is uploaded it OVERWRITES the newer items on OneNote. ATTENTION: Important data might be lost when uploading folders in Conflict.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Settings.qml" line="102"/>
        <source>Upload All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Settings.qml" line="123"/>
        <source>All Notes &amp; Tasks are uploaded automatically including Outdated or Invalid ones. Invalid names are fixed automatically. ATTENTION: Important data might be lost when uploading folders in Conflict.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Settings.qml" line="147"/>
        <source>Overwrite Notes &amp; Tasks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Settings.qml" line="57"/>
        <source>Notes &amp; Tasks Folder Upload</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../assets/main.qml" line="40"/>
        <source>Setting up OneNote. Please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="48"/>
        <source>OneNote setup finished. Memoir&apos;s ready to use!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="81"/>
        <source>Notebooks List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="98"/>
        <source>Task List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="99"/>
        <source>Done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="100"/>
        <source>Due Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="101"/>
        <source>Reminder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="117"/>
        <source>Could Not Initialize ONC. Please contact support.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="137"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
